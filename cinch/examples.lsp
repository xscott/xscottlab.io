


(let a = 123)
(let b = 456.0)

(let z = (cx 7.8 9.0)
(let (y x) = (sincos z))

(func (f n:i64 x:str)
    (for ii = (range n)
        (print x)
    )
)

(struct Point
    x: f64
    y: f64
)

(union FingerTree
    EmptyLeaf
    (SingleLeaf a:i32)
    (DoubleLeaf a:i32 b:i32)
    (TripleLeaf a:i32 b:i32 c:i32)
    (SingleNode x:FingerTree)
    (DoubleNode y:FingerTree x:FingerTree)
    (TripleNode x:FingerTree y:FingerTree z:FingerTree)
)
