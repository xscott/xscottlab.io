
;; Line comments use two semicolons.
(; Block comments use parens and semis (; and they nest. ;) but vim doesn't know that ;)


;; The WebAssembly Text format is much more general than the example shown
;; below.  At the time of this writing, it is difficult to find clear and
;; complete docs.  The official standard uses a notation I don't understand, so
;; I've pieced together what I could from all over the place and tried to use a
;; simple and consistent style that allows me to use wat2wasm to compare with
;; the binaries I'm writing with Crunch.  Many of the sections and declarations
;; are probably much more general than I've shown, but either I didn't need
;; those features, or I didn't know about them, or both.  I haven't really
;; stuck with the nomenclature I've seen in the standards.  For instance, I
;; refer to the various helper names as "tags", but I'm pretty sure they should
;; be called "identifiers", and I'm not sure what I want to call the numbering
;; for function pointer elements in the table.  The ordering of declarations
;; can be in a more flexible order than I've shown too.

(module

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;
    ;; Imports and Exports inline with the definitions:
    ;;
    ;; (import "env" "easy" (func $easy (param i32)))
    ;;
    ;; (func $run (export "run") (param) (result)
    ;;     (call $easy (i32.const 123))
    ;;     (call $easy (i32.const 456))
    ;; )
    ;; (memory (export "mem") 1)
    ;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ;; 1: Type Section - types are used for the imported and defined functions
    ;; in the module.  In the binary, types are referenced by the zero-based
    ;; index from their order of declaration.  The WAT $tags are optional, but
    ;; using them makes reading the WAT file much easier to read.  Apparently
    ;; we're allowed to have type declarations which are not used.

    (type $u32_to_void  (func (param i32)                           ))
    (type $void_to_void (func                                       ))
    (type $void_to_f64  (func                      (result f64)     ))
    (type $buncha_stuff (func (param f64 v128 i64) (result i32 f32) ))


    ;; 2: Import Section - imports are provided by the calling environment.
    ;; They are functions that WASM can call, so we need to specify the types.
    ;; Imports have both a module name and a function name.  I don't really
    ;; need the module name to implement Crunch, so I've taken to always using
    ;; "env" as the module name.  Here the tags $putdone and $putcode are for
    ;; readability with the WAT module, and we use the tags from the Type
    ;; Section above.  In the binary format, imports come first, and so their
    ;; indexes are always before any of the func indexes in the next section.

    (import "env" "putcode" (func $putcode (type $u32_to_void)))
    (import "env" "putdone" (func $putdone (type $void_to_void)))


    ;; 3 and 10: Function and Code Sections - In the binary format, Section 3
    ;; only specifies the signatures and section 10 provides the code.  I'm not
    ;; sure if the WAT format allows code tags, but it seems nice to have them
    ;; all here in one place regardless.  Function indexes come after the the
    ;; indexes for imports, and again it's convenient to use $tags for names.

    (func $start (type $void_to_void)
        ;; There are 100s of WebAssembly opcodes, and I'm not going to cover
        ;; them in here.  I'm not certain when parens can be ignored, so I tend
        ;; to use them for all instructions.  Also note that ops can be nested
        ;; so that it starts to look like a Lisp notation.  I lean towards
        ;; preferring the instructions flattened, but I'm on the fence.

        ;; flatened
        (i32.const 0x48)
        (call $putcode)

        ;; nested
        (call $putcode (i32.const 0x69))

        ;; indirect
        (i32.const 0x21)
        (i32.const 0)
        (call_indirect (type $u32_to_void))

        (call $putdone)
    )

    ;; The $start example above uses the tag from the type section to specify
    ;; the signature, and this more closely reflects how the binary is built.
    ;; However it is possible to write functions which declare the params,
    ;; locals, and results inline.  Then we let the WAT assembler figure it all
    ;; out.  This approach lets us use tags for the params and locals:

    (func $foo (param $a f64) (param $b f64) (result i64)
        (local $x f32)
        (local.set $x
            (f32.demote_f64
                (local.get $a)
            )
        )
        (i64.reinterpret_f64
            (f64.add
                (f64.promote_f32
                    (local.get $x)
                )
                (local.get $b)
            )
        )
    )


    ;; 4 and 9: Table and Element Sections - I only need the Table section to
    ;; provide function pointers, but it seems to be much more general than
    ;; that.  Supposedly you can use the table section to link data or code
    ;; across separate modules and all kinds of other magic.  For my limited
    ;; uses, the table is nothing but a list of function pointers, and the
    ;; index of the functions in the table can be used by the call_indirect
    ;; instruction.
    ;;
    ;; This topic is a bit confusing at first.  The call instructions above
    ;; have the import or function "index" hard coded into the binary.  When
    ;; using call_indirect, the integer on the top of the stack selects the
    ;; "element" from the table.  We could have put the functions in a
    ;; different order here, and it is not necessary to put all functions into
    ;; the table - only the ones we want to call using call_indirect.

    (table 3 funcref)
    (elem (i32.const 0) $putcode $putdone $foo)


    ;; 5 and 11: Memory and Data Sections - The memory section is optional, but
    ;; we always want one for Crunch.  The first argument is initial size in
    ;; 65536 byte pages and the second is a hint for how big the memory might
    ;; grow.  We give the memory a tag name so that we can export it below.
    ;; The data declarations allow us to initialize pieces of the memory.
    ;;
    ;; When I started this project, memory was only allowed to use 32 bits
    ;; total (4GB).  Since then, it looks like the "memory64" feature is
    ;; becoming accepted, but I'm not sure how to use that yet.

    (memory $memory 1 65536)
    (data (i32.const 128) "\11\22")
    (data (i32.const 256) "\44\55")

    ;; In order to enable Memory64, provide an i64 flag (0x04) to the memory
    ;; declaration and all memory/load/store instructions and data declarations
    ;; will use i64 for addresses instead of i32.  Here we're saying we could
    ;; use over 1 million 65536 byte pages, which is 64GB.  However, at the
    ;; time of this writing all support is still experimental.

    ;; (memory $memory i64 1 1048576)
    ;; (data (i64.const 128) "\11\22")
    ;; (data (i64.const 256) "\44\55")


    ;; 6: Global Section - Just like the locals in the functions, globals can
    ;; be one of i32, i64, f32, f64, or v128 types.  They can also be constant
    ;; or mutable.  I suspect the xxx.const initializers are "smart" in that
    ;; they build the correct data type from the literal they are provided.

    (global $r      i32  (i32.const 123)  )
    (global $s (mut i64) (i64.const 456)  )
    (global $t (mut f32) (f32.const 10.0) )
    (global $u      f64  (f64.const 64.0) )
    (global $v v128
        (v128.const i8x16
            0x90 0x91 0x92 0x93 0x94 0x95 0x96 0x97
            0x98 0x99 0x9A 0x9B 0x9C 0x9D 0x9E 0x9F
        )
    )


    ;; 7: Export Section - We can export the memory section and any functions
    ;; for access from the calling environment.  I think we can export globals
    ;; too, but I haven't tested that.

    (export "mem" (memory $memory))
    (export "start" (func $start))


    ;; 8: Start Section - We can provide one function to be called when the
    ;; module is first initialized/loaded.  I don't really need this feature.

    ;;(start $start)


    ;; 12: DataCount Section - Generally the sections come in order in the
    ;; binary, but it looks like they added Section 12 as an afterthought and
    ;; it comes before Section 10 for Code

)


;; Need to put examples of weird instructions, like loads with offset and
;; alignment, somewhere above:
;;
;;  (func (result i64)
;;      i32.const 128
;;      i64.load offset=1 align=2
;;  )
;;

