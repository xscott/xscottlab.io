
(module

    (import "env" "put" (func $put (param i32) (result)))
    (import "env" "out" (func $foo (param i32) (result)))

    (memory $memory i64 1 1048576)

    (func $ifthen (param $n i32) (result)
        (if
            (local.get $n)
            (then
                (i32.const 123)
                (call $put)
            )
        )
    )

(;
    (func $ifelse (param $n i32) (result)
        (if
            (local.get $n)
            (then
                (i32.const 123)
                (call $put)
            )
            (else
                (i32.const 789)
                (call $put)
            )
        )
    )

    (func $ternary1 (param $n i32) (result)
        (if (result i32)
            (local.get $n)
            (then
                (i32.const 1)
            )
            (else
                (i32.const 2)
            )
        )
        drop
    )

    (func $ternary2 (param $n i32) (result)
        (if (result i32 i64)
            (local.get $n)
            (then
                (i32.const 1)
                (i64.const 2)
            )
            (else
                (i32.const 3)
                (i64.const 4)
            )
        )
        drop drop
    )

    (func $ternary3 (param $n i32) (result)
        (if (result i32 i64 f64)
            (local.get $n)
            (then
                (i32.const 1)
                (i64.const 2)
                (f64.const 3)
            )
            (else
                (i32.const 4)
                (i64.const 5)
                (f64.const 6)
            )
        )
        drop drop drop
    )
;)
)

(;
    (func $whatif (export "whatif") (param $n i32) (result)
        (i32.const 123)
        (if (param i32)
            (local.get $n)
            (then
                (call $put)
            )
            (else
                (call $foo)
            )
        )
    )
;)

