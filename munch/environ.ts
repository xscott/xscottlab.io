// Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

import {
    Overloadable, Callable, isCallable,
    failure, panic,
    ConstantBool, LiteralI64, LiteralF64,
    Instruction, Jump,
    Statement, IfElseStmt, WhileStmt, FuncStmt, Variable,
    SpecificType,
} from "./index.ts";

// XXX: cleanup this Literal/Bindable stuff...  Where does it go?

// XXX: GenericType?  SpecificType
export type Bindable = Variable | LiteralI64 | LiteralF64 | ConstantBool;

export class Environment {
    bindings: Map<string, Bindable | Overloadable[]>;

    constructor(
        public parent: null | Environment,
        public stmt: null | Statement,
    ) {
        this.bindings = new Map();
    }

    defName(
        row: number, col: number, name: string, obj: Bindable
    ): void {
        const existing = this.bindings.get(name);
        if (existing !== undefined) {
            if (existing instanceof Array) {
                failure(row, col, 
                    "one or more functions exist with this name"
                );
            } else {
                failure(row, col,
                    "declaration with this name already exists"
                );
            }
        }
        this.bindings.set(name, obj);
    }

    defFunc(
        row: number, col: number, name: string,
        required: SpecificType[], func: Overloadable
    ): void {
        const existing = this.bindings.get(name);
        if (existing === undefined) {
            this.bindings.set(name, [func]);
            return;
        }
        if (existing instanceof Array) {
            for (const other of existing as Overloadable[]) {
                if (other.matches(required)) failure(row, col,
                    "callable with matching positional arguments already exists"
                );
            }
            existing.push(func);
            return;
        }
        failure(row, col, `name ${name} already exists and is not a function`);
    }

    getName(row: number, col: number, name: string): Bindable {
        const binding = this.bindings.get(name);
        if (binding instanceof Array) failure(
            row, col, `${name} is a function and can't be used as a value`
        );
        if (binding === undefined) {
            if (this.parent === null) failure(row, col, `${name} not found`);
            return this.parent.getName(row, col, name);
        }
        return binding as Bindable;
    }

    getCall(
        row: number, col: number, name: string,
        positional: SpecificType[]
    ): Callable {
        const binding = this.bindings.get(name);
        if (binding instanceof Array) {
            for (const overload of binding) {
                if (overload.matches(positional)) {
                    if (isCallable(overload)) return overload;
                    panic("expected Overloadable to be Callable");
                }
            }
            if (this.parent !== null) {
                return this.parent.getCall(row, col, name, positional);
            }
            failure(row, col, `callable named '${name}' not found`);
        }
        if (binding === undefined) {
            if (this.parent === null) failure(
                row, col, `callable named '${name}' not found`
            );
            return this.parent.getCall(row, col, name, positional);
        }
        if (isCallable(binding)) return binding;
        failure(row, col, `${name} is not callable`);
    }

    emitJump(
        row: number, col: number, label: null | string, extra: number
    ): Instruction {

        // XXX: make list of destruct/cleanup variables as needed

        let env: null | Environment = this as Environment;
        while (env !== null) {
            const stmt = env.stmt;
            if (stmt instanceof WhileStmt) {
                if (label === null || label === stmt.label) {
                    return new Jump(label, extra);
                }
            // XXX: Why are we checking for function
            // instead of just falling to the end?
            } else if (stmt instanceof FuncStmt) {
                failure(row, col, "matching loop not found in function");
            }
            env = env.parent;
        }
        failure(row, col, "no matching loop found");
    }

}

