// Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

import {
    Assembler,
    StoragePool, StorageCell, Instruction,
    panic,
    Uns32, Bytes,
    GlobalGet, GlobalSet,
    LocalGet, LocalSet, LocalTee,
} from "./index.ts";

export class GlobalPool implements StoragePool {
    freed: Set<GlobalCell> = new Set();

    constructor(
        public assem: Assembler
    ) {}

    alloc(wasmType: Bytes) {
        for (const cell of this.freed) {
            if (cell.wasmType === wasmType) {
                this.freed.delete(cell);
                return cell;
            }
        }
        const index = this.assem.newGlobal(wasmType);
        return new GlobalCell(index, wasmType);
    }

    free(cell: GlobalCell) {
        this.freed.add(cell);
    }
}

class GlobalCell implements StorageCell {
    constructor(
        public index: Uns32,
        public wasmType: Bytes
    ) {}

    emitGet() { return new GlobalGet(this.index); }
    emitSet() { return new GlobalSet(this.index); }
    emitTee(): never {
        return panic("global tee not implemented");
    }
}

export class LocalPool implements StoragePool {
    offset: Uns32;
    locals: Bytes[] = [];
    freed: Set<LocalCell> = new Set();

    constructor(paramCount: Uns32) {
        // XXX: make this use the `public` stuf
        this.offset = paramCount;
        this.locals = [];
        this.freed = new Set();
    }

    alloc(wasmType: Bytes) {
        for (const cell of this.freed) {
            if (cell.wasmType === wasmType) {
                this.freed.delete(cell);
                return cell;
            }
        }
        const index = this.offset + this.locals.length;
        this.locals.push(wasmType);
        return new LocalCell(index, wasmType);
    }

    free(cell: LocalCell) {
        this.freed.add(cell);
    }
}

class LocalCell implements StorageCell {
    // XXX: Do the delayed indexing for compact locals?
    constructor(
        public index: Uns32,
        public wasmType: Bytes,
    ) {}

    emitGet() { return new LocalGet(this.index); }
    emitSet() { return new LocalSet(this.index); }
    emitTee() { return new LocalTee(this.index); }
}

