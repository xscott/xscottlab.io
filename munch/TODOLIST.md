
 Open Questions
================

- Should we allow overloading `&` and `|` ???

- Are types values that can be compared `==` and `!=` ???

- How will we cache adapters for calls with named params ???

- How will we put complicated library functions in the code ???
    - alloc, incred, decref, free, sin, cos, svdecomp, ...
    - Probably just as the strings for the bodies,
      and use our parser and emitList to build each one.

- It'd be nice to have let/const for compile time constants
    - The generated code would NOT do a global/local get
    - Maybe use `const` for this, and save `let` for later
        - Maybe use `let` for both compile time and runtime
            - compile time avoids using a cell

- Big refactor to have Tuples of Cells implement composite values?
    - Allows cf32 to be implemented as two f32 instead of half f32x4
    - Args, Params, Results can by multiple primtive types
    - Could do `struct { x: f64, y: f64, z: f64 }` as value types
        - Not stored in memory and passed as address
    - Primitive, Composite, and Reference types
    - Need to think carefully about Placement?

- Looking at assignments:

```
    type Vec3 = struct { x: f64, y: f64, z: f64 };

    var A = new Mat[Vec3](10, 10);
    var B = new Mat[Vec3](3, 3);
    var C = new Mat[cf64](4, 4);

    # HandleInCell = HandleInCell;
    A = B;               

    # HandleOnStack = HandleOnStack; # error, sizes don't match
    A[] = B[]; 

    # HandleOnStack = ValueOnStack;
    A[0, 0].x = 1.2;


```

 This Session
==============

- LambdaType

- LambdaFunc

- TypeType

 Near Future
=============

- CellGroup and CellArena
    - SpecificType can be implemented as multiple WasmTypes
    - cf32 = f32 f32
    - cf64 = f64 f64
    - Mat[f64] = u32x4 u32x4
        { blobptr, dataptr, rows, cols }, { vstep, hstep, _, _ }

- CF64
    - `a.real, a.imag = a.imag, a.real`
    - FieldInCell set attribute

- VarStmt
    - Fix Environment to use visibleRow, visibleCol
    - TypeExpr.evalType()
        - Need to be able to specify type for var decls

- Struct and Union types

- Parse
    - New Constructor
    - New Initializer

- Placements:
    - ValueOnStack, ValueInCell
    - FieldInCell
    - HandleOnStack, HandleInCell
    - Incref, Decref
        - First u32 in v128
        - Still need to ask the type because it might
          be multiple cells forming a group

- FuncStmt
    - type inference
    - ReturnStmt

- Memory Allocation and Reference Counts
    - `alloc`
        - `find_range`
        - `set_range`
    - `incref`
    - `decref`
    - `clr_range`
        - `free`
        - `clear_range`

- LambdaExpr
    - expr.ts

- LambdaType
    - grammar.y

- Stub out reference counting with JavaScript imports
    - alloc, incref, decref, (free)

- Implement Ternary (zero-or more results)

- ForStmt
    - for ii = 0 to 100 by 2 { ... }

- public -> readonly where possible

- Put the I32 opcodes in?


 Coming Soon
=============

- Figure out how to use "Function References"
    - Looks like the newer cleaner way to do function pointers
    - I think it means a separate Table for each Signature
        - So the runtime check only required funcref in bounds of table
    - https://github.com/WebAssembly/function-references/blob/main/
        proposals/function-references/Overview.md
    - Requires changes to Assembler implementation
        - Probably means we need to emit the table number too
        - Does it change the Assembler API?

- Setup a test directory:  testcase.crunch, testcase.output

- Can we consolidate Intrinsic and FuncStmt?

- AssertStmt

- ThrowStmt

- SIMD Types
    - v128,
    - i8x16, i16x8, i32x4, i64x2
    - u8x16, u16x8, u32x4, u64x2
    - f32x4, f64x2

- TypeExpr instead of Identifier

- Fix Lexer to handle CF32
- CF32 pair of f32
    - CellGroups

 Longer Term
=============

- Memory Management
    - struct Block { refcount: u32, blkcount: u32 }
    - struct Handle { blkaddr: u32, ptraddr: u32 }
    - alloc, incref, decref, free

- Cleanup / automatic reference counting
    - return, break, continue, close curly

- TypeStr
    - operator +

- ArrayType, MatrixType, VectorType, BoxedType
    - TypeExpr

- Constructor syntax in parse.ts
    - `var x = new Array[i64] { 1, 2, 3 };`

- Array slices
    - subscript() in parser.ts

- Type declarations
    - TypeStmt in parser.ts

- Generics
    - `func foo[T: type](value: T): T { ... }`

- Cleanup XXX and TODO items
   - Put them in this list instead

