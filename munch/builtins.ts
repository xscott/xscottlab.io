// Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

import {
    failure,
    Callable, Overloadable, Placement, Imports,
    Environment,
    ValueOnStack,
    TypeBool, TypeI64, TypeF64, TypeCF64,
    Instruction, FunCall,
    Neg_I64, Neg_F64, Neg_F64x2,
    Add_I64_I64, Add_F64_F64, Add_F64_CF64, Add_CF64_F64, Add_F64x2_F64x2,
    Sub_I64_I64, Sub_F64_F64, Sub_F64_CF64, Sub_CF64_F64, Sub_F64x2_F64x2,
    Mul_I64_I64, Mul_F64_F64, Mul_F64_CF64, Mul_CF64_F64, Mul_CF64_CF64,
    Lt_I64_I64, Lt_F64_F64,
    ConstantBool, LiteralF64,
    SpecificType, Constructor,
    Assembler, Bindable, WasmType,
} from "./index.ts";



const ConstantFalse = new ConstantBool(0);
const ConstantTrue  = new ConstantBool(1);

const ConstantInf = new LiteralF64(-1, 0, "Infinity");
const ConstantNaN = new LiteralF64(-1, 0, "Not a Number");
const ConstantRt2 = new LiteralF64(-1, 0, "1.41421356237309504880");
const ConstantPi  = new LiteralF64(-1, 0, "3.14159265358979323846");
const ConstantE   = new LiteralF64(-1, 0, "2.71828182845904523536");
const ConstantC   = new LiteralF64(-1, 0, "299792458.0");

export class Intrinsic implements Callable, Overloadable {
    head: unknown[];

    constructor(
        public name: string,
        public params: SpecificType[],
        public results: SpecificType[],
        public irctor: Constructor<Instruction>,
        ... head: unknown[]
    ) {
        this.head = head;
    }

    matches(posTypes: SpecificType[]) {
        if (this.params.length !==posTypes.length) return false;
        for (let ii = 0; ii<this.params.length; ++ii) {
            if (!this.params[ii].isTypeEqual(posTypes[ii])) return false;
        }
        return true;
    }

    emitCall(
        row: number, col: number,
        assem: Assembler,
        argCodes: Instruction[],
        argPlaces: Placement[],
        argNames: (null | string)[],
    ): [Instruction, Placement[]] {
        if (argNames.some(name => name !== null)) failure(
            row, col, "this function does not accept named arguments"
        );
        const resultPlaces = this.results.map(
            // XXX: Someday these could be HandleOnStack too
            rr => new ValueOnStack(rr)
        );
        // The argTypes were checked in the call to this.matches(...)
        return [new this.irctor(... this.head, ... argCodes), resultPlaces];
    }
}

class LibraryBuilder {
    env: Environment = new Environment(null, null);
    imports: Map<string, (... args: any[]) => any> = new Map();
    counter: number = 0;
    
    constructor(
        public assem: Assembler,
    ) {}

    defConstant(name: string, value: Bindable) {
        this.env.defName(-1, -1, name, value);
    }

    defIntrinsic(
        name: string,
        params: SpecificType[],
        results: SpecificType[],
        irctor: Constructor<Instruction>,
        ... head: unknown[]
    ) {
        const intrin = new Intrinsic(
            name, params, results, irctor, ... head
        );
        this.env.defFunc(-1, -1, name, params, intrin);
    }

    defImport(
        stdname: string,
        params: SpecificType[],
        results: SpecificType[],
        callable: (... args: any[]) => any,
    ) {
        const wasmName = stdname + "#" + String(this.counter);
        this.counter += 1;
        const wasmParams = params.map(pp => pp.workingType());
        const wasmResults = results.map(rr => rr.workingType());
        const wasmFunc = this.assem.newImport(
            wasmName, wasmParams, wasmResults
        );
        this.imports.set(wasmName, callable);

        return this.defIntrinsic(
            stdname, params, results, FunCall, wasmFunc.funIndex
        );
    }
}

export function stdlib(assem: Assembler): [Environment, Imports] {
    const builder = new LibraryBuilder(assem);

    // TODO: We're going to need a way to make
    // the exports available to the imports.

    const I64  = TypeI64;
    const F64  = TypeF64;
    const CF64 = TypeCF64;
    const BOOL = TypeBool;

    builder.defImport("log", [I64], [], console.log);
    builder.defImport("log", [F64], [], console.log);
    builder.defImport("rand", [], [F64], Math.random);

    builder.defConstant("false", ConstantFalse);
    builder.defConstant("true",  ConstantTrue);
    builder.defConstant("inf",   ConstantInf);
    builder.defConstant("nan",   ConstantNaN);
    builder.defConstant("rt2",   ConstantRt2);
    builder.defConstant("pi",    ConstantPi);
    builder.defConstant("e",     ConstantE);
    builder.defConstant("c",     ConstantC);

    builder.defIntrinsic("-", [I64], [I64], Neg_I64);
    builder.defIntrinsic("-", [F64], [F64], Neg_F64);
    builder.defIntrinsic("-", [CF64], [CF64], Neg_F64x2);

    builder.defIntrinsic("+", [I64, I64], [I64], Add_I64_I64);
    builder.defIntrinsic("+", [F64, F64], [F64], Add_F64_F64);
    builder.defIntrinsic("+", [F64, CF64], [CF64], Add_F64_CF64);
    builder.defIntrinsic("+", [CF64, F64], [CF64], Add_CF64_F64);
    builder.defIntrinsic("+", [CF64, CF64], [CF64], Add_F64x2_F64x2);

    builder.defIntrinsic("-", [I64, I64], [I64], Sub_I64_I64);
    builder.defIntrinsic("-", [F64, F64], [F64], Sub_F64_F64);
    builder.defIntrinsic("-", [F64, CF64], [CF64], Sub_F64_CF64);
    builder.defIntrinsic("-", [CF64, F64], [CF64], Sub_CF64_F64);
    builder.defIntrinsic("-", [CF64, CF64], [CF64], Sub_F64x2_F64x2);

    builder.defIntrinsic("*", [I64, I64], [I64], Mul_I64_I64);
    builder.defIntrinsic("*", [F64, F64], [F64], Mul_F64_F64);
    builder.defIntrinsic("*", [F64, CF64], [CF64], Mul_F64_CF64);
    builder.defIntrinsic("*", [CF64, F64], [CF64], Mul_CF64_F64);
    builder.defIntrinsic("*", [CF64, CF64], [CF64], Mul_CF64_CF64);

    builder.defIntrinsic("<", [I64, I64], [BOOL], Lt_I64_I64);
    builder.defIntrinsic("<", [F64, F64], [BOOL], Lt_F64_F64);

    return [builder.env, Object.fromEntries(builder.imports)];
}

