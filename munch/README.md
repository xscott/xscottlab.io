
 A Language for Number Crunching
=================================

 Short Example
===============


```swift
    <!doctype html><title>Foo Bar</title><script src="crunch.js">

    # A variation on a cliche interview question
    func splashboom(nn:i64):i64 {
        var count = 0;
        for ii = fwd(count) {
            if ii%15 == 0 {
                print("pound bang\n");
            } elif ii%3 == 0 {
                print("thump\n");
            } elif ii%5 == 0 {
                print("splat\n");
            } else {
                count += 1;
            }
        }
        return count;
    }

    splashboom(100);

    </script>
```



 Overview
==========

Browsers are great ...   Most people don't know ...

Occupies a space near Go, C++, or Swift



 Variable, Function, and Type Name Resolution
==============================================

```
    var a = 123;
    {
        f();

        var a = 456;

        func f() {
            print(a);
        }
    }

```

```
    var a = 123;
    {
        f() {
            print(a);
        }

        var a = a + 456;

:
        - Box: { blobptr: u32, dataptr: u32, ignored: u64 }
        - Row: { blobptr: u32, dataptr: u32, size: u32, step: u32 }
        - Col: { blobptr: u32, dataptr: u32, size: u32, step: u32 }
        - Mat: { blobptr: u32, dataptr: u32, rows: u32, cols: u32 }
               { hstep: u32, vstep: u32, ignored: u64 }
        - Str: { blobptr: u32, byteptr: u32, length: u32, hash: u32 }

workingType(): u32 | i64 | f64 | v128
    - only appropriate for Primitives u32, i64, f64, v128
    - Bool: u32
    - i8, i16, i32, i64, u8, u16, u32, u64?  : all go to i64
    - f32, f64 : f64
    - cf32, cf64: v128

memoryBytes(): number
    - Not sure if this is the correct interface...
    - How many bytes to memcpy for assignment or reserve for allocation

isPrimitive(): boolean
    - Placements make decisions based on this

isTypeEqual(that: SpecificType): boolean
    - Frequently ThisType === ThatType
    - But also Mat<T>, LambdaType, etc...


 Primitive Types
=================

```
    bool,
    i8, i16, i32, i64,
    u8, u16, u32, (u64),
    f32, f64, cf32, cf64,
    i8x16, i16x8, i32x4, i64x2,
    u8x16, u16x8, u32x4, u64x2,
    f32x4, f64x2
```

 Reference Types
=================

```
    Str, Array<T, N> (fixed size),
    Box<T>, Row<T>, Col<T>, Mat<T>,
    Struct<...>, Union<...>, Enum<...>
```

 Placements
============

Stil some shit to figure out:

    var a = new Mat[f64](100, 100);
    # This is not a memcpy
    a = new Mag[f64](20, 20);

    type Foo = struct {
        data: f64[3]; # is this the right syntax?
        data[3]: f64; # or is this the one?
    }
    var f = new Foo { data: { 1.0, 2.0, 3.0 } };
    var g = new Foo { data: { 4.0, 5.0, 6.0 } };
    var h = new Foo { data: { 7.0, 8.0, 9.0 } };
    f.data = g.data; # this is a memcpy
    f = h;  # this is an incref and handle copy

Placement:
    discard()
    rvalue()
        - An expression has been evaluated, and it will be used for
          a function call or operator
        - ValueOnStack => ValueOnStack  (Nop)
        - ValueInCell => ValueOnStack  (Cell.emitGet())
        - FieldInCell => ValueOnStack (Cell.emitGet, getLane, promote)
        - HandleOnStack:
            PrimitiveType => ValueOnStack (emitLoad, promote)
            ReferenceType => HandleOnStack (Nop)
        - HandleInCell:
            PrimitiveType => ValueOnStack (emitGet, emitLoad, promote)
            ReferenceType => HandleOnStack (emitGet, Nop)
    assign()
    emitLane()
    emitAttr()
    emitItem()


ValueOnStack:
    Will only hold primitive types.
    toStack():
        Nop
    assign():
        Error, can't assign to temporary value
    discard():
        Drop

ValueInCell:
    Will only hold primitive types.
    toStack():
        cell.emitGet()
    assign():
        no decref needed
        cell.emitSet()
    discard:
        Nop

FieldInCell:
    Parts of a v128 in a lane of a cell
    toStack():
        cell.emitGet()
        lane.emitField()
    assign():
        cell.emitGet()
        modify lane
        cell.emitSet()
    discard:
        Nop

HandleOnStack:
    Can point to Primitive and Reference types
    The handle is usually u64 as { blob: u32, addr: u32 } for Primitive Types
    The handle is usually v128 as { blob: u32, ... } for Reference Types
        We need each type to manage Incref/Decref StrInc/StrDec, etc...
    toStack():
        Primitive Types:
            load and convert
        Reference Types:
            leave as handle
            type checking will deal with it
    assign():
        Primitive Type:
            store or memcpy from src
        Reference Type:
            memcpy memory to memory
    discard:
        Decref

HandleInCell:
    Comparable to HandleOnStack
    toStack():
        Primitive Types:
            cell.emitGet()
            load and convert
        Reference Types:
            cell.emitGet()
            Incref
            leave as handle
            type checking will deal with it
    assign():
        Primitive Type
            move address to stack
            store/memcpy from src
        Reference Type
            movce address to stack
            memcpy memory to memory
    discard():
        Nop

Each Placement has:
    toStack()
    assign(source)
    discard()
    - Work out the details for each Placement
    - Not all placements support all three

Will any/list/dict require something else?
    - VariantInCell and VariantOnStack?
    - Maybe these are just special Handles
        - We've already got per-type incref/decref



 Designed by me for me
=======================


I don't particularly care about semicolon insertion.  If it was there I'd
probably use it, but omitting it simplifies the scanner and parser, and
semicolons never bothered me in C or C++.

I don't care about method syntax.  The object.method(...) notation is popular
with people who use sophisticated editors and auto-completion, but I don't use
or like those editors.

I really like operator and function overloading.  Coming up with names is hard,
and there are lot more mathematical objects than just floats and integers which
benefit from conventional operator notation.

I don't like implicit conversions.  I'm fine with typing 2.0*x instead of 2*x
when I need it.

I'm not making a language for "programming in the large".  I like to write
stuff that's less than a few thousand lines, and the compiler should be able to
rip through that client-side in a fraction of a second.  I don't want separate
compilation and linking or to compile ahead of time.  I want to save and reload
the page.

I'm not making a language for creating web sites.  I'm treating the browser
like a really convenient virtual machine and terminal.  It gives me access to
graphics, audio, and more in a way that's portable across many platforms.
Also, I want to show my programs to my friends and family, so it's nice to be
able to publish it with a link.  But I'm not interested in making web sites for
people.  Other than maybe downloading some data files, I'm not interested in
the back end side of web stuff at all.








