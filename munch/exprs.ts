// Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

import {
    assert, panic, failure, wrapInt64,
    Int64, Uns32, Flt64, Bytes, Wasm,
    ValueOnStack,
    Concat, ConstU32, ConstI64, ConstF64, IfThen,
    TypeBool, TypeI64, TypeF64, TypeCF64,
    Assembler,
    Environment,
    StoragePool, StorageCell,
    Lexical, Expression, 
    Statement, Parameter,
    Identifier, SpecificType,
    Instruction, Placement,
} from "./index.ts";

export class ConstantBool implements Expression {
    // This is for builtin true/false, so no real line numbers
    row: number = -1;
    col: number = -1;

    constructor(
        public value: Uns32,
    ) {}

    emitExpr(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): [Instruction, Placement[]] {
        return [
            new ConstU32(this.value), 
            [new ValueOnStack(TypeBool)]
        ];
    }
}

export class RefItem implements Expression {
    constructor(
        public row: number,
        public col: number,
        public base: Expression,
        public list: Expression[],
    ) {}

    emitExpr(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): [Instruction, Placement[]] {
        panic("not implemented yet");
    }
}

export class RefLane implements Expression {
    constructor(
        public row: number,
        public col: number,
        public base: Expression,
        public lane: Int64,
    ) {}

    emitExpr(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): [Instruction, Placement[]] {
        panic("not implemented yet");
    }
}

export class RefAttr implements Expression {
    constructor(
        public row: number,
        public col: number,
        public base: Expression,
        public attr: string,
    ) {}

    emitExpr(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): [Instruction, Placement[]] {
        const [baseCode, basePlaces] = this.base.emitExpr(
            assem, env, pool, temp
        );
        if (basePlaces.length !== 1) failure(
            this.row, this.col, "expected single value for lhs of attribute"
        );
        const [attrCode, attrPlace] = basePlaces[0].emitAttr(
            this.row, this.col, baseCode, this.attr
        );
        return [ attrCode, [attrPlace] ];
    }
}

export class Ternary implements Expression {
    constructor(
        public row: number,
        public col: number,
        public cond: Expression,
        public then: Expression,
        public other: Expression,
    ) {}

    emitExpr(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): [Instruction, Placement[]] {
        panic("not implemented yet");
    }
}

export class OrExpr implements Expression {
    constructor(
        public row: number,
        public col: number,
        public name: string,
        public args: Expression[],
    ) {}

    emitExpr(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): [Instruction, Placement[]] {
        const [lhsCode, lhsPlace] = emitBool(
            assem, env, pool, temp, this.args[0]
        );
        const [rhsCode, rhsPlace] = emitBool(
            assem, env, pool, temp, this.args[1]
        );

        return [
            new IfThen(Wasm.U32, lhsCode, new ConstU32(1), rhsCode),
            [new ValueOnStack(TypeBool)]
        ];
    }
}

export class AndExpr implements Expression {
    constructor(
        public row: number,
        public col: number,
        public name: string,
        public args: Expression[],
    ) {}

    emitExpr(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): [Instruction, Placement[]] {
        const [lhsCode, lhsPlace] = emitBool(
            assem, env, pool, temp, this.args[0]
        );
        const [rhsCode, rhsPlace] = emitBool(
            assem, env, pool, temp, this.args[1]
        );

        return [
            new IfThen(Wasm.U32, lhsCode, rhsCode, new ConstU32(0)),
            [new ValueOnStack(TypeBool)]
        ];
    }
}

export class CallExpr implements Expression {
    constructor(
        public row: number,
        public col: number,
        public base: string | Expression,
        public args: (Expression | NamedArg)[],
    ) {}

    emitExpr(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): [Instruction, Placement[]] {
        const argCodes = [];
        const argPlaces = [];
        const argNames = [];
        const posTypes = [];

        for (const arg of this.args) {
            if (arg instanceof NamedArg) {
                const [code, place] = emitValue(
                    assem, env, pool, temp, arg.expr
                );
                argCodes.push(code);
                argPlaces.push(place);
                argNames.push(arg.name);
            } else {
                const [code, place] = emitValue(
                    assem, env, pool, temp, arg
                );
                argCodes.push(code);
                argPlaces.push(place);
                argNames.push(null);
                posTypes.push(place.type);
            }
        }

        if (typeof this.base === 'string') {
            // Do overload resolution on named function
            const call = env.getCall(
                this.row, this.col, this.base, posTypes
            );
            return call.emitCall(
                this.row, this.col, assem,
                argCodes, argPlaces, argNames
            );
        }

        // XXX: Evaluate base and call it directly
        panic("not implemented yet");
    }
}

export class NamedArg {
    constructor(
        public row: number,
        public col: number,
        public name: string,
        public expr: Expression
    ) {}
}

export class LambdaFunc implements Expression {
    constructor(
        public row: number,
        public col: number,
        public params: Parameter[],
        public results: null | Expression[],
        public stmts: null | Statement[],
    ) {}


    emitExpr(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): [Instruction, Placement[]] {
        panic("not implemented yet");
    }
}

export function emitValue(
    assem: Assembler, env: Environment,
    pool: StoragePool, temp: StoragePool,
    expr: Expression
): [Instruction, Placement] {
    const [code, places] = expr.emitExpr(
        assem, env, pool, temp
    );
    if (places.length == 0) failure(expr.row, expr.col,
        "expression does not provide a value"
    );
    if (places.length > 1) failure(expr.row, expr.col,
        "expression provides multiple values"
    );
    const [codeOnStack, placeOnStack] = places[0].rvalue();
    return [new Concat(code, codeOnStack), placeOnStack];
}

export function emitBool(
    assem: Assembler, env: Environment,
    pool: StoragePool, temp: StoragePool,
    expr: Expression
): [Instruction, Placement] {
    const [code, place] = emitValue(assem, env, pool, temp, expr);
    if (!place.type.isTypeEqual(TypeBool)) failure(
        expr.row, expr.col, "expected boolean expression"
    );
    return [code, place];
}

