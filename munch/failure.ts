// Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

//
// Both assert() and panic() throw Error for bugs in the compiler itself,
// failure() and throws Failure for bugs in the program being compiled.
//

export function assert(
    condition: boolean, message="assertion failure",
): asserts condition {
    if (!condition) throw Error(message);
}

export function panic(message: string): never {
    // internal error
    throw new Error(message);
}

export class Failure {
    constructor(
        public row: number, 
        public col: number, 
        public msg: string,
    ) {}
}

export function failure(
    row: number, col: number, msg: string
): never {
    // compilation error
    throw new Failure(row, col, msg);
}

