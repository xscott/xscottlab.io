// Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

import {
    Overloadable, Callable, Statement, Expression, Identifier,
    SpecificType, Placement, StoragePool, StorageCell,
    ValueInCell, HandleInCell,
    Instruction, emitBool,
    assert, panic, failure,
    Concat, ForLoop, IfThen, Nop,
    Environment,
    TypeBool,
    Assembler,
    NamedArg,
} from "./index.ts";

export class FuncStmt implements Statement, Callable, Overloadable {
    constructor(
        public row: number,
        public col: number,
        public name: string,
        public params: Parameter[],
        public results: null | Expression[],
        public stmts: null | Statement[],
    ) {}

    declare(env: Environment): void {
        panic("WE NEED THIS");
    }

    emitStmt(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): Instruction {
        panic("not implemented yet");
    }

    matches(posTypes: SpecificType[]): boolean {
        // return false if argument/parameter types don't match
        panic("not implemented yet");
    }

    emitCall(
        row: number, col: number,
        assem: Assembler,
        argCodes: Instruction[],
        argPlaces: Placement[],
        argNames: (null | string)[],
    ): [Instruction, Placement[]] {
        panic("Callable not implemented yet for FuncStmt");
    }
}

export class Parameter {
    // When name === null:  reqParams, Required Parameter
    // When name !== null:  optParams, Optional Parameter
    constructor(
        public row: number,
        public col: number,
        public name: string,
        public type: Expression,
        public expr: null | Expression,
    ) {}
}

export class VarStmt implements Statement {
    constructor(
        public row: number,
        public col: number,
        public lhs: Variable[],
        public rhs: Expression[],
    ) {}

    declare(env: Environment): void {
        for (const vv of this.lhs) {
            env.defName(vv.row, vv.col, vv.name, vv);
        }
    }

    emitStmt(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): Instruction {
        if (this.lhs.length === 0) failure(this.row, this.col,
            "expected a variable declaration with some variables"
        );
        if (this.rhs.length === 1) {
            // There's a single expression on the right-hand-side, so it must
            // return the same number of resutls as we have variables on the
            // left-hand-size.
            const [code, places] = this.rhs[0].emitExpr(
                assem, env, pool, temp
            );
            if (this.lhs.length !== places.length) failure(this.row, this.col,
                "rhs expression must return the same number as lhs variables"
            );
            return this.#emitHelper(assem, env, pool, temp, code, places);
        } else {
            // We need to have the same number of expressions on the right as
            // we do variables on the left, and each expression must be single
            // valued.
            if (this.lhs.length !== this.rhs.length) failure(
                this.row, this.col,
                "number of expressions and variables must match"
            );
            const codeList: Instruction[] = [];
            const placeList: Placement[] = [];
            for (const expr of this.rhs) {
                const [code, places] = expr.emitExpr(assem, env, pool, temp);
                if (places.length !== 1) failure(expr.row, expr.col,
                    "each expression on rhs must be single valued"
                );
                codeList.push(code);
                placeList.push(places[0]);
            }
            const code = new Concat(... codeList);
            return this.#emitHelper(assem, env, pool, temp, code, placeList);
        }
    }

    #emitHelper(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
        code: Instruction, places: Placement[]
    ): Instruction {
        assert(this.lhs.length === places.length);
        const codeList: Instruction[] = [code];

        // Type inference or type expression, and get a cell
        for (let ii = 0; ii < this.lhs.length; ++ii) {
            if (this.lhs[ii].typeExpr === null) {
                this.lhs[ii].typeSpec = places[ii].type;
            } else {
                // XXX: this.lhs[ii].typeExpr.evalType()
                panic("We don't really have typeExpr yet...");
            }
            this.lhs[ii].cell = pool.alloc(places[ii].type.workingType());
        }

        // We run this loop backwards to pop values off the stack and assign
        // them to their storage pool.  This order of operations should not be
        // visible from the behavior of the program.
        for (let ii = this.lhs.length - 1; ii >= 0; --ii) {
            const [stackCode, stackPlacement] = places[ii].rvalue();
            codeList.push(stackCode);
            const cell = this.lhs[ii].cell;
            assert(cell !== null);
            codeList.push(cell.emitSet());
        }
        return new Concat(... codeList);
    }
}

export class Variable implements Expression {
    typeSpec: null | SpecificType = null;
    cell: null | StorageCell = null;
    visibleRow: number = -1;
    visibleCol: number = -1;

    constructor(
        public row: number,
        public col: number,
        public name: string,
        public typeExpr: null | Expression,
    ) {}

    emitExpr(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): [Instruction, Placement[]] {
        assert(this.typeSpec !== null);
        assert(this.cell !== null);
        const place = (this.typeSpec.isPrimitive() ?
            new ValueInCell(this.typeSpec, this.cell) :
            new HandleInCell(this.typeSpec, this.cell)
        );
        return [new Nop(), [place]];
    }
}

export class IfElseStmt implements Statement {
    constructor(
        public row: number,
        public col: number,
        public cond: Expression,
        public thenStmts: Statement[],
        public elseStmts: Statement[],
    ) {}

    declare(env: Environment): void {}

    emitStmt(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): Instruction {
        const type = "\x40"; // VOID
        const [condCode, condPlace] = emitBool(
            assem, env, pool, temp, this.cond
        );
        const thenCode = emitBlock(assem, env, pool, temp, this, this.thenStmts);
        const elseCode = emitBlock(assem, env, pool, temp, this, this.elseStmts);
        return new IfThen(type, condCode, thenCode, elseCode);
    }
}

export class WhileStmt implements Statement {
    constructor(
        public row: number,
        public col: number,
        public label: null | string,
        public cond: Expression,
        public body: Statement[],
    ) {}

    declare(env: Environment): void {}

    emitStmt(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): Instruction {
        const [condCode, condPlace] = emitBool(
            assem, env, pool, temp, this.cond
        );
        const bodyCode = emitBlock(assem, env, pool, temp, this, this.body);
        return new ForLoop(
            this.label, new Nop(), condCode, new Nop(), bodyCode
        );
    }
}

export class BreakStmt implements Statement {
    constructor(
        public row: number,
        public col: number,
        public label: null | string,
    ) {}

    declare(env: Environment): void {}

    emitStmt(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): Instruction {
        return env.emitJump(this.row, this.col, this.label, 2);
    }
}

export class ContinueStmt implements Statement {
    constructor(
        public row: number,
        public col: number,
        public label: null | string,
    ) {}

    declare(env: Environment): void {}

    emitStmt(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): Instruction {
        return env.emitJump(this.row, this.col, this.label, 0);
    }
}

export class ReturnStmt implements Statement {
    constructor(
        public row: number,
        public col: number,
        public list: Expression[],
    ) {}

    declare(env: Environment): void {}

    emitStmt(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): Instruction {
        panic("not implemented yet");
    }
}

export class ExprStmt implements Statement {
    constructor(
        public row: number,
        public col: number,
        public expr: Expression,
    ) {}

    declare(env: Environment): void {}

    emitStmt(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): Instruction {
        const [code, places] = this.expr.emitExpr(
            assem, env, pool, temp
        );
        const discards = places.reverse().map(
            (place: Placement) => place.discard()
        );
        return new Concat(code, ... discards);
    }
}

export class AssignStmt implements Statement {
    constructor(
        public row: number,
        public col: number,
        public lhs: Expression[],
        public rhs: Expression[],
    ) {}

    declare(env: Environment): void {}

    emitStmt(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): Instruction {
        const codes: Instruction[] = [];
        const places: Placement[] = [];

        if (this.rhs.length === 1) {
            // If there's a single rhs, it must return the same number of
            // values as locations to store them on the lhs.
            const [rhsCode, rhsPlaces] = this.rhs[0].emitExpr(
                assem, env, pool, temp
            );
            if (rhsPlaces.length !== this.lhs.length) failure(
                this.row, this.col,
                "rhs returns different number of values than lhs"
            );
            codes.push(rhsCode);
            places.push(... rhsPlaces);
        } else {
            // There are multiple rhs.  They must be the same number as
            // locations on the lhs, and each rhs must be single valued.
            if (this.rhs.length !== this.lhs.length) failure(
                this.row, this.col,
                "need the same number of rhs values as lhs locations"
            );
            for (let ii = 0; ii<this.rhs.length; ++ii) {
                const [rhsCode, rhsPlaces] = this.rhs[ii].emitExpr(
                    assem, env, pool, temp
                );
                if (rhsPlaces.length !== 1) failure(this.row, this.col,
                    "each rhs expression must provide one value"
                );
                codes.push(rhsCode);
                const [rvalueCode, rvaluePlace] = rhsPlaces[0].rvalue();
                codes.push(rvalueCode);
                places.push(rvaluePlace);
            }
        }

        const saved: StorageCell[] = [];

        // TODO: We could eliminate a bunch of sets and gets of rhs values to
        // temp StorageCells if we could detect when there are no possible
        // order dependencies between the lhs locations.  So: No side effects,
        // no assigning to the same StorageCell, and no assigning to the same
        // addresses.  A common case might be storing to unique ValueInCells.
        // But really, no idea how common multiple assignments will really be.

        // We evaluated all of the rhs in order, and we need to evaluate lhs in
        // order too.  This means we save all but the first rhs from the stack
        // to a temporary cell.  No overhead for assigning one value.
        for (let ii = places.length - 1; ii>0; --ii) {
            const cell = temp.alloc(places[ii].type.workingType());
            codes.push(cell.emitSet());
            saved.push(cell);
        }

        // Handle the first lhs.  It's rhs is on the stack.
        const [lhsCode, lhsPlaces] = this.lhs[0].emitExpr(
            assem, env, pool, temp
        );
        if (lhsPlaces.length !== 1) failure(this.row, this.col,
            "lhs expressions must be single valued"
        );
        if (!lhsPlaces[0].type.isTypeEqual(places[0].type)) failure(
            this.row, this.col, "type mismatch in assignment"
        );
        codes.push(lhsCode);
        codes.push(lhsPlaces[0].assign(this.row, this.col, places[0]));

        // Handle the remaining lhs.  Their rhs are in saved cells.
        for (let ii = 1; ii<this.lhs.length; ++ii) {
            const cell = saved.pop();
            assert(cell !== undefined);
            codes.push(cell.emitGet());
            temp.free(cell);
            const [lhsCode, lhsPlaces] = this.lhs[ii].emitExpr(
                assem, env, pool, temp
            );
            if (lhsPlaces.length !== 1) failure(this.row, this.col,
                "lhs expressions must be single valued"
            );
            if (!lhsPlaces[0].type.isTypeEqual(places[ii].type)) failure(
                this.row, this.col, "type mismatch in assignment"
            );
            codes.push(lhsCode);
            codes.push(lhsPlaces[0].assign(this.row, this.col, places[ii]));
        }

        return new Concat(... codes);
    }
}

export class BlockStmt implements Statement {
    constructor(
        public row: number,
        public col: number,
        public list: Statement[],
    ) {}

    declare(env: Environment): void {}

    emitStmt(
        assem: Assembler,
        env: Environment,
        pool: StoragePool,
        temp: StoragePool,
    ) {
        return emitBlock(assem, env, pool, temp, this, this.list);
    }
}

function emitBlock(
    assem: Assembler,
    parent: Environment,
    pool: StoragePool,
    temp: StoragePool,
    owner: Statement,
    stmts: Statement[]
) {
    const env = new Environment(parent, owner);
    stmts.forEach(stmt => stmt.declare(env));
    const code = new Concat(
        ... stmts.map(
            (stmt: Statement) => stmt.emitStmt(assem, env, pool, temp)
        )
    );
    // XXX: env.emitClose(); Need destructors
    return code;
}


