// Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

import {
    failure, panic,
    Instruction, Placement, SpecificType, StorageCell,
    GetLaneF64x2,
    ValueOnStack, FieldInCell,
    Bytes,
    Wasm,
} from "./index.ts";

export const TypeBool: SpecificType = {
    isTypeEqual(that) { return this === that; },
    isPrimitive() { return true; },
    workingType() { return Wasm.U32; },
    memoryBytes() { return 1; },

    valueOnStackAttr(
        row: number,
        col: number,
        base: Instruction,
        attr: string
    ): [Instruction, Placement] {
        failure(row, col, "bool does not have attributes");
    },

    valueInCellAttr(
        row: number,
        col: number,
        cell: StorageCell,
        base: Instruction,
        attr: string
    ): [Instruction, Placement] {
        failure(row, col, "bool does not have attributes");
    },
} as const;

export const TypeI64: SpecificType = {
    isTypeEqual(that) { return this === that; },
    isPrimitive() { return true; },
    workingType() { return Wasm.I64; },
    memoryBytes() { return 8; },

    valueOnStackAttr(
        row: number,
        col: number,
        base: Instruction,
        attr: string
    ): [Instruction, Placement] {
        failure(row, col, "i64 does not have attributes");
    },

    valueInCellAttr(
        row: number,
        col: number,
        cell: StorageCell,
        base: Instruction,
        attr: string
    ): [Instruction, Placement] {
        failure(row, col, "i64 does not have attributes");
    },
} as const;

export const TypeF64: SpecificType = {
    isTypeEqual(that) { return this === that; },
    isPrimitive() { return true; },
    workingType() { return Wasm.F64; },
    memoryBytes() { return 8; },

    valueOnStackAttr(
        row: number,
        col: number,
        base: Instruction,
        attr: string
    ): [Instruction, Placement] {
        failure(row, col, "f64 does not have attributes");
    },

    valueInCellAttr(
        row: number,
        col: number,
        cell: StorageCell,
        base: Instruction,
        attr: string
    ): [Instruction, Placement] {
        failure(row, col, "f64 does not have attributes");
    },
} as const;

export const TypeCF64: SpecificType = {
    isTypeEqual(that) { return this === that; },
    isPrimitive() { return true; },
    workingType() { return Wasm.V128; }, // f64x2
    memoryBytes() { return 16; },

    valueOnStackAttr(
        row: number,
        col: number,
        base: Instruction,
        attr: string
    ): [Instruction, Placement] {
        if (attr === "real") return [
            new GetLaneF64x2(base, 0),
            new ValueOnStack(TypeF64),
        ];
        if (attr === "imag") return [
            new GetLaneF64x2(base, 1),
            new ValueOnStack(TypeF64),
        ];
        failure(row, col, "cf64 does not have attribute: " + attr);
    },

    valueInCellAttr(
        row: number,
        col: number,
        cell: StorageCell,
        base: Instruction,
        attr: string,
    ): [Instruction, Placement] {
        if (attr === "real") return [
            base, new FieldInCell(TypeF64, cell, GetLaneF64x2, 0)
        ];
        if (attr === "imag") return [
            base, new FieldInCell(TypeF64, cell, GetLaneF64x2, 1)
        ];
        failure(row, col, "cf64 does not have attribute: " + attr);
    },
} as const;


