// Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

//
// This prints strings, Uint8Arrays, and recursive objects nicely.
//

export function debug(value: unknown): void {
    if (value instanceof Uint8Array) {
        console.log("hexdump  length:", value.length, " bytes:");
        for (let ii = 0; ii < value.length; ii += 16) {
            const offset = ii.toString(10).padStart(6, ' ');
            const bytes = Array.from(value.slice(ii, ii + 16))
                .map(byte => byte.toString(16).padStart(2, '0'))
                .join(' ');
            console.log(`${offset}  ${bytes}`);
        }
        return;
    }

    if (typeof value === 'string') {
        console.log("string length:", value.length);
        let pretty = "\t\"";
        for (let ii = 0; ii<value.length; ++ii) {
            const code = value.charCodeAt(ii);
            if (32 <= code && code <= 127) {
                pretty += value[ii];
                continue;
            }
            if (code <= 0xFF) {
                pretty += "\\x" + code.toString(16).padStart(2, '0');
                continue;
            }
            pretty += "\\u" + code.toString(16).padStart(4, '0');
        }
        pretty += "\"";
        console.log(pretty);
        return;
    }

    console.dir(value, { depth: Infinity, colors: true });
}

