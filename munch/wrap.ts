
import {
    Int64, Uns32, assert,
} from "./index.ts";

export function wrapInt64(value: bigint): Int64 {
    const HALF = 1n<<63n;
    const MASK = (1n<<64n) - 1n;
    return ((value + HALF) & MASK) - HALF;
}

export function wrapUns32(value: number): Uns32 {
    assert(Number.isSafeInteger(value));
    // This wraps and returns in [0, 2**32 - 1]
    return value >>> 0;
}

