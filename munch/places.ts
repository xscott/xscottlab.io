// Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

import {
    panic, assert, failure, Uns32,
    Nop, Drop, Concat, Placement, Instruction,
    LaneGetter,
    //LaneSetter,
    SpecificType, StorageCell, Variable,
} from "./index.ts";

export class ValueOnStack implements Placement {
    constructor(
        public type: SpecificType
    ) {}

    discard(): Instruction { return new Drop(); }

    rvalue(): [Instruction, Placement] {
        return [new Nop(), this];
    }

    assign(row: number, col: number, src: Placement): Instruction {
        failure(row, col, "Can not assign to expression");
        panic("not implemented yet");
    }

    emitAttr(
        row: number, col: number,
        base: Instruction, attr: string
    ): [Instruction, Placement] {
        return this.type.valueOnStackAttr(row, col, base, attr);
    }

}

export class ValueInCell implements Placement {
    constructor(
        public type: SpecificType,
        public cell: StorageCell,
    ) {}

    discard(): Instruction { return new Nop(); }

    rvalue(): [Instruction, Placement] {
        return [
            this.cell.emitGet(),
            new ValueOnStack(this.type)
        ]
    }

    assign(row: number, col: number, src: Placement): Instruction {
        if (src instanceof ValueOnStack) {
            return this.cell.emitSet();
        }

        if (src instanceof ValueInCell) {
            return new Concat(
                src.cell.emitGet(),
                this.cell.emitSet()
            );
        }

        panic("unhandled assignment to ValueInCall");
    }

    emitAttr(
        row: number, col: number,
        base: Instruction, attr: string
    ): [Instruction, Placement] {
        return this.type.valueInCellAttr(row, col, this.cell, base, attr);
    }
}

export class FieldInCell implements Placement {
    constructor(
        public type: SpecificType,
        public cell: StorageCell,
        public getter: LaneGetter,
        //public setter: LaneSetter,
        public lane: Uns32,
    ) {}

    discard(): Instruction { return new Nop(); }

    rvalue(): [Instruction, Placement] {
        return [
            new this.getter(this.cell.emitGet(), this.lane),
            new ValueOnStack(this.type)
        ]
    }

    assign(row: number, col: number, src: Placement): Instruction {
        panic("assignment to FieldInCell not implemented yet");
    }

    emitAttr(
        row: number, col: number,
        base: Instruction, attr: string
    ): [Instruction, Placement] {
        // XXX: Will we have nested attributes?
        panic("emitAttr for FieldInCell not implemented yet");
    }
}

export class HandleInCell implements Placement {
    constructor(
        public type: SpecificType,
        public cell: StorageCell,
    ) {}

    discard(): Instruction { return new Nop(); }

    rvalue(): [Instruction, Placement] {
        assert(this.cell !== null);
        panic("need to incref the value after");
        /*
        const code = new Concat(
            this.cell.emitGet(),
            new Incref()
        );
        return [
            this.cell.emitGet(),
            new ValueOnStack(this.type)
        ]
        */
    }

    assign(row: number, col: number, src: Placement): Instruction {
        panic("assignment to HandleInCell not implemented yet");
    }

    emitAttr(
        row: number, col: number,
        base: Instruction, attr: string
    ): [Instruction, Placement] {
        panic("emitAttr to HandleInCell not implemented yet");
    }
}

