
%token
    VAR FUNC IF ELIF ELSE WHILE RETURN BREAK CONTINUE
    LPAREN RPAREN LCURLY RCURLY LBRACE RBRACE
    COMMA EQUAL COLON ARROW QUEST SEMI
    DOT OR AND NOT EQ NE LT LE GT GE ADD SUB
    MUL DIV MOD VID ATS POW TICK TILDE
    I64 F64 BOOL STR IDENT

%%

stmtlist
    : // zero or more
    | stmtlist statement
    ;

statement
    : FUNC funcdecl
    | VAR vardecl
    | IF ifthenstmt
    | WHILE whilestmt
    | RETURN returnstmt
    | BREAK       SEMI
    | BREAK IDENT SEMI
    | CONTINUE       SEMI
    | CONTINUE IDENT SEMI
    | blockstmt
    | assignment SEMI
    | expression SEMI
    | SEMI // empty statements
    ;

funcdecl
    : funcname LPAREN paramlist RPAREN COLON resultlist blockstmt
    | funcname LPAREN paramlist RPAREN                  blockstmt
    ;

funcname
    : OR | AND | EQ | NE | LT | LE | GT | GE | ADD | SUB
    | MUL | DIV | MOD | VID | ATS | POW | NOT | TICK | TILDE
    | IDENT
    ;

paramlist
    : parameter COMMA paramlist
    | parameter
    | // zero or more
    ;

parameter
    : IDENT COLON expression
    | IDENT COLON expression EQUAL expression
    ;

resultlist
    : expression COMMA resultlist
    | expression
    | // zero or more
    ;

blockstmt
    : LCURLY stmtlist RCURLY
    ;

vardecl
    : varlist EQUAL exprlist SEMI
    ;

varlist
    : variable COMMA varlist
    | variable
    | // zero or more
    ;

variable
    : IDENT COLON expression
    | IDENT
    ;

ifthenstmt
    : expression blockstmt elifelse
    ;

elifelse
    : ELIF expression blockstmt elifelse
    | ELSE blockstmt
    | // empty
    ;

whilestmt
    :             expression blockstmt
    | COLON IDENT expression blockstmt
    ;

returnstmt
    : RETURN returnlist SEMI
    ;

returnlist
    : expression COMMA returnlist
    | expression
    | // zero or more
    ;

assignment
    : exprlist EQUAL exprlist SEMI
    ;

exprlist
    : IDENT COMMA exprlist
    | IDENT
    | // zero or more
    ;

expression
    : lambdafunc
    ;

lambdafunc
    : LPAREN paramlist RPAREN                  ARROW lambdafunc
    | LPAREN paramlist RPAREN COLON resultlist ARROW blockstmt
    | ternary
    ;

ternary
    : binor QUEST expression COLON expression
    | binor
    ;

binor // lassoc
    : binor OR binand
    | binand
    ;

binand // lassoc
    : binand AND bincmp
    | bincmp
    ;

cmpops: EQ | NE | LT | LE | GT | GE;

bincmp // nassoc
    : binadd cmpops binadd
    | binadd
    ;

binadd // lassoc
    : binadd ADD binmul
    | binadd SUB binmul
    | binmul
    ;

mulops: MUL | DIV | MOD | VID | ATS;

binmul // lassoc
    : binmul mulops prefix
    | prefix
    ;

prefix
    : ADD prefix
    | SUB prefix
    | NOT prefix
    | TILDE prefix
    | binpow
    ;

binpow // rassoc
    : suffix POW prefix
    | suffix
    ;

suffix
    : suffix TICK
    | suffix DOT IDENT
    | suffix DOT I64
    | suffix LPAREN arglist RPAREN
    | suffix LBRACE sublist RBRACE
    | primary
    ;

arglist
    : argument COMMA arglist
    | argument
    | // zero or more
    ;

argument
    : expression
    | namedarg
    ;

namedarg
    : IDENT EQUAL expression
    ;

sublist
    : subscript COMMA sublist
    | subscript
    | // zero or more
    ;

subscript
    : expression
    ;

primary
    : paren
    | I64 | F64 | BOOL | STR | IDENT 
    ;

paren
    : LPAREN expression RPAREN
    | LPAREN typelist ARROW typelist RPAREN
    ;

typelist
    : expression COMMA typelist
    | expression
    | // zero more
    ;

