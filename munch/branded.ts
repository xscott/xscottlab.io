// Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

//
// These aren't substantially better than using simple type aliases for
// documentation purposes.  They revert to the associated primitive type when a
// light wind blows.  They do force functions and methods to declare their
// intentions in a very visible way, but they don't enforce any real safety at
// compile time.  I end up just adding `as TypeXX` to silence the errors.  I'm
// keeping it here in cases I get ambitious enough to try using them again.
// 
// declare const brand: unique symbol;
// type Branded<Type, Name> = Type & { readonly [brand]: Name };
// 
// export type Int64 = Branded<bigint, "Int64">;
// export type Uns32 = Branded<number, "Uns32">;
// export type Flt32 = Branded<number, "Flt32">;
// export type Flt64 = Branded<number, "Flt64">;
// export type Bytes = Branded<string, "Bytes">;
//

export type Int64 = bigint;
export type Uns32 = number;
export type Flt32 = number;
export type Flt64 = number;
export type Bytes = string;

