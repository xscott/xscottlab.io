// Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

import {
    assert, panic, failure,
    Pack,
    Int64, Uns32, Flt64, Bytes, 
    Lexical,
    wrapInt64,
    Expression,
    Assembler, Environment, StoragePool, StorageCell,
    Instruction, Placement,
    ConstI64, ConstF64, ConstV128, ValueOnStack,
    TypeI64, TypeF64, TypeCF64,
} from "./index.ts";

export function tokenize(lines: string[]): Lexical[] {
    const spaces = /[ \t\r]*/y;
    const floating: RegExp = new RegExp(
        "[0-9]"                    + // required leading digit
        "[0-9_]*"                  + // digits or underscores
        "[.]"                      + // required decimal point
        "[0-9]"                    + // required decimal digit
        "[0-9_]*"                  + // digits or underscores
        "(?:[eE][-+]?[0-9_]+)?"    + // optional exponent
        "j?"                       + // complex imaginary
        "(?![a-zA-Z.])"            , // no alpha or dot
        "y"
    );
    const integer: RegExp = new RegExp(
        "(?:"                      + // pick one of {
            "(0b[01_]+)|"          + //     binary
            "(0x[0-9a-fA-F_]+)|"   + //     hexadecimal
            "([1-9][0-9_]*)|"      + //     decimal
            "0"                    + //     plain zero
        ")"                        + // }
        "(?![a-zA-Z.])"            , // no alpha or dot
        "y"
    );
    const keyword: RegExp = new RegExp(
        // TODO:
        //       type struct union
        //       for in to by
        //       assert
        "func|var|if|elif|else|" +
        "while|return|break|continue",
        "y"
    );
    const identifier: RegExp = /[a-zA-Z_][a-zA-Z_0-9]*/y;
    const punctuation: RegExp = new RegExp((
            "[ ] ( ) { } . , ; : ? " +
            "=== == => = !== != ! <= < >= > " +
            "| & + - * / % \\ @ ^ ' ~"
        ).split(" ").map(s => "\\" + s.split("").join("\\")).join("|"),
        "y"
    );

    type LexicalConstructor = new (
        row: number, col: number, text: string
    ) => Lexical;

    const table: [RegExp, LexicalConstructor][] = [
        // the order of these matters
        [floating,    LiteralF64],
        [integer,     LiteralI64],
        [keyword,     Keyword],
        [identifier,  Identifier],
        [punctuation, Punctuation],
    ];

    let row = 0, col = 0;
    const tokens = [];

    loop: for (;;) {
        const line = lines[row];
        if (line === undefined) break;

        spaces.lastIndex = col;
        spaces.exec(line);
        col = spaces.lastIndex;
        const chr = line[col];
        if (chr === undefined || chr === "#") {
            ++row;
            col = 0;
            continue;
        }
        if (chr === '"') {
            const [str, end] = string(line, row, col);
            col = end;
            tokens.push(str);
            continue;
        }

        for (const [regex, ctor] of table) {
            regex.lastIndex = col;
            const match = regex.exec(line);
            if (match) {
                tokens.push(new ctor(row, col, match[0]));
                col = regex.lastIndex;
                continue loop;
            }
        }

        failure(row, col, "unexpected lexical syntax");
    }

    return tokens;

    function string(
        line: string, row: number, col: number
    ): [LiteralStr, number] {
        // Any web pages I care about will be encoded in UTF-8, but when we ask
        // for the source to the script we're compiling it will be given to us
        // as a JavaScript UTF-16 string.  The tokenizer for most program
        // tokens will only accept ASCII for delimiters, identifiers, and
        // operators, but I'm going to allow full unicode in comments and
        // strings.
        //
        // However, Crunch strings are not strictly UTF-8.  Much like the Go
        // programming language, Crunch allows any 8-bit binary value in them.
        // So \xHH escape sequences will insert any byte, \uHHHH and \UHHHHHH
        // will insert the corresponding UTF-8 bytes, and UTF-16 Code Points
        // (possibly as a surrogate pair) from the source code will be
        // converted to UTF-8 sequences of bytes.
        //
        // So looking at the whole picture, we've got a UTF-8 web page
        // converted to a UTF-16 string, scanned/parsed into a sequence of
        // bytes, and stored in a UTF-16 string that only uses values/codes in
        // the range of 0 to 255.  See the Pack class for the motivation
        // on using JavaScript String to store 8 bit bytes.

        const BACK_SLASH = 0x5C, DOUBLE_QUOTE = 0x22, NEWLINE = 0x0A;
        let bytes = "";
        let ptr = col + 1;
        for (;;) {
            // Note: strings and comments are allowed to have unicode,
            // but the rest of the source code is expected to be ascii.
            const ord = line.codePointAt(ptr);
            if (ord === undefined) failure(
                row, col, "unterminated string literal"
            );
            if (ord === DOUBLE_QUOTE) { break; }
            if (ord === NEWLINE) failure(row, ptr,
                "unexpected newline in string literal"
            );
            if (ord === BACK_SLASH) {
                switch (line[ptr + 1]) {
                    case  'n': { bytes += "\n"; ptr += 2; continue; }
                    case  't': { bytes += "\t"; ptr += 2; continue; }
                    case  '0': { bytes += "\0"; ptr += 2; continue; }
                    case  '"': { bytes += "\""; ptr += 2; continue; }
                    case '\\': { bytes += "\\"; ptr += 2; continue; }
                    case  'x': {
                        const value = hexnum(line, row, ptr + 2, 2);
                        bytes += String.fromCodePoint(value);
                        ptr += 4;
                        continue;
                    }
                    case  'u': {
                        const value = hexnum(line, row, ptr + 2, 4);
                        bytes += Pack.utf8(value);
                        ptr += 6;
                        continue;
                    }
                    case  'U': {
                        const value = hexnum(line, row, ptr + 2, 6);
                        bytes += Pack.utf8(value);
                        ptr += 8;
                        continue;
                    }
                    default: failure(
                        row, ptr, "unexpected character as escape sequence"
                    );
                }
            }
            // convert the ordinal to UTF-8 bytes, and increment
            // by 2 if the source code had a surrogates pair
            bytes += Pack.utf8(ord);
            ptr += ord > 0xFFFF ? 2 : 1;
        }
        const text = line.slice(col + 1, ptr);
        return [new LiteralStr(row, col, text, bytes), ptr + 1];
    }

    function hexnum(
        line: string, row: number,
        col: number, len: number,
    ): number {
        const hex = line.slice(col, col + len);
        for (let ii = 0; ii<len; ++ii) {
            if (!/[0-9a-fA-F]/.test(hex[ii])) {
                failure(row, col + ii, "expected hex digit");
            }
        }
        return parseInt(hex, 16);
    }
}


export class Keyword implements Lexical {
    constructor(
        public row: number,
        public col: number,
        public text: string,
    ) {}
}

export class Punctuation implements Lexical {
    constructor(
        public row: number,
        public col: number,
        public text: string,
    ) {}
}

export class LiteralI64 implements Lexical, Expression {
    value: Int64;

    constructor(
        public row: number,
        public col: number,
        public text: string,
    ) {
        // XXX: Range checks, We'll probably allow up to 0xFFF...FFF
        const bigint = BigInt(text.replaceAll('_', ''));
        this.value = wrapInt64(bigint);
    }

    emitExpr(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): [Instruction, Placement[]] {
        return [
            new ConstI64(this.value), 
            [new ValueOnStack(TypeI64)]
        ];
    }
}

export class LiteralF64 implements Lexical, Expression {
    text: string;
    real: Flt64;

    constructor(
        public row: number,
        public col: number,
        arg: string | number,
    ) {
        if (typeof arg === 'string') {
            const value = parseFloat(arg.replaceAll('_', '')); 
            this.text = arg;
            this.real = value;
            if (arg.endsWith("j")) {
                // Complex imaginary number.  Even though we're converting to
                // LiteralCF6, this needs to go after the initialization for
                // the LiteralF64 so that TypeScript will accept it.
                return new LiteralCF64(row, col, arg, 0.0, value);
            }
        } else {
            assert(typeof(arg) === 'number');
            this.text = '<constant folding>';
            this.real = arg;
        }
    }

    emitExpr(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): [Instruction, Placement[]] {
        return [
            new ConstF64(this.real), 
            [new ValueOnStack(TypeF64)]
        ];
    }
}

export class LiteralCF64 implements Lexical, Expression {
    constructor(
        public row: number,
        public col: number,
        public text: string,
        public real: number,
        public imag: number,
    ) {}

    emitExpr(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): [Instruction, Placement[]] {
        const instruction = new ConstV128();
        instruction.setF64(0, this.real);
        instruction.setF64(1, this.imag);
        return [instruction, [new ValueOnStack(TypeCF64)]];
    }
}

export class LiteralStr implements Lexical, Expression {
    constructor(
        public row: number,
        public col: number,
        public text: string,
        public bytes: Bytes,
    ) {}

    emitExpr(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): [Instruction, Placement[]] {
        return panic("not implemented yet");
    }
}

export class Identifier implements Lexical, Expression {
    constructor(
        public row: number,
        public col: number,
        public text: string,
    ) {}

    emitExpr(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): [Instruction, Placement[]] {
        const obj = env.getName(this.row, this.col, this.text);
        return obj.emitExpr(assem, env, pool, temp);
    }
}

