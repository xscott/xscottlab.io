// Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

import {
    Assembler, Bytes, Environment, NamedArg, FlowControl, Uns32
} from "./index.ts";

export type Constructor<T> = new (...args: any[]) => T;

export interface Lexical {
    row: number;
    col: number;
    text: string;
}

export interface Expression {
    row: number;
    col: number;
    emitExpr(
        assem: Assembler, env: Environment,
        pool: StoragePool, temp: StoragePool,
    ): [Instruction, Placement[]];
}

export interface Statement {
    row: number;
    col: number;

    declare(env: Environment): void;

    emitStmt(
        assem: Assembler,
        env: Environment,
        pool: StoragePool,
        temp: StoragePool,
    ): Instruction;
}

export interface Overloadable {
    matches(posTypes: SpecificType[]): boolean;
}

export interface Callable {
    emitCall(
        row: number, col: number,
        assem: Assembler, // for adapters
        argCodes: Instruction[],
        argPlaces: Placement[],
        argNames: (null | string)[],
    ): [Instruction, Placement[]];
}

export function isCallable(obj: unknown): obj is Callable {
    return obj !== null && typeof obj === 'object' && "emitCall" in obj;
}

export interface TypeExpression {
    evalType(env: Environment): SpecificType;
}

export interface GenericType {
    specialize(env: Environment, args: TypeSpecializer[]): SpecificType;
}

export interface TypeSpecializer {

}

export interface SpecificType {
    isTypeEqual(that: SpecificType): boolean;
    isPrimitive(): boolean;
    workingType(): Bytes;
    memoryBytes(): number;

    valueOnStackAttr(
        row: number,
        col: number,
        base: Instruction,
        attr: string
    ): [Instruction, Placement];

    valueInCellAttr(
        row: number,
        col: number,
        cell: StorageCell,
        base: Instruction,
        attr: string
    ): [Instruction, Placement];
}

export type LaneGetter = new (
    base: Instruction, lane: Uns32
) => Instruction;

/*
export type LaneSetter = new (
    base: Instruction, lane: Un32, rval: Instruction
) => Instruction;
*/

export interface Placement {
    type: SpecificType;

    // XXX: Will these need assem, builtins, pool, or temp?
    discard(): Instruction;
    rvalue(): [Instruction, Placement];
    assign(row: number, col: number, src: Placement): Instruction;

    emitAttr(
        row: number, col: number,
        base: Instruction, attr: string,
    ): [Instruction, Placement];

    /*
    emitLane(... _) {
    }

    emitItem(... _) {
    }
    */
}

export interface StoragePool {
    alloc(wasmType: Bytes): StorageCell;
    free(cell: StorageCell): void;
}

export interface StorageCell {
    emitGet(): Instruction;
    emitSet(): Instruction;
    emitTee(): Instruction;
}

export interface Instruction {
    optimize(): Instruction;
    emitBytes(assem: Assembler, flow: null | FlowControl): Bytes;
}

export type Imports = { [key: string]: (... args: any[]) => any };

export interface Runnable {
    run(): void;
    mem: WebAssembly.Memory;
}

