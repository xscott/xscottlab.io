

(module
    (type $sig (func (param) (result)))

    (import "env" "foo" (func $foo))
    (import "env" "bar" (func $bar))

    (elem declare funcref (ref.func $foo))
    (elem declare funcref (ref.func $bar))

    (func (export "run") (result)
        (call $foo)


        (ref.func $bar)
        (call_ref $sig)
        ;; Puts a zero in the binary to
        ;; specify the type signature:
        ;;unreachable
    )
)


;;(elem declare funcref (ref.func $log))
;;(import "env" "log" (func $log (param i32)))
;;(type $sig (func (param i32) (result)))
;;
;;
;;(func $run (export "run") (param) (result)
;;    ;;(i32.const 123)
;;    ;;(call $log)
;;

;;    ;;(call $hmm)
;;
;;    (i32.const 123)
;;    (ref.func $log)
;;    (call_ref)
;;    ;;drop
;;    ;;drop
;;
;;    ;;(call_ref (i32.const 123) (ref.func $log))
;;)

;;(func $hmm (param) (result)
;;    (i32.const 123)
;;    (call $log)
;;)

;;(elem declare funcref (ref.func $hmm))
