
//export interface Runnable {
    //run(x: number): void;
    ////mem: WebAssembly.Memory;
//}
//)).instance.exports as unknown as Runnable;

import { readFileSync } from "node:fs"

const binary = readFileSync("hmm.wasm");
const exports = (await WebAssembly.instantiate(
    binary, {
        env: { 
            foo: (() => console.log("foo")),
            bar: (() => console.log("bar")),
        } 
    }
)).instance.exports;

exports.run();

