// Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

import {
    assert,
    Int64, Uns32, Flt64, Flt32, Bytes,
} from "./index.ts";

//
// To build the binary, we are using (abusing) JavaScript strings to make
// arrays of bytes.  This generally wastes 1 extra byte per element, but it
// gives us the convenience of using the underlying rope implementation to
// concatenate pieces simply.  The alternatives are using a wrapper around a
// Uint8Array which would be more verbose and introduce some complexity
// whenever we need to reallocate, or using a JavaScript Array, which would
// waste 3 extra bytes.  I think this is the best choice from three flawed
// alternatives.
//

export class Pack {

    static #f32data = new Uint8Array(4);
    static #f64data = new Uint8Array(8);
    static #f32view = new DataView(Pack.#f32data.buffer);
    static #f64view = new DataView(Pack.#f64data.buffer);
    static #encoder = new TextEncoder();

    // Uns32 => Unsigned LEB128 Byte String
    static u32(value: Uns32): Bytes {
        assert(typeof value === 'number');
        assert(Number.isInteger(value));
        let bigint = BigInt(value);
        assert(0n <= value && value < 4294967296n);
        const result = [];
        for (;;) {
            const chunk = Number(bigint & 0x7Fn);
            bigint >>= 7n;
            if (bigint === 0n) {
                result.push(chunk);
                return String.fromCharCode(... result);
            } else {
                result.push(chunk | 0x80);
            }
        }
    }

    // Uns32 => Signed LEB128 Byte String
    static i64(value: Int64): Bytes {
        assert(typeof value === 'bigint');
        assert(-9223372036854775808n <= value);
        assert(value <= 9223372036854775807n);

        const result = [];
        for (;;) {
            const chunk = Number(value & 0x7Fn);
            value >>= 7n;
            if ((value ===  0n) && ((chunk&0x40) === 0) ||
                (value === -1n) && ((chunk&0x40) !== 0)
            ){
                result.push(chunk);
                return String.fromCharCode(... result);
            } else {
                result.push(chunk | 0x80);
            }
        }
    }

    // Flt32 => IEE754 Float Byte String (4 bytes)
    static f32(value: Flt32): Bytes {
        assert(typeof value === 'number');
        Pack.#f32view.setFloat32(0, value, true);
        return String.fromCharCode(... Pack.#f32data);
    }

    // Flt64 => IEE754 Double Byte String (8 bytes)
    static f64(value: Flt64): Bytes {
        assert(typeof value === 'number');
        Pack.#f64view.setFloat64(0, value, true);
        return String.fromCharCode(... Pack.#f64data);
    }

    // Unicode Code Point => UTF8 Byte String
    static utf8(value: Uns32): Bytes {
        assert(typeof value === 'number');
        assert(
            Number.isInteger(value) &&
            0 <= value && value <= 0x10FFFF,
            "value must be in unicode range:" + value
        );
        const string = String.fromCodePoint(value);
        return String.fromCharCode(... this.#encoder.encode(string));
    }

    // JS UTF16 String => UTF8 Byte String
    static str(value: string): Bytes {
        assert(typeof value === 'string');
        return String.fromCharCode(... this.#encoder.encode(value));
    }

    static zeros(count: Uns32): Bytes {
        assert(typeof count === 'number');
        assert(count >= 0);
        return "\x00".repeat(count);
    }
}

