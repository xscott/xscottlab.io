//! Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

import { writeFileSync } from "node:fs"

import {
    Runnable,
    assert,
    Uns32,
    debug,
    tokenize,
    parsify,
    Assembler,
    stdlib,
    GlobalPool,
    LocalPool,
} from "./index.ts";

export function compile(source: string): Runnable {
    const lines = source.split("\n");
    const tokens = tokenize(lines);
    const parse = parsify(tokens);
    debug(parse);

    const assem = new Assembler();
    const address = assem.newMemory(16*1024*1024);
    assert(address === 0, "scratch and memory allocation bitmap");
    const [builtins, imports] = stdlib(assem);

    const pool = new GlobalPool(assem);
    const temp = new LocalPool(0);


    const rawCode = parse.emitStmt(assem, builtins, pool, temp);
    debug(rawCode);
    const optCode = rawCode.optimize();
    debug(optCode);
    
    const bytes = optCode.emitBytes(assem, null);
    const run = assem.newExport("run", [], []);
    run.setCode(bytes);
    run.setLocals(temp.locals);

    const binary = assem.assemble();
    writeFileSync("binary.wasm", binary);

    debug(binary);

    const module = new WebAssembly.Module(binary);
    const instance = new WebAssembly.Instance(module, { env: imports });
    return instance.exports as unknown as Runnable;
}

