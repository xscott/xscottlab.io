import {
    Bytes, Uns32,
    Pack,
} from "./index.ts";

export type WasmType = Bytes;

export const Wasm = {
    V128 : "\x7B",
    F64  : "\x7C",
    F32  : "\x7D",
    I64  : "\x7E",
    U32  : "\x7F",
} as const;

export const Opcodes = {
    ABORT         : "\x00",         NOP           : "\x01",
    BLOCK         : "\x02",         LOOP          : "\x03",
    IF            : "\x04",         ELSE          : "\x05",
    TRY           : "\x06",         CATCH         : "\x07",
    THROW         : "\x08",         RETHROW       : "\x09",
    END           : "\x0B",         
    DELEGATE      : "\x18",         CATCH_ALL     : "\x19",
    DROP          : "\x1A",         SELECT        : "\x1B",
    JUMP          : "\x0C",         BRANCH        : "\x0D",
    RETURN        : "\x0F",         SWITCH        : "\x0E",
    FUNCALL       : "\x10",         PTRCALL       : "\x11",
    LOCAL_GET     : "\x20",         LOCAL_SET     : "\x21",
    LOCAL_TEE     : "\x22",        
    GLOBAL_GET    : "\x23",         GLOBAL_SET    : "\x24",
    MEMORY_SIZE   : "\x3F",         MEMORY_GROW   : "\x40",
    MEMORY_COPY   : "\xFC\x0A",     MEMORY_FILL   : "\xFC\x0B",

    funcall(idx: Uns32): Bytes {
        return (
            Opcodes.FUNCALL +
            Pack.u32(idx)
        );
    },
    ptrcall(sig: Uns32): Bytes {
        return (
            Opcodes.PTRCALL +
            Pack.u32(sig) +
            "\x00" // Table 0
        );
    },

    //i64_const(val) { return Opcodes.I64_CONST + Pack.i64(val); },
    //f64_const(val) { return Opcodes.F64_CONST + Pack.f64(val); },
                                                        
    // I'm only using WASM's i32 type for bools and pointers,
    // so I just need a subset of unsigned 32 bit operations
    U32_LOAD      : "\x28",         U32_STORE     : "\x36",
    U32_CONST     : "\x41",         U32_EQZ       : "\x45",
    U32_EQ        : "\x46",         U32_NE        : "\x47",
    U32_LT        : "\x49",         U32_GT        : "\x4B",
    U32_LE        : "\x4D",         U32_GE        : "\x4F",
    U32_ADD       : "\x6A",         U32_SUB       : "\x6B",
    U32_MUL       : "\x6C",         U32_AND       : "\x71",
    U32_OR        : "\x72",         U32_XOR       : "\x73",
    U32_SHL       : "\x74",         U32_SHR       : "\x76",

    // All integer arithmetic is done with WASM's signed i64
    I64_LOAD_I8   : "\x30",         LOAD_U8       : "\x31",
    I64_LOAD_I16  : "\x32",         LOAD_U16      : "\x33",
    I64_LOAD_I32  : "\x34",         LOAD_U32      : "\x35",
    I64_LOAD_B64  : "\x29",                  
    I64_STORE_B8  : "\x3C",         STORE_B16     : "\x3D",
    I64_STORE_B32 : "\x3E",         STORE_B64     : "\x37",
    I64_CONST     : "\x42",         I64_EQZ       : "\x50",
    I64_EQ        : "\x51",         I64_NE        : "\x52",
    I64_LT        : "\x53",         I64_GT        : "\x55",
    I64_LE        : "\x57",         I64_GE        : "\x59",
    I64_ADD       : "\x7C",         I64_SUB       : "\x7D",
    I64_MUL       : "\x7E",         I64_DIV       : "\x7F",
    I64_MOD       : "\x81",         I64_AND       : "\x83",
    I64_OR        : "\x84",         I64_XOR       : "\x85",
    I64_SHL       : "\x86",         I64_SAR       : "\x87",
    I64_SHR       : "\x88",         I64_CNT       : "\x7B",
    I64_ROL       : "\x89",         I64_ROR       : "\x8A",
    I64_CLZ       : "\x79",         I64_CTZ       : "\x7A",

    F32_LOAD      : "\x2A",         F32_STORE     : "\x38",
    F32_CONST     : "\x43",
    F32_EQ        : "\x5B",         F32_NE        : "\x5C",
    F32_LT        : "\x5D",         F32_GT        : "\x5E",
    F32_LE        : "\x5F",         F32_GE        : "\x60",
    F32_ADD       : "\x92",         F32_SUB       : "\x93",
    F32_MUL       : "\x94",         F32_DIV       : "\x95",
    F32_MIN       : "\x96",         F32_MAX       : "\x97",
    F32_COPYSIGN  : "\x98",         F32_SQRT      : "\x91",
    F32_ABS       : "\x8B",         F32_NEG       : "\x8C",
    F32_CEIL      : "\x8D",         F32_FLOOR     : "\x8E",
    F32_TRUNC     : "\x8F",         F32_ROUND     : "\x90",
                                              
    F64_LOAD      : "\x2B",         F64_STORE     : "\x39",
    F64_CONST     : "\x44",                               
    F64_EQ        : "\x61",         F64_NE        : "\x62",
    F64_LT        : "\x63",         F64_GT        : "\x64",
    F64_LE        : "\x65",         F64_GE        : "\x66",
    F64_ADD       : "\xA0",         F64_SUB       : "\xA1",
    F64_MUL       : "\xA2",         F64_DIV       : "\xA3",
    F64_MIN       : "\xA4",         F64_MAX       : "\xA5",
    F64_COPYSIGN  : "\xA6",         F64_SQRT      : "\x9F",
    F64_ABS       : "\x99",         F64_NEG       : "\x9A",
    F64_CEIL      : "\x9B",         F64_FLOOR     : "\x9C",
    F64_TRUNC     : "\x9D",         F64_ROUND     : "\x9E",

    // The TO are numeric conversions.  The AS are bit copies.
    I32_TO_I64    : "\xAC",
    U32_TO_I64    : "\xAD",         I64_TO_U32    : "\xA7",
    F32_TO_F64    : "\xBB",         F64_TO_F32    : "\xB6",
    I64_TO_F32    : "\xB4",         F32_TO_I64    : "\xAE",
    I64_TO_F64    : "\xB9",         F64_TO_I64    : "\xB0",
    I64_AS_F64    : "\xBF",         F64_AS_I64    : "\xBD",
    F32_AS_I32    : "\xBC",         I32_AS_F32    : "\xBE",

    V128_LOAD     : "\xFD\x00",     V128_STORE    : "\xFD\x0B",
    V128_CONST    : "\xFD\x0C",    
    I8X8_LOAD     : "\xFD\x01",     U8X8_LOAD     : "\xFD\x02",
    I16X4_LOAD    : "\xFD\x03",     U16X4_LOAD    : "\xFD\x04",
    I32X2_LOAD    : "\xFD\x05",     U32X2_LOAD    : "\xFD\x06",
    B8_SPLOAD     : "\xFD\x07",     B16_SPLOAD    : "\xFD\x08",
    B32_SPLOAD    : "\xFD\x09",     B64_SPLOAD    : "\xFD\x0A",
                                              
    // SHUFFLE mixes two v128 using an immediate of indices
    // SWIZZLE mixes one v128 using a second v128 for indices 
    V128_SHUFFLE  : "\xFD\x0D",     V128_SWIZZLE  : "\xFD\x0E",

    // SPLATs take one argument and duplicate to all lanes
    B8X16_SPLAT   : "\xFD\x0F",     B16X8_SPLAT   : "\xFD\x10",
    B32X4_SPLAT   : "\xFD\x11",     B64X2_SPLAT   : "\xFD\x12",
    F32X4_SPLAT   : "\xFD\x13",     F64X2_SPLAT   : "\xFD\x14",
                                              
    I8X16_GETLANE : "\xFD\x15",     U8X16_GETLANE : "\xFD\x16",
    I16X8_GETLANE : "\xFD\x18",     U16X8_GETLANE : "\xFD\x19",
    B32X4_GETLANE : "\xFD\x1B",     B64X2_GETLANE : "\xFD\x1D",
    F32X4_GETLANE : "\xFD\x1F",     F64X2_GETLANE : "\xFD\x21",
                                   
    B8X16_SETLANE : "\xFD\x17",     B16X8_SETLANE : "\xFD\x1A",
    B32X4_SETLANE : "\xFD\x1C",     B64X2_SETLANE : "\xFD\x1E",
    F32X4_SETLANE : "\xFD\x20",     F64X2_SETLANE : "\xFD\x22",
                                   
    B8X16_EQ      : "\xFD\x23",     B8X16_NE      : "\xFD\x24",
    I8X16_LT      : "\xFD\x25",     U8X16_LT      : "\xFD\x26",
    I8X16_GT      : "\xFD\x27",     U8X16_GT      : "\xFD\x28",
    I8X16_LE      : "\xFD\x29",     U8X16_LE      : "\xFD\x2A",
    I8X16_GE      : "\xFD\x2B",     U8X16_GE      : "\xFD\x2C",
                                   
    B16X8_EQ      : "\xFD\x2D",     B16X8_NE      : "\xFD\x2E",
    I16X8_LT      : "\xFD\x2F",     U16X8_LT      : "\xFD\x30",
    I16X8_GT      : "\xFD\x31",     U16X8_GT      : "\xFD\x32",
    I16X8_LE      : "\xFD\x33",     U16X8_LE      : "\xFD\x34",
    I16X8_GE      : "\xFD\x35",     U16X8_GE      : "\xFD\x36",
                                   
    B32X4_EQ      : "\xFD\x37",     B32X4_NE      : "\xFD\x38",
    I32X4_LT      : "\xFD\x39",     U32X4_LT      : "\xFD\x3A",
    I32X4_GT      : "\xFD\x3B",     U32X4_GT      : "\xFD\x3C",
    I32X4_LE      : "\xFD\x3D",     U32X4_LE      : "\xFD\x3E",
    I32X4_GE      : "\xFD\x3F",     U32X4_GE      : "\xFD\x40",

    B64X2_EQ      : "\xFD\xD6\x01", B64X2_NE      : "\xFD\xD7\x01",
    I64X2_LT      : "\xFD\xD8\x01", I64X2_GT      : "\xFD\xD9\x01",
    I64X2_LE      : "\xFD\xDA\x01", I64X2_GE      : "\xFD\xDB\x01",
    // There don't seem to be any unsigned 64 bit comparison opcodes
    //   U64X2_LE  U64X2_LT  U64X2_GT  U64X2_GE

    F32X4_EQ      : "\xFD\x41",     F32X4_NE      : "\xFD\x42",
    F32X4_LT      : "\xFD\x43",     F32X4_GT      : "\xFD\x44",
    F32X4_LE      : "\xFD\x45",     F32X4_GE      : "\xFD\x46",
    F32X4_NEG     : "\xFD\xE1\x01",
    F32X4_ADD     : "\xFD\xE4\x01", F32X4_SUB     : "\xFD\xE5\x01",
    F32X4_MUL     : "\xFD\xE6\x01",

    F64X2_EQ      : "\xFD\x47",     F64X2_NE      : "\xFD\x48",
    F64X2_LT      : "\xFD\x49",     F64X2_GT      : "\xFD\x4A",
    F64X2_LE      : "\xFD\x4B",     F64X2_GE      : "\xFD\x4C",
    F64X2_NEG     : "\xFD\xED\x01",
    F64X2_ADD     : "\xFD\xF0\x01", F64X2_SUB     : "\xFD\xF1\x01",
    F64X2_MUL     : "\xFD\xF2\x01",
} as const;

