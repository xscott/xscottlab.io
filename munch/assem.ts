// Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

import {
    assert, panic,
    Uns32, Bytes,
    Pack,
    Wasm, WasmType, Opcodes
} from "./index.ts";

const ValidWasmTypes: Set<string> = new Set(Object.values(Wasm));

export class Assembler {
    startFunc:   null | Uns32      = null;
    #signatures: Map<Bytes, Uns32> = new Map();
    #procedures: Procedure[]       = [];
    #typedefs:   Bytes[]           = [];
    #pointers:   Uns32[]           = [];
    #memOffset:  Uns32             = 0;
    #memBytes:   Bytes[]           = [];
    #globals:    Bytes[]           = [];
    #importing:  boolean           = true;
    #cached:     Map<Bytes, Uns32> = new Map();

    newImport(
        impName: string,
        params: WasmType[],
        results: WasmType[],
    ): Procedure {
        assert(this.#importing, "imports must come first");
        const procedure = this.#newProcedure(params, results);
        procedure.impName = impName;
        return procedure;
    }

    newExport(
        expName: string,
        params: WasmType[],
        results: WasmType[],
    ): Procedure {
        this.#importing = false;
        const procedure = this.#newProcedure(params, results);
        procedure.expName = expName;
        return procedure;
    }

    newLambda(
        params: WasmType[],
        results: WasmType[],
    ): Procedure {
        this.#importing = false;
        const procedure = this.#newProcedure(params, results);
        procedure.ptrIndex = this.#pointers.length;
        this.#pointers.push(procedure.funIndex);
        return procedure;
    }

    newFunction(
        params: WasmType[],
        results: WasmType[],
    ): Procedure {
        this.#importing = false;
        return this.#newProcedure(params, results);
    }

    cachedFunc(
        params: WasmType[],
        results: WasmType[],
        locals: WasmType[],
        code: Bytes
    ): Uns32 {
        const key = (
            params.join("") + ":" +
            results.join("") + ":" +
            locals.join("") + ":" +
            code
        );
        const existing = this.#cached.get(key);
        if (existing !== undefined) return existing;
        const funIndex = this
            .newFunction(params, results)
            .setLocals(locals)
            .setCode(code)
            .funIndex
        ;
        this.#cached.set(key, funIndex);
        return funIndex;
    }

    #newProcedure(
        params: WasmType[],
        results: WasmType[],
    ): Procedure {
        const procedure = new Procedure(
            this.signature(params, results),
            this.#procedures.length
        );
        this.#procedures.push(procedure);
        return procedure;
    }

    newGlobal(
        type: WasmType, value: null | string | number | bigint = null
    ): number {
        assert(ValidWasmTypes.has(type), "must be valid type");
        const mutable = "\x01";
        let global = type + mutable;
        switch (type) {
            case Wasm.V128:
                value ??= Pack.zeros(16);
                assert(typeof value === 'string', "must be string:" + value);
                assert(value.length === 16, "must be length 16:" + value);
                global += Opcodes.V128_CONST + value;
                break;
            case Wasm.F64:
                value ??= 0.0;
	    	assert(typeof value === 'number');
                global += Opcodes.F64_CONST + Pack.f64(value);
                break;
            case Wasm.F32:
                value ??= 0.0;
	    	assert(typeof value === 'number');
                global += Opcodes.F32_CONST + Pack.f32(value);
                break;
            case Wasm.I64:
                value ??= 0n;
	    	assert(typeof value === 'bigint');
                global += Opcodes.I64_CONST + Pack.i64(value);
                break;
            case Wasm.U32:
                value ??= 0;
	    	assert(typeof value === 'number');
                global += Opcodes.U32_CONST + Pack.u32(value);
                break;
            default: panic("unhandled global type: " + type);
        }
        global += Opcodes.END;
        const index = this.#globals.length;
        this.#globals.push(global);
        return index;
    }

    newMemory(
        length_or_bytes: Uns32 | Bytes,
        align: Uns32 = 1
    ): number {
        if (this.#memOffset % align !== 0) {
            this.#memOffset += align - this.#memOffset % align;
        }
        const address = this.#memOffset;
        if (typeof length_or_bytes  === 'number') {
            this.#memOffset += length_or_bytes;
        } else {
            assert(typeof length_or_bytes === 'string');
            const bytes = length_or_bytes;
            this.#memOffset += bytes.length;

            let memory = "\x00"; // flags
            memory += Opcodes.U32_CONST;
            memory += Pack.u32(address);
            memory += Opcodes.END;
            memory += Pack.u32(bytes.length);
            memory += bytes;
            this.#memBytes.push(memory);
        }
        return address;
    }

    signature(
        params: WasmType[],
        results: WasmType[],
    ): number {
        assert(params instanceof Array);
        assert(results instanceof Array);
        params.forEach(x => assert(ValidWasmTypes.has(x)));
        results.forEach(x => assert(ValidWasmTypes.has(x)));
        const param_bytes = params.join('');
        const result_bytes = results.join('');
        const key = param_bytes + ":" + result_bytes;
        const existing = this.#signatures.get(key);
        if (existing !== undefined) return existing;

        let typedef = "\x60"; // Function Type
        typedef += Pack.u32(params.length);
        typedef += param_bytes;
        typedef += Pack.u32(results.length);
        typedef += result_bytes;
        const sigIndex = this.#typedefs.length;
        this.#typedefs.push(typedef);
        this.#signatures.set(key, sigIndex);
        return sigIndex;
    }

    assemble(): Uint8Array {
        let binary = "\0asm\x01\0\0\0";

        // Section 1: Types Definitions
        if (this.#typedefs.length > 0) {
            binary += "\x01"; // Type Section
            const section: string = (
                Pack.u32(this.#typedefs.length) +
                this.#typedefs.join("")
            );
            binary += Pack.u32(section.length);
            binary += section;
        }

        // Section 2: Import Names and Signatures
        const imports: Procedure[] = this.#procedures.filter(
            (fun: Procedure) => typeof fun.impName === 'string'
        );
        if (imports.length > 0) {
            binary += "\x02"; // Import section
            let section: string = Pack.u32(imports.length);
            for (const imp of imports) {
                assert(imp.code == null, "imports must not have code");
                // We always use "env" as the module name
                const modname = "env";
                section += Pack.u32(modname.length);
                section += modname;
                assert(typeof imp.impName === 'string');
                const impName = Pack.str(imp.impName);
                section += Pack.u32(impName.length);
                section += impName;
                section += "\x00" // function import
                section += Pack.u32(imp.sigIndex);
            }
            binary += Pack.u32(section.length);
            binary += section;
        }

        // Section 3: Non-Import Signatures
        const funcdefs: Procedure[] = this.#procedures.filter(
            fun => typeof fun.impName !== 'string'
        );
        if (funcdefs.length > 0) {
            binary += "\x03"; // Function Section
            let section: string = Pack.u32(funcdefs.length);
            for (const def of funcdefs) {
                assert(def.code != null, "non-imports must have code");
                section += Pack.u32(def.sigIndex);
            }
            binary += Pack.u32(section.length);
            binary += section;
        }

        // Section 4: Table (Number of Pointers)
        if (this.#pointers.length > 0) {
            binary += "\x04"; // Table Section
            // We only have one table for use with the pointer mapping to our
            // lambda functions. The actual pointer to index mapping is done in
            // Section 9 below.  Tables have a lot of other capabilities that I
            // don't really understand or use.
            let section: string = Pack.u32(1); // num tables
            section += "\x70"; // funcref
            section += "\x00"; // flags
            section += Pack.u32(this.#pointers.length);
            binary += Pack.u32(section.length);
            binary += section;
        }

        // Section 5: Memory Pages
        { // we always want memory
            binary += "\x05"; // Memory Section
            const pages = Math.ceil(this.#memOffset/65536);
            let section: string = ""
            section += Pack.u32(1); // num memory
            section += "\x01"; // flag, have max
            section += Pack.u32(pages); // init
            section += Pack.u32(65536); // max
            binary += Pack.u32(section.length);
            binary += section;
        }

        // Section 6: Global Definitions
        if (this.#globals.length) {
            binary += "\x06"; // Global Section
            let section: string = Pack.u32(this.#globals.length);
            section += this.#globals.join("");
            binary += Pack.u32(section.length);
            binary += section;
        }

        // Section 7: Export Names
        { // we always export memory as "mem"
            binary += "\x07"; // Export Section
            const exports: Procedure[] = this.#procedures.filter(
                (fun: Procedure) => typeof fun.expName === 'string'
            );
            let section: string = Pack.u32(1 + exports.length);
            const memname = "mem";
            section += Pack.u32(memname.length);
            section += memname;
            section += "\x02"; // kind: mem
            section += "\x00"; // mem index
            for (const exp of exports) {
                assert(typeof exp.expName === 'string');
                const expName = Pack.str(exp.expName);
                section += Pack.u32(expName.length);
                section += expName;
                section += "\x00"; // kind: func
                section += Pack.u32(exp.funIndex);
            }
            binary += Pack.u32(section.length);
            binary += section;
        }

        // Section 8: Start Function
        if (this.startFunc !== null) {
            binary += "\x08"; // Start Section
            // In JavaScript, the Start function gets called before the
            // JavaScript initialize function has returned.  Maybe this is
            // useful for initializing internal variables or data.  However, we
            // can't use this as a replacement for a C-style main() or _start()
            // because the exports (including the memory export) are not
            // visible to the imports this early in the process.  In other
            // words, we wouldn't be able to pass strings or array data to the
            // import functions, because we need the memory buffer to be
            // exported to do that.
            const section: string = Pack.u32(this.startFunc);
            binary += Pack.u32(section.length);
            binary += section;
        }

        // Section 9: Element (List of Pointers)
        if (this.#pointers.length > 0) {
            binary += "\x09"; // Elem Section
            let section: string = "";
            section += "\x01"; // segments
            section += "\x00"; // flags
            section += Opcodes.U32_CONST;
            section += Pack.u32(0);  // offset
            section += Opcodes.END;
            section += Pack.u32(this.#pointers.length);
            for (const funIndex of this.#pointers) {
                section += Pack.u32(funIndex);
            }
            binary += Pack.u32(section.length);
            binary += section;
        }

        // Section 12: DataCount Kluge
        if (this.#memBytes.length > 0) {
            binary += "\x0C"; // DataCount Section
            // The DataCount section was added for the "Bulk Memory Operations"
            // extension.  Basically, in order to do a validation in one-pass,
            // the Code (Section 10) needs to know how many entries are in the
            // Data (Section 11) which comes after.  Therefore they created a
            // the DataCount (Section 12) to go before 10, and all it does is
            // say how many parts are in Section 11.  Hindsight is 20/20.
            //
            // We don't currently use the opcodes which need this information
            // (data.drop, memory.inet, others?), but it's not a lot of code
            // and it helps us to compare with wat2wasm output for debugging.
            const section = Pack.u32(this.#memBytes.length);
            binary += Pack.u32(section.length);
            binary += section;
        }

        // Section 10: Function Code
        if (funcdefs.length) {
            binary += "\x0A"; // Code Section
            let section: string = Pack.u32(funcdefs.length);
            for (const def of funcdefs) {
                const body = def.locals + def.code;
                section += Pack.u32(body.length);
                section += body;
            }
            binary += Pack.u32(section.length);
            binary += section;
        }

        // Section 11: Memory Data
        if (this.#memBytes.length > 0) {
            binary += "\x0B"; // Data Section
            let section: string = Pack.u32(this.#memBytes.length);
            section += this.#memBytes.join("");
            binary += Pack.u32(section.length);
            binary += section;
        }

        const result = new Uint8Array(binary.length);
        for (let ii = 0; ii<binary.length; ++ii) {
            const code = binary.charCodeAt(ii);
            assert(0 <= code && code < 256);
            result[ii] = code;
        }
        return result;
    }
}

class Procedure {
    ptrIndex: null | number = null;
    impName:  null | string = null;
    expName:  null | string = null;
    locals: Bytes = "\x00" as Bytes;
    code: null | Bytes = null;
    
    constructor(
        public sigIndex: number,
        public funIndex: number,
    ) {}

    setLocals(locals: Bytes[]) {
        assert(locals instanceof Array);
        locals.forEach(x => assert(ValidWasmTypes.has(x)));
        const len = Pack.u32(locals.length);
        const one = Pack.u32(1);
        // we can replace `one` with other numbers for repeated locals
        this.locals = len + locals.map(x => one + x).join("");
        return this;
    }

    setCode(bytes: Bytes) {
        assert(typeof bytes === 'string');
        this.code = bytes + Opcodes.END;
        return this;
    }
}
