// Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

"use strict";
import { writeFileSync } from "node:fs"

//{{{ helpers 

class SealedRecord {
    constructor(fields) {
        Object.assign(this, fields);
        Object.seal(this);
    }
}

class FrozenRecord {
    constructor(fields) {
        Object.assign(this, fields);
        Object.freeze(this);
    }
}

function assert(condition, msg="assertion failure") {
    // internal error
    if (!condition) throw Error(msg);
}

function panic(msg) {
    // internal error
    throw Error(msg);
}

function error(row, col, msg) {
    // compilation error
    throw { row, col, msg };
}

function warning(row, col, msg) {
    // compilation warning
    console.log(`warning row: ${row} col: ${col}, ${msg}`);
}

function profile(name, callable) {
    const before = Date.now();
    const result = callable();
    const after = Date.now();
    console.log(`${name}:\t${after - before} msec`);
    return result;
}

function kindof(x) {
    const t = typeof x;
    if (t === "object") {
        if (x === null) return null;
        return x.constructor;
    }
    if (t === "number") return Number;
    if (t === "bigint") return BigInt;
    if (t === "string") return String;
    if (t === "symbol") return Symbol;
    if (t === "undefined") return undefined;
    panic("unhandled kindof type");
}
//}}}
//{{{ bytestr 

// We want concatenable arrays of bytes, so we're creating strings where all of
// the 2-byte codes only contain values from 0 to 256.  We call these
// "bytestr", but of course they're just JavaScript strings.  We're assuming JS
// strings are implemented with ropes (otherwise this stuff is quadratic slow).

function bytestr_u32(num) {
    const big = BigInt(num);
    assert(0n <= big && big < (1n<<32n));
    return bytestr_leb128(big);
}

function bytestr_i64(num) {
    const big = BigInt(num);
    assert(-(1n<<63n) <= big && big < (1n<<63n));
    return bytestr_leb128(big);
}

function bytestr_leb128(num) {
    const kind = kindof(num);
    assert(kind === Number || kind === BigInt);
    let big = BigInt(num);
    var result = "";
    for (;;) {
        const chunk = Number(big & 0x7Fn);
        big >>= 7n;
        if ((big ===  0n) && ((chunk&0x40) === 0) ||
            (big === -1n) && ((chunk&0x40) !== 0)
        ){
            result += String.fromCharCode(chunk);
            break;
        } else {
            result += String.fromCharCode(chunk | 0x80);
        }
    }
    return result;
}

function bytestr_utf8(str) {
    assert(kindof(str) === String);
    // Takes a UTF16 string and make a UTF8 bytestr
    assert(typeof str === 'string');
    if (!bytestr_utf8.encoder) {
        bytestr_utf8.encoder = new TextEncoder();
    }
    return bytestr_array(bytestr_utf8.encoder.encode(str));
}

function bytestr_byte(num) {
    assert(kindof(num) === Number);
    assert(0 <= num && num < 256);
    return String.fromCharCode(num);
}

function bytestr_f64(num) {
    assert(kindof(num) === Number);
    if (!bytestr_f64.array) {
        bytestr_f64.array = new Float64Array(1);
        bytestr_f64.bytes = new Uint8Array(bytestr_f64.array.buffer);
    }
    bytestr_f64.array[0] = num;
    return bytestr_array(bytestr_f64.bytes);
}

function bytestr_f32(num) {
    assert(kindof(num) === Number);
    if (!bytestr_f32.array) {
        bytestr_f32.array = new Float32Array(1);
        bytestr_f32.bytes = new Uint8Array(bytestr_f32.array.buffer);
    }
    bytestr_f32.array[0] = num;
    return bytestr_array(bytestr_f32.bytes);
}

function bytestr_zeros(count) {
    assert(kindof(count) === Number);
    assert(count >= 0);
    return "\x00".repeat(count);
}

function bytestr_array(arr) {
    var result = "";
    for (let elem of arr) {
        assert(0 <= elem && elem < 256);
        result += String.fromCharCode(elem);
    }
    return result;
}

//}}}
//{{{ debug 

function debug(data) {
    let result = "";
    recur(data, 0);
    console.log(result);
    return;

    function recur(data, indent) {
        if (typeof(data) === 'object') {
            if (data === null) {
                result += "null";
                return;
            }
            if (data instanceof Uint8Array) {
                result += "Uint8Array( ";
                for (let ii = 0; ii<data.length; ++ii) {
                    result += data[ii].toString(16).padStart(2, '0') + " ";
                }
                result += ")";
                return;
            }
            result += data.constructor.name + "( ";
            let nested = false;
            for (let key in data) {
                const val = data[key];
                if (Number(key) == key || typeof(val) === 'object') {
                    nested = true;
                    continue;
                }
                result += key + ":";
                recur(val, indent+1);
                result += " ";
            }
            if (nested) {
                for (let key in data) {
                    if (Number(key) == key) continue;
                    const val = data[key];
                    if (typeof(val) === 'object') {
                        result += "\n" + "    ".repeat(indent+1) + key + ": ";
                        recur(val, indent+1);
                    }
                }
                for (let key in data) {
                    if (Number(key) != key) continue;
                    const val = data[key];
                    result += "\n" + "    ".repeat(indent+1) + key + ": ";
                    recur(val, indent+1);
                }
                result += "\n" + "    ".repeat(indent) + ")";
            } else {
                result += ")";
            }
            return;
        }
        if (typeof(data) === 'string') {
            for (const ii in data) {
                const ord = data.charCodeAt(ii)
                if (ord < 32 || 127 < ord) {
                    result += "hex( ";
                    for (const jj in data) {
                        const num = data.charCodeAt(jj);
                        const str = num.toString(16).padStart(2, "0");
                        result += str + " ";
                    }
                    result += ")";
                    return;
                }
            }
            result += '"' + data + '"';
            return;
        }
        if (typeof(data) === 'bigint') {
            result += String(data) + 'n';
            return;
        }
        result += String(data);
    }
}

//}}}
//{{{ WasmType

const WASM_V128 = "\x7B";
const WASM_F64  = "\x7C";
const WASM_F32  = "\x7D";
const WASM_I64  = "\x7E";
const WASM_U32  = "\x7F";

const WASM_TYPES = new Map([
    [WASM_V128, "v128"],
    [WASM_F64,  "f64"],
    [WASM_F32,  "f32"],
    [WASM_I64,  "i64"],
    [WASM_U32,  "u32"],
]);

//}}}
//{{{ Assembler 

class Assembler {
    startfunc   = undefined;
    #signatures = new Map();
    #callables  = [];
    #typedefs   = [];
    #pointers   = [];
    #memoffset  = 0;
    #membytes   = [];
    #globals    = [];
    #importing  = true;

    new_import(impname, params, results) {
        if (!this.#importing) panic("imports must come first");
        return this.new_function({ impname, params, results });
    }

    new_export(expname, params, results) {
        this.#importing = false;
        return this.new_function({ expname, params, results });
    }

    new_static(params, results) {
        this.#importing = false;
        return this.new_function({ params, results });
    }

    new_lambda(params, results) {
        this.#importing = false;
        return this.new_function({ params, results, needptr: true });
    }

    new_function({ impname, expname, params, results, needptr }) {
        const sigindex = this.signature(params, results);
        const callable = new WasmFunc(sigindex, impname, expname);
        callable.index = this.#callables.length;
        this.#callables.push(callable);
        if (needptr) {
            callable.pointer = this.#pointers.length;
            this.#pointers.push(callable.index);
        }
        return callable;
    }

    signature(params, results) {
        assert(kindof(params) === Array);
        assert(kindof(results) === Array);
        params.forEach(x => assert(WASM_TYPES.has(x)));
        results.forEach(x => assert(WASM_TYPES.has(x)));
        const param_bytes = params.join('');
        const result_bytes = results.join('');
        const key = param_bytes + ":" + result_bytes;
        const existing = this.#signatures.get(key);
        if (existing !== undefined) return existing;

        let typedef = "\x60"; // Function Type
        typedef += bytestr_leb128(params.length);
        typedef += param_bytes;
        typedef += bytestr_leb128(results.length);
        typedef += result_bytes;
        const sigindex = this.#typedefs.length;
        this.#typedefs.push(typedef);
        this.#signatures.set(key, sigindex);
        return sigindex;
    }

    new_global(type) {
        const mutable = bytestr_byte(1);
        let global = type + mutable;
        switch (type) {
            case WASM_V128:
                global += Opcodes.V128_CONST + bytestr_zeros(16);
                break;
            case WASM_F64:
                global += Opcodes.F64_CONST + bytestr_zeros(8);
                break;
            case WASM_F32:
                global += Opcodes.F32_CONST + bytestr_zeros(4);
                break;
            case WASM_I64:
                global += Opcodes.I64_CONST + bytestr_leb128(0);
                break;
            case WASM_U32:
                global += Opcodes.U32_CONST + bytestr_leb128(0);
                break;
            default: panic("unhandled global type: " + type);
        }
        global += Opcodes.END;
        const index = this.#globals.length;
        this.#globals.push(global);
        return index;
    }

    new_memory(length_or_bytes, align=1) {
        if (this.#memoffset % align !== 0) {
            this.#memoffset += align - this.#memoffset % align;
        }
        const address = this.#memoffset;
        if (kindof(length_or_bytes) === Number) {
            const length = length_or_bytes;
            this.#memoffset += length_or_bytes;
        } else {
            assert(kindof(length_or_bytes) === String);
            const bytes = length_or_bytes;
            this.#memoffset += bytes.length;

            const flags = bytestr_byte(0);
            let memory = flags;
            memory += Opcodes.U32_CONST;
            memory += bytestr_leb128(address);
            memory += Opcodes.END;
            memory += bytestr_leb128(bytes.length);
            memory += bytes;
            this.#membytes.push(memory);
        }
        return address;
    }

    assemble() {
        let binary = "\0asm\x01\0\0\0";

        // Section 1: Types Definitions
        if (this.#typedefs.length > 0) {
            binary += bytestr_byte(1); // Type Section
            const section = (
                bytestr_leb128(this.#typedefs.length) +
                this.#typedefs.join("")
            );
            binary += bytestr_leb128(section.length);
            binary += section;
        }

        // Section 2: Import Names and Signatures
        const imports = this.#callables.filter(
            fun => typeof fun.impname === "string"
        );
        if (imports.length > 0) {
            let section = bytestr_leb128(imports.length);
            for (const imp of imports) {
                if (imp.code !== undefined) {
                    panic("imports must not have code");
                }
                // We always use "env" as the module name
                const modname = "env"; //bytestr_utf8("env");
                section += bytestr_leb128(modname.length);
                section += modname;
                const impname = bytestr_utf8(imp.impname);
                section += bytestr_leb128(impname.length);
                section += impname;
                section += bytestr_byte(0) // function import
                section += bytestr_leb128(imp.sigindex);
            }
            binary += bytestr_byte(2); // Import section
            binary += bytestr_leb128(section.length);
            binary += section;
        }

        // Section 3: Non-Import Signatures
        const funcdefs = this.#callables.filter(
            fun => typeof fun.impname !== 'string'
        );
        if (funcdefs.length > 0) {
            let section = bytestr_leb128(funcdefs.length);
            for (const def of funcdefs) {
                if (def.code === undefined) {
                    panic("non-imports must have code");
                }
                section += bytestr_leb128(def.sigindex);
            }
            binary += bytestr_byte(3); // Function Section
            binary += bytestr_leb128(section.length);
            binary += section;
        }

        // Section 4: Table (Number of Pointers)
        if (this.#pointers.length > 0) {
            // We only have one table for use with the pointer mapping to our
            // lambda functions. The actual pointer to index mapping is done in
            // Section 9 below.  Tables have a lot of other capabilities that I
            // don't really understand or use.
            let section = bytestr_leb128(1); // num tables
            section += bytestr_byte(0x70);   // funcref
            section += bytestr_byte(0x00);   // flags
            section += bytestr_leb128(this.#pointers.length);
            binary += bytestr_byte(4); // Table Section
            binary += bytestr_leb128(section.length);
            binary += section;
        }

        // Section 5: Memory Pages
        if (true) { // we always want memory
            const pages = Math.ceil(this.#memoffset/65536);
            let section = ""
            section += bytestr_leb128(1); // num memory
            section += bytestr_byte(1); // flag, have max
            section += bytestr_leb128(pages); // init
            section += bytestr_leb128(65536); // max
            binary += bytestr_byte(5); // Memory Section
            binary += bytestr_leb128(section.length);
            binary += section;
        }

        // Section 6: Global Definitions
        if (this.#globals.length) {
            let section = bytestr_leb128(this.#globals.length);
            section += this.#globals.join("");
            binary += bytestr_byte(6); // Global Section
            binary += bytestr_leb128(section.length);
            binary += section;
        }

        // Section 7: Export Names
        if (true) { // we always export memory as "mem"
            const exports = this.#callables.filter(
                fun => kindof(fun.expname) === String
            );
            let section = bytestr_leb128(1 + exports.length);
            const memname = "mem"; // bytestr_utf8("mem");
            section += bytestr_leb128(memname.length);
            section += memname;
            section += bytestr_byte(2); // kind: mem
            section += bytestr_byte(0); // mem index
            for (const exp of exports) {
                const expname = bytestr_utf8(exp.expname);
                section += bytestr_leb128(expname.length);
                section += expname;
                section += bytestr_byte(0); // kind: func
                section += bytestr_leb128(exp.index);
            }
            binary += bytestr_byte(7); // Export Section
            binary += bytestr_leb128(section.length);
            binary += section;
        }

        // Section 8: Start Function
        if (this.startfunc !== undefined) {
            // In JavaScript, the Start function gets called before the
            // JavaScript initialize function has returned.  Maybe this is
            // useful for initializing internal variables or data.  However, we
            // can't use this as a replacement for a C-style main() or _start()
            // because the exports (including the memory export) are not
            // visible to the imports this early in the process.  In other
            // words, we wouldn't be able to pass strings or array data to the
            // import functions, because we need the memory buffer to be
            // exported to do that.
            const section = bytestr_leb128(this.startfunc);
            binary += bytestr_byte(8); // Start Section
            binary += bytestr_leb128(section.length);
            binary += section;
        }

        // Section 9: Element (List of Pointers)
        if (this.#pointers.length > 0) {
            let section = "";
            section += bytestr_byte(0x01); // segments
            section += bytestr_byte(0x00); // flags
            section += Opcodes.U32_CONST;
            section += bytestr_leb128(0);  // offset
            section += Opcodes.END;
            section += bytestr_leb128(this.#pointers.length);
            for (const ptr of this.#pointers) {
                section += bytestr_leb128(ptr);
            }
            binary += bytestr_byte(9); // Elem Section
            binary += bytestr_leb128(section.length);
            binary += section;
        }

        // Section 12: DataCount Kluge
        if (this.#membytes.length > 0) {
            // The DataCount section was added for the "Bulk Memory Operations"
            // extension.  Basically, in order to do a validation in one-pass,
            // the Code (Section 10) needs to know how many entries are in the
            // Data (Section 11) which comes after.  Therefore they created a
            // the DataCount (Section 12) to go before 10, and all it does is
            // say how many parts are in Section 11.  Hindsight is 20/20.
            //
            // We don't currently use the opcodes which need this information
            // (data.drop, memory.inet, others?), but it's not a lot of code
            // and it helps us to compare with wat2wasm output for debugging.
            const section = bytestr_leb128(this.#membytes.length);
            binary += bytestr_byte(12); // DataCount Section
            binary += bytestr_leb128(section.length);
            binary += section;
        }

        // Section 10: Function Code
        if (funcdefs.length) {
            let section = bytestr_leb128(funcdefs.length);
            for (const def of funcdefs) {
                const body = def.locals + def.code;
                section += bytestr_leb128(body.length);
                section += body;
            }
            binary += bytestr_byte(10); // Code Section
            binary += bytestr_leb128(section.length);
            binary += section;
        }

        // Section 11: Memory Data
        if (this.#membytes.length > 0) {
            let section = bytestr_leb128(this.#membytes.length);
            section += this.#membytes.join("");
            binary += bytestr_byte(11); // Data Section
            binary += bytestr_leb128(section.length);
            binary += section;
        }

        const result = new Uint8Array(binary.length);
        for (let ii = 0; ii<binary.length; ++ii) {
            const code = binary.charCodeAt(ii);
            assert(0 <= code && code < 256);
            result[ii] = code;
        }
        return result;
    }
}
//}}}
//{{{ WasmFunc 

class WasmFunc extends SealedRecord {
    constructor(sigindex, impname, expname) {
        super({ sigindex, impname, expname,
            index: undefined,
            pointer: undefined,
            locals: "\0",
            code: undefined
        });
    }

    set_locals(locals) {
        assert(kindof(locals) === Array);
        locals.forEach(x => assert(WASM_TYPES.has(x)));
        const len = bytestr_leb128(locals.length);
        const one = bytestr_leb128(1);
        // we can replace `one` with other numbers for repeated locals
        this.locals = len + locals.map(x => one + x).join("");
        return this;
    }

    set_code(bytes) {
        assert(kindof(bytes) === String);
        this.code = bytes + Opcodes.END;
        return this;
    }
}

//}}}
//{{{ Opcodes 

// I've taken liberties with the naming on these, and some of them are
// different than in the official documentation:
//
//   https://webassembly.github.io/spec/core/appendix/index-instructions.html
//

const Opcodes = {
    ABORT         : "\x00",         NOP           : "\x01",
    BLOCK         : "\x02",         LOOP          : "\x03",
    IF            : "\x04",         ELSE          : "\x05",
    TRY           : "\x06",         CATCH         : "\x07",
    THROW         : "\x08",         RETHROW       : "\x09",
    END           : "\x0B",         
    DELEGATE      : "\x18",         CATCH_ALL     : "\x19",
    DROP          : "\x1A",         SELECT        : "\x1B",
    JUMP          : "\x0C",         BRANCH        : "\x0D",
    RETURN        : "\x0F",         SWITCH        : "\x0E",
    FUNCALL       : "\x10",         PTRCALL       : "\x11",
    LOCAL_GET     : "\x20",         LOCAL_SET     : "\x21",
    LOCAL_TEE     : "\x22",        
    GLOBAL_GET    : "\x23",         GLOBAL_SET    : "\x24",
    MEMORY_SIZE   : "\x3F",         MEMORY_GROW   : "\x40",
    MEMORY_COPY   : "\xFC\x0A",     MEMORY_FILL   : "\xFC\x0B",

    funcall(idx) { return Opcodes.FUNCALL + bytestr_leb128(idx); },
                                                        
    // I'm only using WASM's i32 type for bools and pointers,
    // so I just n:ed a subset of unsigned 32 bit operations
    U32_LOAD      : "\x28",         U32_STORE     : "\x36",
    U32_CONST     : "\x41",         U32_EQZ       : "\x45",
    U32_EQ        : "\x46",         U32_NE        : "\x47",
    U32_LT        : "\x49",         U32_GT        : "\x4B",
    U32_LE        : "\x4D",         U32_GE        : "\x4F",
    U32_ADD       : "\x6A",         U32_SUB       : "\x6B",
    U32_MUL       : "\x6C",         U32_AND       : "\x71",
    U32_OR        : "\x72",         U32_XOR       : "\x73",
    U32_SHL       : "\x74",         U32_SHR       : "\x76",

    // All integer arithmetic is done with WASM's signed i64
    I64_LOAD_I8   : "\x30",         LOAD_U8       : "\x31",
    I64_LOAD_I16  : "\x32",         LOAD_U16      : "\x33",
    I64_LOAD_I32  : "\x34",         LOAD_U32      : "\x35",
    I64_LOAD_B64  : "\x29",                  
    I64_STORE_B8  : "\x3C",         STORE_B16     : "\x3D",
    I64_STORE_B32 : "\x3E",         STORE_B64     : "\x37",
    I64_CONST     : "\x42",         I64_EQZ       : "\x50",
    I64_EQ        : "\x51",         I64_NE        : "\x52",
    I64_LT        : "\x53",         I64_GT        : "\x55",
    I64_LE        : "\x57",         I64_GE        : "\x59",
    I64_ADD       : "\x7C",         I64_SUB       : "\x7D",
    I64_MUL       : "\x7E",         I64_DIV       : "\x7F",
    I64_MOD       : "\x81",         I64_AND       : "\x83",
    I64_OR        : "\x84",         I64_XOR       : "\x85",
    I64_SHL       : "\x86",         I64_SAR       : "\x87",
    I64_SHR       : "\x88",         I64_CNT       : "\x7B",
    I64_ROL       : "\x89",         I64_ROR       : "\x8A",
    I64_CLZ       : "\x79",         I64_CTZ       : "\x7A",

    F32_LOAD      : "\x2A",         F32_STORE     : "\x38",
    F32_CONST     : "\x43",
    F32_EQ        : "\x5B",         F32_NE        : "\x5C",
    F32_LT        : "\x5D",         F32_GT        : "\x5E",
    F32_LE        : "\x5F",         F32_GE        : "\x60",
    F32_ADD       : "\x92",         F32_SUB       : "\x93",
    F32_MUL       : "\x94",         F32_DIV       : "\x95",
    F32_MIN       : "\x96",         F32_MAX       : "\x97",
    F32_COPYSIGN  : "\x98",         F32_SQRT      : "\x91",
    F32_ABS       : "\x8B",         F32_NEG       : "\x8C",
    F32_CEIL      : "\x8D",         F32_FLOOR     : "\x8E",
    F32_TRUNC     : "\x8F",         F32_ROUND     : "\x90",
                                              
    F64_LOAD      : "\x2B",         F64_STORE     : "\x39",
    F64_CONST     : "\x44",                               
    F64_EQ        : "\x61",         F64_NE        : "\x62",
    F64_LT        : "\x63",         F64_GT        : "\x64",
    F64_LE        : "\x65",         F64_GE        : "\x66",
    F64_ADD       : "\xA0",         F64_SUB       : "\xA1",
    F64_MUL       : "\xA2",         F64_DIV       : "\xA3",
    F64_MIN       : "\xA4",         F64_MAX       : "\xA5",
    F64_COPYSIGN  : "\xA6",         F64_SQRT      : "\x9F",
    F64_ABS       : "\x99",         F64_NEG       : "\x9A",
    F64_CEIL      : "\x9B",         F64_FLOOR     : "\x9C",
    F64_TRUNC     : "\x9D",         F64_ROUND     : "\x9E",

    //i64_const(val) { return Opcodes.I64_CONST + bytestr_leb128(val); },
    //f64_const(val) { return Opcodes.F64_CONST + bytestr_f64(val); },

    // The TO are numeric conversions.  The AS are bit copies.
    I32_TO_I64    : "\xAC",
    U32_TO_I64    : "\xAD",         I64_TO_U32    : "\xA7",
    F32_TO_F64    : "\xBB",         F64_TO_F32    : "\xB6",
    I64_TO_F32    : "\xB4",         F32_TO_I64    : "\xAE",
    I64_TO_F64    : "\xB9",         F64_TO_I64    : "\xB0",
    I64_AS_F64    : "\xBF",         F64_AS_I64    : "\xBD",
    F32_AS_I32    : "\xBC",         I32_AS_F32    : "\xBE",

    V128_LOAD     : "\xFD\x00",     V128_STORE    : "\xFD\x0B",
    V128_CONST    : "\xFD\x0C",    
    I8X8_LOAD     : "\xFD\x01",     U8X8_LOAD     : "\xFD\x02",
    I16X4_LOAD    : "\xFD\x03",     U16X4_LOAD    : "\xFD\x04",
    I32X2_LOAD    : "\xFD\x05",     U32X2_LOAD    : "\xFD\x06",
    B8_SPLOAD     : "\xFD\x07",     B16_SPLOAD    : "\xFD\x08",
    B32_SPLOAD    : "\xFD\x09",     B64_SPLOAD    : "\xFD\x0A",
                                              
    // SHUFFLE mixes two v128 using an immediate of indices
    // SWIZZLE mixes one v128 using a second v128 for indices 
    V128_SHUFFLE  : "\xFD\x0D",     V128_SWIZZLE  : "\xFD\x0E",
    B8X16_SPLAT   : "\xFD\x0F",     B16X8_SPLAT   : "\xFD\x10",
    B32X4_SPLAT   : "\xFD\x11",     B64X2_SPLAT   : "\xFD\x12",
    F32X4_SPLAT   : "\xFD\x13",     F64X2_SPLAT   : "\xFD\x14",
                                              
    I8X16_GETLANE : "\xFD\x15",     U8X16_GETLANE : "\xFD\x16",
    I16X8_GETLANE : "\xFD\x18",     U16X8_GETLANE : "\xFD\x19",
    B32X4_GETLANE : "\xFD\x1B",     B64X2_GETLANE : "\xFD\x1D",
    F32X4_GETLANE : "\xFD\x1F",     F64X2_GETLANE : "\xFD\x21",
                                   
    B8X16_SETLANE : "\xFD\x17",     B16X8_SETLANE : "\xFD\x1A",
    B32X4_SETLANE : "\xFD\x1C",     B64X2_SETLANE : "\xFD\x1E",
    F32X4_SETLANE : "\xFD\x20",     F64X2_SETLANE : "\xFD\x22",
                                   
    B8X16_EQ      : "\xFD\x23",     B8X16_NE      : "\xFD\x24",
    I8X16_LT      : "\xFD\x25",     U8X16_LT      : "\xFD\x26",
    I8X16_GT      : "\xFD\x27",     U8X16_GT      : "\xFD\x28",
    I8X16_LE      : "\xFD\x29",     U8X16_LE      : "\xFD\x2A",
    I8X16_GE      : "\xFD\x2B",     U8X16_GE      : "\xFD\x2C",
                                   
    B16X8_EQ      : "\xFD\x2D",     B16X8_NE      : "\xFD\x2E",
    I16X8_LT      : "\xFD\x2F",     U16X8_LT      : "\xFD\x30",
    I16X8_GT      : "\xFD\x31",     U16X8_GT      : "\xFD\x32",
    I16X8_LE      : "\xFD\x33",     U16X8_LE      : "\xFD\x34",
    I16X8_GE      : "\xFD\x35",     U16X8_GE      : "\xFD\x36",
                                   
    B32X4_EQ      : "\xFD\x37",     B32X4_NE      : "\xFD\x38",
    I32X4_LT      : "\xFD\x39",     U32X4_LT      : "\xFD\x3A",
    I32X4_GT      : "\xFD\x3B",     U32X4_GT      : "\xFD\x3C",
    I32X4_LE      : "\xFD\x3D",     U32X4_LE      : "\xFD\x3E",
    I32X4_GE      : "\xFD\x3F",     U32X4_GE      : "\xFD\x40",

    B64X2_EQ      : "\xFD\xD6\x01", B64X2_NE      : "\xFD\xD7\x01",
    I64X2_LT      : "\xFD\xD8\x01", I64X2_GT      : "\xFD\xD9\x01",
    I64X2_LE      : "\xFD\xDA\x01", I64X2_GE      : "\xFD\xDB\x01",
    // There don't seem to be any unsigned 64 bit comparison opcodes
    //   U64X2_LE  U64X2_LT  U64X2_GT  U64X2_GE

    F32X4_EQ      : "\xFD\x41",     F32X4_NE      : "\xFD\x42",
    F32X4_LT      : "\xFD\x43",     F32X4_GT      : "\xFD\x44",
    F32X4_LE      : "\xFD\x45",     F32X4_GE      : "\xFD\x46",

    F64X2_EQ      : "\xFD\x47",     F64X2_NE      : "\xFD\x48",
    F64X2_LT      : "\xFD\x49",     F64X2_GT      : "\xFD\x4A",
    F64X2_LE      : "\xFD\x4B",     F64X2_GE      : "\xFD\x4C",
};

//}}}
//{{{ runwasm  XXX: delete this?

const runlib = {
    imports: [
        ["logi64", [WASM_I64],  [],
            (x) => console.log("LOGI64:", x)
        ],
        ["logf64", [WASM_F64],  [],
            (x) => console.log("LOGF64:", x)
        ],
    ],
    locals: [
    ],
    globals: [
        ["foo", WASM_F64],
        ["bar", WASM_I64],
    ],

    func(name) {
        for (let ii = 0; ii<this.imports.length; ++ii) {
            if (this.imports[ii][0] === name) return ii;
        }
        panic(`runlib.func can't find ${name}`);
    }
}

async function runwasm(...bytestrs) {
    const bytecode = bytestrs.join('');

    const assembler = new Assembler();
    assembler.new_memory(64); // create scratch at the bottom

    const imports = {};
    for (const [name, params, results, lambda] of runlib.imports) {
        assembler.new_import(name, params, results);
        imports[name] = lambda;
    }

    for (const [name, type] of runlib.globals) {
        assembler.new_global(type);
    }

    const run = assembler.new_export("run", [], []);
    // XXX: add some locals like x and y for debugging
    run.set_code(bytecode);

    const binary = assembler.assemble();
    writeFileSync("binary.wasm", binary);

    const exports = (await WebAssembly.instantiate(
        binary, { env: imports }
    )).instance.exports;
    profile("running", () => exports.run());
}

//}}}
//{{{ tokenize 

function tokenize(lines) {
    const keywords = new Set([
        "let", "var", "type", "func", "while", "for", "if", "elif", "else",
        "break", "continue", "return", "new", "struct", "union", "assert",
    ]);
    const reserved = new Set([
        "pragma", "enum", "match", "macro",
        "try", "throw", "catch", "yield",
    ]);
    const booleans = new Map([["true", 1], ["false", 2]]);

    const white = /[ \t\r]*/y;
    const newline = /[\n]/y;
    const table = [ // the order matters here to get longest matches first
        [floating, /\d[0-9_]*\.[0-9_]+(?:[eE][-+]?[0-9_]+)?f?i?(?!\w|\.)/y],
        [integer,  /((0b[01_]+)|(0x[0-9a-fA-F_]+)|(\d[0-9_]*))(?!\w|\.)/y],
        [alphabet, /\$?[a-zA-Z_]\w*/y],
        // Regex syntax has too many escapes for this set of punctuation marks,
        // so we list all the possibilities then split, escape, join, and
        // compile it.  The longer patterns must be earlier for alternation to
        // work correctly. There must be exactly one space between tokens.
        [punctuation, new RegExp((
            "[ ] ( ) { } . , ; : ? === == => = !== != ! <= < >= > " +
            "|= | &= & += + -= - *= * /= / %= % \\= \\ @= @ ^= ^ ' ~"
        ).split(" ").map(s => "\\" + s.split("").join("\\")).join("|"), "y")],
    ];

    let row = 0, col = 0;
    const tokens = [];

    for (;;) {
        const line = lines[row];
        if (line === undefined) break;

        for (;;) {
            white.lastIndex = col;
            white.exec(line);
            col = white.lastIndex;
            newline.lastIndex = col;
            if (newline.exec(line)) panic(
                "newline in the line (forgot to use String.raw)"
            );
            const chr = line[col];
            if (chr === undefined || chr === "#") {
                ++row;
                col = 0;
                break;
            }
            if (chr === '"') {
                const [str, end] = string(line, row, col);
                col = end;
                tokens.push(str);
                break;
            }

            tokens.push(scan(line));
        }
    }

    return tokens;

    function scan(line) {
        for (let [build, regex] of table) {
            regex.lastIndex = col;
            const match = regex.exec(line);
            if (match) {
                const result = build(row, col, match[0]);
                col = regex.lastIndex;
                return result;
            }
        }
        error(row, col, "lexical error");
    }

    function floating(row, col, text) {
        let value = parseFloat(text.replaceAll('_', ''));
        if (text[text.length - 1] === "i") {
            if (text[text.length - 2] === "f") {
                return new LiteralCF32(row, col, text, value);
            }
            return new LiteralCF64(row, col, text, value);
        }
        if (text[text.length - 1] === "f") {
            return new LiteralF32(row, col, text, value);
        }
        return new LiteralF64(row, col, text, value);
    }

    function integer(row, col, text) {
        let value = BigInt(text.replaceAll('_', ''));
        if (value > 9223372036854775807n) {
            error(row, col, "integer overflow");
        }
        if (value < -9223372036854775808n) {
            error(row, col, "integer underflow");
        }
        return new LiteralI64(row, col, text, value);
    }

    function alphabet(row, col, text) {
        if (reserved.has(text)) error(
            row, col, `'${text}' is a reserved word`
        );
        if (keywords.has(text)) {
            return new Keyword(row, col, text);
        }
        if (booleans.has(text)) {
            const value = booleans.get(text);
            return new LiteralBool(row, col, text, value);
        }
        if (text[0] === "$") {
            return new LiteralSym(row, col, text);
        }
        return new Identifier(row, col, text);
    }

    function punctuation(row, col, text) {
        return new Punctuation(row, col, text);
    }

    function string(line, row, col) {
        const BACK_SLASH = 0x5C, DOUBLE_QUOTE = 0x22, NEWLINE = 0x0A;
        let bytes = "";
        let ptr = col + 1;
        for (;;) {
            // Note: strings and comments are allowed to have unicode,
            // but the rest of the source code is expected to be ascii.
            const ord = line.codePointAt(ptr);
            if (ord == DOUBLE_QUOTE) { break; }
            if (ord == NEWLINE) error(row, ptr,
                "unexpected newline in string literal"
            );
            if (ord === BACK_SLASH) {
                switch (line[ptr + 1]) {
                    case  'n': { bytes += "\n"; ptr += 2; continue; }
                    case  't': { bytes += "\t"; ptr += 2; continue; }
                    case  '0': { bytes += "\0"; ptr += 2; continue; }
                    case  '"': { bytes += "\""; ptr += 2; continue; }
                    case '\\': { bytes += "\\"; ptr += 2; continue; }
                    case  'x': {
                        const value = hexnum(line, row, ptr + 2, 2);
                        bytes += String.fromCodePoint(value);
                        ptr += 4;
                        continue;
                    }
                    case  'u': {
                        const value = hexnum(line, row, ptr + 2, 4);
                        bytes += String.fromCodePoint(value);
                        ptr += 6;
                        continue;
                    }
                    case  'U': {
                        const value = hexnum(line, row, ptr + 2, 6);
                        bytes += String.fromCodePoint(value);
                        ptr += 8;
                        continue;
                    }
                    default: error(
                        row, ptr, "unexpected character as escape sequence"
                    );
                }
            }
            // convert the ordinal to UTF-8 bytes, and increment
            // by 2 if the source code had a surrogates pair
            bytes += bytestr_utf8(String.fromCodePoint(ord));
            ptr += ord > 0xFFFF ? 2 : 1;
        }
        let text = line.slice(col + 1, ptr);
        return [new LiteralStr(row, col, text, bytes), ptr + 1];
    }

    function hexnum(line, row, col, len) {
        let hex = line.slice(col, col + len);
        for (let ii = 0; ii<len; ++ii) {
            if (!/[0-9a-fA-F]/.test(hex[ii])) {
                error(row, col + ii, "expected hex digit");
            }
        }
        return parseInt(hex, 16);
    }
}

class Keyword extends FrozenRecord {
    constructor(row, col, text) {
        super({ row, col, text });
    }
}

class Identifier extends FrozenRecord {
    constructor(row, col, text) {
        super({ row, col, text });
    }

    /* implements

    lvalue(env) {
    }

    rvalue(env) {
    }

    */
}

class Punctuation extends FrozenRecord {
    constructor(row, col, text) {
        super({ row, col, text });
    }
}

//}}}
//{{{ Literal

class LiteralBool extends FrozenRecord {
    constructor(row, col, text, value) {
        super({ row, col, text, value });
    }
}

class LiteralI64 extends FrozenRecord {
    constructor(row, col, text, value) {
        super({ row, col, text, value });
    }

    evaluate(env) {
        return [
            new CodeConstI64("LiteralI64", this.value),
            new ValueOnStack(TYPE_I64),
        ];
    }
}

class LiteralF32 extends FrozenRecord {
    constructor(row, col, text, value) {
        super({ row, col, text, value });
    }

    evaluate(env) {
        return [
            new CodeConstF32("LiteralF32", this.value),
            new ValueOnStack(TYPE_F32),
        ];
    }
}

class LiteralF64 extends FrozenRecord {
    constructor(row, col, text, value) {
        super({ row, col, text, value });
    }

    evaluate(env) {
        return [
            new CodeConstF64("LiteralF64", this.value),
            new ValueOnStack(TYPE_F64),
        ];
    }
}

class LiteralCF64 extends FrozenRecord {
    constructor(row, col, text, value) {
        super({ row, col, text, value });
    }

    evaluate(env) {
        return [
            new CodeConstCF64("LiteralCF64", 0.0, this.value),
            new ValueOnStack(TYPE_CF64),
        ];
    }
}

class LiteralCF32 extends FrozenRecord {
    constructor(row, col, text, value) {
        super({ row, col, text, value });
    }

    evaluate(env) {
        return [
            new CodeConstCF32("LiteralCF32", 0.0, this.value),
            new ValueOnStack(TYPE_CF32),
        ];
    }
}

class LiteralSym extends FrozenRecord {
    constructor(row, col, text) {
        // Symbols are interned so we can compare
        // with a unique integer for each one
        if (!LiteralSym.map) {
            LiteralSym.map = new Map();
            LiteralSym.value = 0;
        }
        let value = LiteralSym.map.get(text);
        if (!value) {
            value = ++LiteralSym.value;
            LiteralSym.set(text, value);
        }
        super({ row, col, text, value });
    }
}

class LiteralStr extends FrozenRecord {
    // XXX: Need to intern these for re-use
    // XXX: Need to insert these in the assembler
    constructor(row, col, text, bytes) {
        super({ row, col, text, bytes });
    }
}


//}}}
//{{{ parse 

function parse(tokens) {
    const stmtmap = new Map([
        ["let",      letorvarstmt],
        ["var",      letorvarstmt],
        ["type",     typestmt],
        ["func",     funcstmt],
        ["if",       ifthen],
        ["while",    whilestmt],
        ["for",      forstmt],
        ["break",    breakorcont],
        ["continue", breakorcont],
        ["return",   returnstmt],
        ["assert",   assertstmt],
    ]);

    const result = codeblock();
    if (tokens.length) error(
        tokens[0].row, tokens[0].col,
        "unexpected token " + tokens[0].text
    );
    return result;

    function codeblock() {
        const list = [];
        for (;;) {
            while (match(";")); // skip semis
            const ss = statement();
            if (!ss) break;
            list.push(ss);
        }
        return list;
    }

    function statement() {
        if (kindof(tokens[0]) === Keyword) {
            const func = stmtmap.get(tokens[0].text);
            if (func) return func(tokens.shift());
        }

        const lvals = commalist(expression);
        if (lvals.length === 0) return curlyblock();

        const equal = match("=");
        if (equal) {
            const rvals = commalist(expression);
            if (rvals.length === 0) error(
                equal.row, equal.col, "expected rvalue for assignment"
            );
            const last = rvals[rvals.length - 1];
            expectsemi(last.row, last.col);
            return new AssignStatement(equal.row, equal.col, lvals, rvals);
        }

        const exprs = lvals;
        const first = exprs[0];
        const last = exprs[exprs.length - 1];
        expectsemi(last.row, last.col);
        return new ExprStatement(first.row, first.col, exprs);
    }

    function curlyblock() {
        const cc = match("{");
        if (!cc) return null;
        const list = codeblock();
        if (!match("}")) error(
            cc.row, cc.col, "expected closing curly"
        );
        return new BlockStatement(cc.row, cc.col, list);
    }

    function expectsemi(row, col) {
        if (!match(";")) error(row, col, "expected semicolon");
    }

    function letorvarstmt(kwd) {
        const lhs = commalist(kwd === "var" ? variable : constant);
        const eq = match("=");
        if (!eq) error(
            kwd.row, kwd.col, "expected assignment in variable declaration"
        );
        const rhs = commalist(expression);
        expectsemi(eq.row, eq.col);
        return new LetOrVarStatement(kwd.row, kwd.col, lhs, rhs);
    }

    function variable(ctor) { return namedvalue(Variable); }
    function constant(ctor) { return namedvalue(Constant); }

    function namedvalue(ctor) {
        const name = ident();
        if (!name) null;
        const cc = match(":");
        if (cc) {
            const type = typespec();
            if (!type) error(
                cc.row, cc.col, "expected type specifier"
            );
            return new ctor(
                name.row, name.col, name.text, type
            );
        }
        return new ctor(
            name.row, name.col, name.text, null
        );
    }

    function typestmt(kwd) {
        const name = ident();
        if (!name) error(
            kwd.row, kwd.col, "expected name with type definition"
        );
        const equal = match("=");
        if (!equal) error(
            name.row, name.col, "expected assignment with type definition"
        );
        const alias = typespec();
        if (alias) {
            return new TypeStatement(kwd.row, kwd.col, name.text, alias);
        }
        const kind = unistruct();
        if (!kind) error(name.row, name.col,
            "expected struct, union, or alias type definition"
        );
        return new TypeStatement(kwd.row, kwd.col, name.text, kind);
    }

    function unistruct() {
        const which = keyword("struct") ?? keyword("union");
        if (which) {
            const fields = nestedlist("(", fielditem, ")");
            if (!fields) error(
                struct.row, struct.col, "expected list of field definitions"
            );
            return which.text === "struct" ?
                new StructType(which.row, which.col, fields) :
                new  UnionType(which.row, which.col, fields)
            ;
        }
        return null;
    }

    function fielditem() {
        const name = ident();
        if (name) {
            const colon = match(":");
            if (!colon) error(
                name.row, name.col, "expected colon after field name"
            );
            const type = typespec() ?? unistruct();
            if (!type) error(
                colon.row, colon.col, "expected type after colon"
            );
            return new FieldItem(name.row, name.col, name.text, type);
        }
        return unistruct();
    }

    function funcstmt(kwd) {
        const name = match(
            "|", "&", "==", "!=", "<", "<=", ">", ">=",
            "+", "-", "*", "/", "%", "\\", "@", "^", "!", "'", "!",
        ) ?? ident();
        if (!name) error(
            kwd.row, kwd.col, "expected name or operator"
        );
        const params = nestedlist("(", parameter, ")");
        if (!params) error(
            name.row, name.col, "expected parameter list"
        );
        const results = match(":") ? commalist(typespec) : null;
        const body = curlyblock();
        if (!body) error(
            params.row, params.col, "expected function body"
        );
        return new FuncStatement(
            kwd.row, kwd.col, name.text, params, results, body.list
        );
    }

    function parameter() {
        const name = ident();
        if (!name) return null;
        const colon = match(":");
        if (!colon) error(name.row, name.col,
            "expected colon after parameter name"
        );
        const type = typespec();
        if (!type) error(
            colon.row, colon.col,
            "parameter requires type specifier"
        );
        const equal = match("=");
        if (!equal) return new RequiredParam(
            name.row, name.col, name.text, type
        );
        const expr = expression();
        if (!expr) error(equal.row, equal.col,
            "expected expression for default value"
        );
        return new OptionalParam(
            name.row, name.col, name.text, type, expr
        );
    }

    function ifthen(kwd) {
        const cond = expression();
        if (!cond) error(kwd.row, kwd.col, "expected expression");
        const then = curlyblock();
        if (!then) error(cond.row, cond.col, "expected curly block");
        const other = elifelse();
        return new IfElseStatement(kwd.row, kwd.col, cond, then.list, other);
    }

    function elifelse() {
        const elif = keyword("elif");
        if (elif) {
            const cond = expression();
            if (!cond) error(elif.row, elif.col, "expected expression");
            const then = curlyblock();
            if (!then) error(cond.row, cond.col, "expected curly block");
            const other = elifelse();
            return new IfElseStatement(elif.row, elif.col, cond, then.list, other);
        }

        const other = keyword("else");
        if (other) {
            const body = curlyblock();
            if (!body) error(other.row, other.col, "expected curly block");
            return body.list;
        }

        return null;
    }

    function whilestmt(kwd) {
        const colon = match(":");
        if (!colon) return whilelabel(kwd, null);
        const label = ident();
        if (!label) error(
            colon.row, colon.col, "expectected label name after colon"
        );
        return whilelabel(kwd, label.text);
    }

    function whilelabel(kwd, label) {
        const cond = expression();
        if (!cond) error(
            label.row, label.col, "expected condition with while loop"
        );
        const body = curlyblock();
        if (!body) error(
            cond.row, cond.col, "expected curly block with while loop"
        );
        return new WhileStatement(kwd.row, kwd.col, label, cond, body.list);
    }

    function forstmt(kwd) {
        const colon = match(":");
        if (!colon) return forlabel(kwd, null);
        const label = ident();
        if (!label) error(
            colon.row, colon.col, "expectected label name after colon"
        );
        return forlabel(kwd, label.text);
    }

    function forlabel(kwd, label) {
        const lhs = commalist(vardecl);
        const eq = match("=");
        if (!eq) error(
            kwd.row, kwd.col, "expected equal sign in for loop"
        );
        const rhs = expression();
        if (!rhs) error(
            eq.row, eq.col, "expected expression in for loop"
        );
        const body = curlyblock();
        if (!body) error(
            rhs.row, rhs.col, "expected curly block with for loop"
        );
        return new ForStatement(kwd.row, kwd.col, label, lhs, rhs, body.list);
    }

    function breakorcont(kwd) {
        assert(kwd.text === "break" || kwd.text === "continue");
        const iscont = kwd.text === "continue";
        const label = ident();
        expectsemi(kwd.row, kwd.col);
        return new JumpStatement(kwd.row, kwd.col, iscont, label.text);
    }

    function returnstmt(kwd) {
        const values = commalist(expression);
        expectsemi(kwd.row, kwd.col);
        return new ReturnStatement(kwd.row, kwd.col, values);
    }

    function assertstmt(kwd) {
        const cond = expression();
        if (!cond) error(kwd.row, kwd.col,
            "expected condition with 'assert' statement"
        );
        const comma = match(",");
        if (comma) {
            const msg = expression();
            if (!msg) error(comma.row, comma.col,
                "expected message after comma in assert"
            );
            expectsemi(kwd.row, kwd.col);
            return new AssertStatement(kwd.row, kwd.col, cond, msg);
        }
        expectsemi(kwd.row, kwd.col);
        return new AssertStatement(kwd.row, kwd.col, cond, null);
    }

    function typespec() {
        let base = ident() ?? lambdatype();
        if (!base) return null;
        for (;;) {
            const specs = nestedlist("[", specifier, "]");
            if (!specs) break;
            base = new SpecifiedType(base.row, base.col, base, specs);
        }
        return base;
    }

    function specifier() { return integer() ?? typespec(); }

    function lambdatype() {
        const paren = match("(");
        if (paren) {
            const params = commalist(typespec);
            const arrow = match("=>");
            if (!arrow) error(
                paren.row, paren.col, "expected arrow in lambda type"
            );
            const results = commalist(typespec);
            if (!match(")")) error(
                arrow.row, arrow.col, "expected closing paren for lambda type"
            );
            return new LambdaType(paren.row, paren.col, params, results);
        }
        return null;
    }

    function expression() {
        // This is one of the few places where we need a bit of look ahead.
        // If we see either of the following, it's a lambda expression,
        // otherwise we parse it as a simple expression:

        // LPAREN RPAREN
        if (kindof(tokens[0]) === Punctuation && tokens[0].text === "(" &&
            kindof(tokens[1]) === Punctuation && tokens[1].text === ")"
        ) return lambdafunc(tokens[0].row, tokens[0].col);

        // LPAREN IDENT COLON
        if (kindof(tokens[0]) === Punctuation && tokens[0].text === "(" &&
            kindof(tokens[1]) === Identifier &&
            kindof(tokens[2]) === Punctuation && tokens[2].text === ":"
        ) return lambdafunc(tokens[0].row, tokens[0].col);

        return ternary();
    }

    function lambdafunc(row, col) {
        const params = nestedlist("(", parameter, ")");
        if (!params) error(row, col, "expected lambda expression");

        for (const pp of params) {
            if (kindof(pp) === OptionalParam) error(pp.row, pp.col,
                "lambda expressions can't have optional parameters"
            );
        }

        const results = match(":") ? commalist(typespec) : null;
        const arrow = match("=>");
        if (!arrow) error(
            row, col, "expected arrow => for lambda expression"
        );

        const body = curlyblock();
        if (body) return new LambdaExpr(row, col, params, results, body.list);

        const expr = expression();
        if (expr) return new LambdaExpr(row, col, params, results, expr);

        error(arrow.row, arrow.col,
            "expected expression or curly block for lambda body"
        );
    }

    function ternary() {
        const cc = binor();
        if (!cc) return null;
        const qq = match("?");
        if (!qq) return cc;
        const tt = expression();
        if (!tt) error(
            qq.row, qq.col, "expected then expression for ternary"
        );
        if (!match(":")) error(
            qq.row, qq.col, "expected colon for ternary"
        );
        const ee = expression()
        if (!ee) error(
            qq.row, qq.col, "expected else expression for ternary"
        );
        return new TernaryExpr(qq.row, qq.col, cc, tt, ee);
    }

    function binor() { return lassoc(binand, "|"); }

    function binand() { return lassoc(bincmp, "&"); }

    function bincmp() {
        return rassoc(binadd, binadd, // non-associative
            "===", "!==", "==", "!=", "<=", "<", ">=", ">"
        );
    }

    function binadd() { return lassoc(binmul, "+", "-"); }

    function binmul() { return lassoc(prefix, "*", "/", "%"); }

    function prefix() {
        const op = match("+", "-", "!", "~");
        if (!op) return binpow();
        const rhs = prefix();
        if (!rhs) error(
            op.row, op.col, "expected rhs for " + op.text
        );
        return new OperatorCall(
            op.row, op.col, op.text,
            new CallArgument(rhs.row, rhs.col, null, rhs)
        );
    }

    function binpow() { return rassoc(suffix, prefix, "^"); }

    function suffix() {
        let base = primary();
        if (!base) return null;
        for (;;) {
            const tick = match("'");
            if (tick) {
                base = new OperatorCall(
                    tick.row, tick.col, tick.text,
                    new CallArgument(base.row, base.col, null, base)
                );
                continue;
            }

            const dot = match(".");
            if (dot) {
                const attr = ident();
                if (attr) {
                    base = new AttrReference(
                        dot.row, dot.col, base, attr.text
                    );
                    continue;
                }

                const lane = integer();
                if (lane) {
                    base = new LaneReference(
                        dot.row, dot.col, base, lane.value
                    );
                    continue;
                }

                error(dot.row, dot.col, "expected attr or lane after dot");
            }

            const items = nestedlist("[", subscript, "]");
            if (items) {
                base = new ItemReference(base.row, base.col, base, items);
                continue;
            }

            const args = nestedlist("(", argument, ")");
            if (args) {
                base = new FunctionCall(base.row, base.col, base, args);
                continue;
            }

            const kwd = keyword("new");
            if (kwd) {
                const type = typespec();
                if (!type) error(kwd.row, kwd.col,
                    "expected type name or specifier after 'new'"
                );
                const init = nestedlist("{", initializer, "}");
                if (init) return new TypeInitializer(
                    kwd.row, kwd.col, type, init
                );
                const args = nestedlist("(", argument, ")");
                if (args) return new TypeConstructor(
                    kwd.row, kwd.col, type, args
                );
                error(type.row, type.col, "expected initializer or constructor");
            }

            return base;
        }
    }

    function subscript() {
        // someday this might do slices and such
        return expression();
    }

    function argument() {
        // This is another place where need just a bit of lookahead,
        // distinguishing named arguments from positional expressions:

        // IDENT EQUAL
        if (kindof(tokens[0]) === Identifier &&
            kindof(tokens[1]) === Punctuation && tokens[1].text === "="
        ) {
            const name = tokens.shift();
            const eqop = tokens.shift();
            const expr = expression();
            if (!expr) error(eqop.row, eqop.col, "expected expression");
            return new CallArgument(name.row, name.col, name, expr);
        }
        
        const expr = expression();
        if (!expr) return null;
        return new CallArgument(expr.row, expr.col, null, expr);
    }

    function primary() {
        const pp = match("(");
        if (pp) {
            const ee = expression();
            if (!ee) error(pp.row, pp.col, "expected expression");
            if (!match(")")) error(
                ee.row, ee.col, "expected closing paren"
            );
            return ee;
        }

        if (kindof(tokens[0]) === LiteralBool) return tokens.shift();
        if (kindof(tokens[0]) === LiteralI64)  return tokens.shift();
        if (kindof(tokens[0]) === LiteralF32)  return tokens.shift();
        if (kindof(tokens[0]) === LiteralF64)  return tokens.shift();
        if (kindof(tokens[0]) === LiteralCF32) return tokens.shift();
        if (kindof(tokens[0]) === LiteralCF64) return tokens.shift();
        if (kindof(tokens[0]) === LiteralSym)  return tokens.shift();
        if (kindof(tokens[0]) === LiteralStr)  return tokens.shift();
        if (kindof(tokens[0]) === Identifier)  return tokens.shift();

        return null;
    }

    function initializer() {
        // Initializer lists can be nested:
        //
        //     let x = matrix[f64] {
        //         { 1, 2, 3 },
        //         { 4, 5, 6 },
        //     };
        //
        return nestedlist("{", initializer, "}") ?? expression();
    }

    function nestedlist(open, item, close) {
        const cc = match(open);
        if (!cc) return null;
        const list = commalist(item);
        if (!match(close)) error(
            cc.row, cc.col, `expected matching ${close}`
        );
        return list;
    }

    function commalist(item) {
        const list = [];
        for (;;) {
            const ii = item();
            if (!ii) break;
            list.push(ii);
            if (!match(",")) break;
        }
        return list;
    }

    function lassoc(next, ...ops) {
        let lhs = next();
        if (!lhs) return null;
        for (;;) {
            const op = match(...ops);
            if (!op) return lhs;
            const rhs = next();
            if (!rhs) error(op.row, op.col, "expected rhs for " + op.text);
            lhs = new OperatorCall(
                op.row, op.col, op.text,
                new CallArgument(lhs.row, lhs.col, null, lhs),
                new CallArgument(rhs.row, rhs.col, null, rhs)
            );
        }
    }

    function rassoc(left, right, ...ops) {
        const lhs = left();
        if (!lhs) return null;
        const op = match(...ops);
        if (!op) return lhs;
        const rhs = right();
        if (!rhs) error(op.row, op.col, "expected rhs for " + op.text);
        return new OperatorCall(
            op.row, op.col, op.text,
            new CallArgument(lhs.row, lhs.col, null, lhs),
            new CallArgument(rhs.row, rhs.col, null, rhs)
        );
    }

    function ident() {
        return kindof(tokens[0]) === Identifier ? tokens.shift() : null;
    }

    function integer() {
        return kindof(tokens[0]) === LiteralI64 ? tokens.shift() : null;
    }

    function keyword(name) {
        if (kindof(tokens[0]) === Keyword) {
            if (tokens[0].text === name) {
                return tokens.shift();
            }
        }
    }

    function match(...patterns) {
        if (kindof(tokens[0]) === Punctuation) {
            if (patterns.includes(tokens[0].text)) {
                return tokens.shift();
            }
        }
        return null;
    }
}

//}}}
//{{{ Statement 

class TypeStatement extends FrozenRecord {
    constructor(row, col, name, rhs) {
        super({ row, col, name, rhs });
    }
    /* implements

        declare(env: Environment): void {

        }

        deftype(env: Environment): void {
            * struct/unions: determine field types and offsets
            * alias: check that the other is `available()`
        }

    */
}

class LetOrVarStatement extends FrozenRecord {
    constructor(row, col, vars, exprs) {
        super({ row, col, vars, exprs });
    }

    declare(env) {
        for (const vv of this.vars) {
            env.names.newname(vv.row, vv.col, vv.name, vv);
        }
    }

    codegen(env) {
        if (this.exprs.length === 1) {
            // The number of locations from the one expression
            // must match the number of vars we have

        } else {
            // Each expression must return one location, and
            // we must match the number of vars we have
        }
        // set all each.initialized = true;

        return new Code("let stmt not ready yet");
    }
}

class Variable extends SealedRecord {
    constructor(row, col, name, type) {
        super({ row, col, name, type,
            initialized: false,
        });
    }

    /* implements

        ready(callables: Array<Callable>): bool {
            if (initialized) return true;
            assert(callables.size > 0);
            let [first, ...rest] = callables;
            error(first.row, first.col, 
                `function call might access named value '${this.name}'`
                + ` declared at row: ${this.row} col: ${this.col}`
                + ` before it is initialized`
            );
            return false;
        }

    */

    // evaluate(...) { ... }
}

class Constant extends SealedRecord {
    constructor(row, col, name, type) {
        super({ row, col, name, type,
            initialized: false,
        });
    }

    // ready(...) { ... }
    // evaluate(...) { ... }
}

class FuncStatement extends SealedRecord {
    constructor(row, col, name, params, results, body) {
        super({ row, col, name, params, results, body });
    }

    // XXX: Remember:  Real functions will have "adapters" when needed.
    // The caller's arguments must be executed from left to right, and
    // the optional parameters must be executed from left to right too.
    // This means we evaluate the args, and call the adapter that will
    // re-order arguments and evaluate default expressions as needed
    // before calling the real function that does the work.

    // XXX: Remember that integer parameters MUST be TYPE_I64.
    // TYPE_U8, TYPE_I8, ... TYPE_U64 are not allowed.

    // Called by NameSpace to implement overloading
    accepts(row, col, positional, optional) {
        // if (positional args work with positional params) {
        //     if (optional args work with optional params) {
        //          return true;
        //     }
        //     error(row, call,
        //         "named argument doesn't match option parameter"
        //     );
        // }
        // return false;
    }

    // Called by NameSpace to implement overloading
    matches(row, col, positional) {
        // if (their positional parameters match ours) {
        //     error(row, col,
        //         "function with equivalent positional parameters exists"
        //     );
        // }
        // return false;
        //
    }

    /* implements

        // Called from FunctionCall or OperatorCall
        funcall(args): [Code, ...Location] {
            // Needs to check dependencies for ready()
        }

        // Called from codegen()
        declare(env: Environment): void {
            env.scope.setfunc(this.name.text, this);
        }

        // Called from codegen()
        compile(env) {
            * We have to compile our body by using a FunctionScope
              and grabbing our dependencies from it
        }


        // XXX: This might all be crap.  What happens if we just
        // compile ourselves as soon as we're called, or during
        // our statement if we're never called?

        dependencies: Set<Available>

        ready(callables): bool {
            if (callables.has(this)) return true;
            callables.add(this);

            for (const dep in this.dependencies) {
                if (!dep.ready(callables)) return false;
            }
            this.dependencies.clear();

            return true;
        }

    */

}

class RequiredParam extends SealedRecord {
    constructor(row, col, name, type) {
        super({ row, col, name, type });
    }
}

class OptionalParam extends SealedRecord {
    constructor(row, col, name, type, expr) {
        super({ row, col, name, type, expr });
    }
}


class ReturnStatement extends FrozenRecord {
    constructor(row, col, values) {
        super({ row, col, values });
    }

    /* implements

        codegen(env: Environment): Code {

        }
    */
}

class AssertStatement extends FrozenRecord {
    constructor(row, col, cond, msg) {
        super({ row, col, cond, msg });
    }

    /* implements

        codegen(env: Environment): Code {

        }
    */
}

class IfElseStatement extends FrozenRecord {
    constructor(row, col, cond, then, other) {
        super({ row, col, cond, then, other });
    }

    /* implements

        codegen(env: Environment): Code {
            * Recursively compiles both branches with a BlockScope
            * Don't forget to update the branch depth
        }
    */
}

class WhileStatement extends FrozenRecord {
    constructor(row, col, label, expr, body) {
        super({ row, col, label, expr, body });
    }

    /* implements

        codegen(env: Environment): Code {
            * Recursively compiles with a LoopScope
            * Don't forget to update the branch depth
        }

    */
}

class ForStatement extends FrozenRecord {
    constructor(row, col, label, lhs, rhs, body) {
        super({ row, col, label, lhs, rhs, body });
    }

    /* implements

        codegen(env: Environment): Code {
            // Seems like we should have 3 kinds of for-loop:
                Enumerator - start, stop, step

                Iterator - gets a structure to keep state

                Generator - rewrites to a lambda function
                    * Create the body as a lambda to be
                      called by the generator function
                    * The generator function has to handle
                      special return values to indicate
                      break, continue, return.
                    * This means break, continue, return
                      are have a different implementation
                      when being used in the lambda of a
                      for loop
        }

    */
}

class JumpStatement extends FrozenRecord {
    // Both break and continue statements are nearly identical.
    // We implement while-loops for-loops with both a wasm-loop
    // and a wasm-block, so the only difference is which one we
    // jump to with the wasm-branch.
    constructor(row, col, iscont, label) {
        super({ row, col, iscont, label });
    }

    /* implements

        codegen(env: Environment): Code {
            * Lookup the branch depth to jump to
        }

    */

}

class AssignStatement extends FrozenRecord {
    constructor(row, col, lvals, rvals) {
        super({ row, col, lvals, rvals });
    }

    // XXX: Check if any lvals are FunctionCall or OperatorCall
    //      error("expression is not assignable");

    /* implements

        codegen(env: Environment): Code {

        }
    */
}

class ExprStatement extends FrozenRecord {
    constructor(row, col, exprs) {
        super({ row, col, exprs });
    }

    codegen(env) {
        let code = [], locs = [];
        for (let expr of this.exprs) {
            const [rcode, ...rlocs] = expr.evaluate(env);
            code.push(rcode);
            locs.push(rlocs);
        }
        const drops = locs.flat().map(x => x.discard()).reverse();
        return new Code("ExprStatement", ...code, ...drops);
    }
}

class BlockStatement extends FrozenRecord {
    constructor(row, col, block) {
        super({ row, col, block });
    }

    codegen(env) {
        const new_names = new NameSpace(env.names);
        const newenv = new Environment(
            env.assem,
            new_names,
            env.cells,
            env.temps,
        );
        const code = codegen(this.block, newenv);
        return new Code("BlockStatement", code);
    }
}

//}}}
//{{{ Expression 

class LambdaExpr extends FrozenRecord {
    constructor(row, col, params, results, body) {
        super({ row, col, params, results, body });
    }

    /* implements

        rvalue() {
            Is this a good time to compile itself?

        }

        lvalue() { error(...) }

    */
}

class TernaryExpr extends FrozenRecord {
    constructor(row, col, cond, then, other) {
        super({ row, col, cond, then, other });
    }

    /* implements

        rvalue() {

        }

        lvalue:
            Pick which of C or D to assign?

            A == B ? C : D   =   E;

            kinda weird

    */
}

class AttrReference extends FrozenRecord {
    constructor(row, col, base, name) {
        super({ row, col, base, name });
    }

    evaluate(env) {
        const [basecode, ...baselocs] = this.base.evaluate(env);
        if (baselocs.length !== 1) error(
            row, col, "left side must return exactly one value"
        );
        return baselocs[0].refattr(
            this.row, this.col, basecode, this.name
        );
    }

    lvalues(env) {

    }
}

class LaneReference extends FrozenRecord {
    constructor(row, col, base, lane) {
        super({ row, col, base, lane });
    }

    /* implements

        lvalue() {

        }

        rvalue() {

        }

    */
}

class ItemReference extends FrozenRecord {
    constructor(row, col, base, items) {
        super({ row, col, base, items });
    }

    /* implements

        lvalue() {

        }

        rvalue() {

        }

    */
}

class OperatorCall extends FrozenRecord {
    constructor(row, col, name, ...args) {
        super({ row, col, name, args });
    }

    evaluate(env) {
        // This is basically the Identifier path for FunctionCall below, but we
        // already know that we have a function name and that we don't have
        // optional arguments to worry about.
        this.args.forEach(x => x.argeval(env));
        const func = env.names.getfunc(
            this.row, this.col, this.name, this.args, []
        );
        return func.funcall(this.row, this.col, env, this.args);
    }
}

class FunctionCall extends FrozenRecord {
    constructor(row, col, base, args) {
        super({ row, col, base, args });
    }

    evaluate(env) {

        if (kindof(this.base) === Identifier) {
            // If the base is an identifier, we're doing overload resolution by
            // asking the namespace for a function that matches our argument
            // list.  We can evaluate our arguments now because we don't really
            // evaluate the base function expression.
            this.args.forEach(x => x.argeval(env));
            const positional = this.args.filter(arg => arg.name === null);
            const optional   = this.args.filter(arg => arg.name !== null);
            const func = env.names.getfunc(
                this.row, this.col, this.base.text, positional, optional
            );
            // We pass the original list of arguments because they
            // must be executed in order.  The function will adapt
            // the arguments to match parameters as needed.
            return func.funcall(this.row, this.col, env, this.args);
        } 
        
        // The base is something more complicated than just an identifier.  It
        // could be callable if it's a lambda.  (Anything else??)  We need to
        // execute the base before the arguments to make sure we execute things
        // from left to right, so we evaluate it first too cuz why not.
        const [basecode, ...baselocs] = this.base.evaluate(env);
        if (funclocs.length !== 1) error(this.row, this.call,
            "expression must return exactly one value to be callable"
        );
        const baseloc = baselocs[0];
        // Again, we pass the original list of arguments so they
        // can be executed in order.
        this.args.forEach(x => x.argeval(env));
        const [callcode, calllocs] = baseloc.ptrcall(
            this.row, this.col, env, this.args
        );
        return [new Code("FunctionCall", basecode, callcode), ...calllocs];
    }
}

class CallArgument extends SealedRecord {
    constructor(row, col, name, expr) {
        super({ row, col, name, expr, code: null, loc: null });
    }

    argeval(env) {
        const [code, ...locs] = this.expr.evaluate(env);
        if (locs.length !== 1) error(this.row, this.col,
            "argument to expression must return one result"
        );
        // Arguments to function calls need to always be on the stack
        [this.code, this.loc] = locs[0].tostack(this.row, this.col, code);
        // XXX: we should probably ask the location to `growi64` for
        // TYPE_U32 and TYPE_I32 if we have them.
    }
}

class TypeInitializer extends FrozenRecord {
    constructor(row, col, type, init) {
        super({ row, col, type, init });
    }
}

class TypeConstructor extends FrozenRecord {
    constructor(row, col, type, args) {
        super({ row, col, type, args });
    }
}

//}}}
//{{{ CrunchType 

class SimpleType extends FrozenRecord {
    constructor(name, type, bytes) {
        super({ name, type, bytes });
    }

    equal(that) { return this === that; }

    membytes() { return this.bytes; }
    wasmtype() { return this.type; }
}

const TYPE_BOOL = new SimpleType("bool", WASM_U32, 1);
const TYPE_SYM  = new SimpleType("sym",  WASM_U32, 4);
const TYPE_I8   = new SimpleType("i8",   WASM_I64, 1);
const TYPE_I16  = new SimpleType("i16",  WASM_I64, 2);
const TYPE_I32  = new SimpleType("i32",  WASM_I64, 4);
const TYPE_I64  = new SimpleType("i64",  WASM_I64, 8);
const TYPE_U8   = new SimpleType("u8",   WASM_I64, 1);
const TYPE_U16  = new SimpleType("u16",  WASM_I64, 2);
const TYPE_U32  = new SimpleType("u32",  WASM_I64, 4);
const TYPE_U64  = new SimpleType("u64",  WASM_I64, 8);
const TYPE_F32  = new SimpleType("f32",  WASM_F32, 4);
const TYPE_F64  = new SimpleType("f64",  WASM_F64, 8);


class ComplexType extends FrozenRecord {
    constructor(name, fieldtype, bytes) {
        super({ name, fieldtype, bytes });
    }

    equal(that) { return this === that; }

    membytes() { return this.bytes; }
    wasmtype() { return WASM_V128; }

    refattr(row, col, name) {
        if (name === "re") return [0*this.bytes/2, this.fieldtype];
        if (name === "im") return [1*this.bytes/2, this.fieldtype];
        error(row, col, `type '${this.name}' does not have field '${name}'`);
    }
}

const TYPE_CF32 = new ComplexType("cf32", TYPE_F32,  8);
const TYPE_CF64 = new ComplexType("cf64", TYPE_F64, 16);


class SimdUnionType extends FrozenRecord {
    constructor() {
        super({});
    }

    equal(that) { return this === that; }

    membytes() { return 16; }
    wasmtype() { return WASM_V128; }

    refattr(row, col, name) {
        if (name === "i8x16") return [0, TYPE_I8X16];
        if (name === "i16x8") return [0, TYPE_I16X8];
        if (name === "i32x4") return [0, TYPE_I32X4];
        if (name === "i64x2") return [0, TYPE_I64X2];
        if (name === "u8x16") return [0, TYPE_U8X16];
        if (name === "u16x8") return [0, TYPE_U16X8];
        if (name === "u32x4") return [0, TYPE_U32X4];
        if (name === "u64x2") return [0, TYPE_U64X2];
        if (name === "f32x4") return [0, TYPE_F32X4];
        if (name === "f64x2") return [0, TYPE_F64X2];
        error(row, col, `type 'v128' does not have field '${name}'`);
    }
}

const TYPE_V128 = new SimdUnionType();

class SimdStructType extends FrozenRecord {
    constructor(name, lanes, fieldtype) {
        super({ name, lanes, fieldtype });
    }

    equal(that) { return this === that; }

    membytes() { return 16; }
    wasmtype() { return WASM_V128; }

    refattr(row, col, name) {
        if (name === "v128") return [0, TYPE_V128];
        error(row, col, `type '${this.name}' does not have field '${name}'`);
    }

    reflane(row, col, lane) {
        if (lane >= this.lanes) error(row, col,
            "lane ${lane} is out of bounds for type '${this.type}'"
        );
        return [lane*16/this.lanes, this.fieldtype];
    }
}

const TYPE_I8X16 = new SimdStructType("i8x16", 16, TYPE_I8);
const TYPE_I16X8 = new SimdStructType("i16x8",  8, TYPE_I16);
const TYPE_I32X4 = new SimdStructType("i32x4",  4, TYPE_I32);
const TYPE_I64X2 = new SimdStructType("i64x2",  2, TYPE_I64);
const TYPE_U8X16 = new SimdStructType("u8x16", 16, TYPE_U8);
const TYPE_U16X8 = new SimdStructType("u16x8",  8, TYPE_U16);
const TYPE_U32X4 = new SimdStructType("u32x4",  4, TYPE_U32);
const TYPE_U64X2 = new SimdStructType("u64x2",  2, TYPE_U64);
const TYPE_F32X4 = new SimdStructType("f32x4",  4, TYPE_F32);
const TYPE_F64X2 = new SimdStructType("f64x2",  2, TYPE_F64);


class StringType {

}

class LambdaType {
    constructor(row, col, params, results) {
        Object.assign(this, { row, col, params, results });
    }
}

class StructType {
    constructor(row, col, fields) {
        Object.assign(this, { row, col, fields });
    }
}

class UnionType {
    constructor(row, col, fields) {
        Object.assign(this, { row, col, fields });
    }
}

class FieldItem {
    constructor(row, col, name, type) {
        Object.assign(this, { row, col, name, type });
    }
}

//}}}
//{{{ NameSpace 

//
// When compiling a `FuncStatement` body the NameSpace will keep track of any
// lookups that fall outside of its own scope and insert them into an
// `externals` list if they are not already `ready`.  When compile returns, the
// FuncStatement will take the externals and use it as their dependencies.
//
// There is a subtlety here, because those name lookups can find variables and
// functions outside of the current scope which are not `ready` yet, and this
// is not an error.  When this happens, the `FunctionScope` will wrap those in
// an `ExternalLookup` that lies and pretends to be `available` before it is.
// It's the job of `FuncStatement` to check their `dependencies` when they are
// called to make sure everything is actually `available` when it's needed.
//

class NameSpace {
    // XXX: fakeready is garbage, right?
    constructor(parent) {
        this.parent = parent;
        this.bindings = new Map();
    }

    newfunc(row, col, name, positional, func) {
        const binding = this.bindings.get(name);
        if (!binding) {
            this.bindings.set(name, new Set([func]));
            return;
        }
        if (kindof(binding) !== Set) error(row, col,
            "the name '${name}' is already in use"
        );
        for (let func of binding) {
            if (func.matches(row, col, positional)) panic(
                "that shouldn't have returned"
            );
        }
        binding.add(func);
    }

    newname(row, col, name, bind) {
        if (this.bindings.has(name)) error(row, col,
            "the name '${name}' is already in use"
        );
        this.bindings.set(name, bind);
    }

    getfunc(row, col, name, positional, optional) {
        const binding = this.bindings.get(name);

        if (!binding) {
            if (!this.parent) error(row, col, `function ${name} not found`);
            return this.parent.getfunc(row, col, name, positional, optional);
        }

        if (binding && kindof(binding) !== Set) {
            // It's not a FuncStatement, is it callable?
            if (binding.funcall) return binding;
            error(row, call, `name '${name}' is not callable`);
        }

        for (let func of binding) {
            if (func.accepts(row, col, positional, optional)) return func;
        }
        if (!this.parent) error(row, col,
            `function ${name} with matching types not found`
        );
        return this.parent.getfunc(row, col, name, positional, optional);
    }

    getname(row, col, name) {
        const binding = this.bindings.get(name);
        if (binding) {
            if (kindof(binding) === Set) error(row, col,
                `can't use function '${name}' as a value`
            );
            return binding;
        }
        if (!this.parent) error(row, col, `name '${name}' not found`);

        const external = this.parent.getname(row, col, name);
        if (!external) panic("shouldn't have returned if it's not found");

        if (this.fakeready) {
            this.externals.add(external);
            return new ExternalReady(external);
        }

        return external;
    }

}

//}}}
//{{{ Environment 

class Environment extends FrozenRecord {
    constructor(assem, builtins, names, cells, temps) {
        super({ assem, builtins, names, cells, temps, deferred: [] });
    }

    incref_and_defer() {
        const cell = this.temps.alloc(WASM_I64);
        this.deffered.push(cell);
        return new Code(
            "incref and defer",
            builtin_call("incref"),
            cell.tee()
        );
    }

    cleanup_deferred() {
        const list = this.deferred.map(
            cell => {
                this.temps.free(cell);
                return new Code(
                    "cleanup cell",
                    cell.get(),
                    builtin_call("decref")
                );
            }
        );
        this.deferred.length = 0;
        return new Code("cleanup deferred", ...list);
    }
}

//}}}
//{{{ builtins

class BuiltinFunction extends FrozenRecord {

    constructor(name, params, results, code) {
        super({ name, params, results, code });
    }

    matches(row, col, positional) {
        // XXX: assume we don't make mistakes with builtins?
        return false;
    }

    accepts(row, col, positional, optional) {
        if (positional.length !== this.params.length) return false;
        for (const ii in this.params) {
            // XXX: We're going to need to deal with arg TYPE_I32
            // and TYPE_U32 being acceptable to param TYPE_I64
            if (this.params[ii] !== positional[ii].loc.type) return false;
        }
        // XXX: will any builtins accept named arguments?
        if (optional.length) error(row, col,
            "function does not accept named arguments"
        );
        return true;
    }

    funcall(row, col, env, args) {
        // XXX: We're going to need to deal with arg TYPE_I32
        // and TYPE_U32 being converted to param TYPE_I64
        const argcode = args.map(arg => arg.code);
        // XXX: This will need to handle (ahem) pointer types, right?
        const reslocs = this.results.map(res => new ValueOnStack(res));
        return [
            new Code("BuiltinImport call", ...argcode, this.code),
            ...reslocs
        ]
    }

    //return new Code(info, Opcodes.FUNCALL, bytestr_u32(index));
}

function debugbool(x) { console.log("BOOL:", x ? "true" : "false", x); }

class Builtins {
    constructor(assem) {
        this.imports = {};
        this.namespace = new NameSpace(null);

        const imports = [
            ["debug", "debugi64", [TYPE_I64], [], x => console.log("I64:", x)],
            ["debug", "debugf64", [TYPE_F64], [], x => console.log("F64:", x)],
            ["debug", "debugbool", [TYPE_BOOL], [], debugbool],
            ["sin", "sinf64", [TYPE_F64], [TYPE_F64], Math.sin],
            ["cos", "cosf64", [TYPE_F64], [TYPE_F64], Math.cos],
            ["tan", "tanf64", [TYPE_F64], [TYPE_F64], Math.tan],
            ["exp", "expf64", [TYPE_F64], [TYPE_F64], Math.exp],
            // XXX: Fill in all of the other Math functions
            ["rand", "rand",  [],         [TYPE_F64], Math.random],

            ["^", "powi64", [TYPE_I64, TYPE_I64], [TYPE_I64], (x, y) => x**y],
            ["^", "powf64", [TYPE_F64, TYPE_F64], [TYPE_F64], (x, y) => x**y],
            ["^", "intpow", [TYPE_F64, TYPE_I64], [TYPE_F64], (x, y) => x**Number(y)],
        ];
        for (const [cname, jsname, cparams, cresults, jscode] of imports) {
            this.imports[jsname] = jscode;
            const wparams = cparams.map(x => x.wasmtype());
            const wresults = cresults.map(x => x.wasmtype());
            const func = assem.new_import(jsname, wparams, wresults);
            this.namespace.newfunc(
                -1, -1, cname, cparams,
                new BuiltinFunction(
                    cname, cparams, cresults,
                    Opcodes.FUNCALL + bytestr_u32(func.index)
                )
            );
        }

        const intrins = [
            // XXX: Pretty much all of these intrinsics  should participate in
            // constant folding

            ["==", [TYPE_I64, TYPE_I64], [TYPE_BOOL], Opcodes.I64_EQ],
            ["!=", [TYPE_I64, TYPE_I64], [TYPE_BOOL], Opcodes.I64_NE],
            ["<",  [TYPE_I64, TYPE_I64], [TYPE_BOOL], Opcodes.I64_LT],
            ["<=", [TYPE_I64, TYPE_I64], [TYPE_BOOL], Opcodes.I64_LE],
            [">",  [TYPE_I64, TYPE_I64], [TYPE_BOOL], Opcodes.I64_GT],
            [">=", [TYPE_I64, TYPE_I64], [TYPE_BOOL], Opcodes.I64_GE],

            ["+", [TYPE_I64, TYPE_I64], [TYPE_I64], Opcodes.I64_ADD],
            ["-", [TYPE_I64, TYPE_I64], [TYPE_I64], Opcodes.I64_SUB],
            ["*", [TYPE_I64, TYPE_I64], [TYPE_I64], Opcodes.I64_MUL],
            ["/", [TYPE_I64, TYPE_I64], [TYPE_I64], Opcodes.I64_DIV],
            ["%", [TYPE_I64, TYPE_I64], [TYPE_I64], Opcodes.I64_MOD],

            ["-", [TYPE_I64], [TYPE_I64],
                // WASM doesn't have an I64_NEG?  So we build it from scratch:
                Opcodes.I64_CONST + bytestr_i64(-1) + Opcodes.I64_MUL
                // It might be better to do it with a subtraction, but that
                // means we'd have to put the zero before the argument.
                // Hopefully the WASM runtime will recognize this pattern.
                //
                // XXX: We *could* fix this easily if we introduced "pre"
                // and "post" code for builtin functions...  Or we do
                // a special Callable type...
                //
            ],

            ["bitand", [TYPE_I64, TYPE_I64], [TYPE_I64], Opcodes.I64_AND],
            ["bitor",  [TYPE_I64, TYPE_I64], [TYPE_I64], Opcodes.I64_OR],
            ["bitxor", [TYPE_I64, TYPE_I64], [TYPE_I64], Opcodes.I64_XOR],
            ["bitshl", [TYPE_I64, TYPE_I64], [TYPE_I64], Opcodes.I64_SHL],
            ["bitshr", [TYPE_I64, TYPE_I64], [TYPE_I64], Opcodes.I64_SHR],
            ["bitsar", [TYPE_I64, TYPE_I64], [TYPE_I64], Opcodes.I64_SAR],
            ["bitrol", [TYPE_I64, TYPE_I64], [TYPE_I64], Opcodes.I64_ROL],
            ["bitror", [TYPE_I64, TYPE_I64], [TYPE_I64], Opcodes.I64_ROR],
            ["biteqz", [TYPE_I64], [TYPE_I64], Opcodes.I64_EQZ],
            ["bitcnt", [TYPE_I64], [TYPE_I64], Opcodes.I64_CNT],
            ["bitclz", [TYPE_I64], [TYPE_I64], Opcodes.I64_CLZ],
            ["bitctz", [TYPE_I64], [TYPE_I64], Opcodes.I64_CTZ],
            ["bitneg", [TYPE_I64], [TYPE_I64],
                // Synthesize bitwise negation from XOR
                Opcodes.I64_CONST + bytestr_i64(-1) + Opcodes.I64_XOR
            ],

            ["==", [TYPE_F64, TYPE_F64], [TYPE_BOOL], Opcodes.F64_EQ],
            ["!=", [TYPE_F64, TYPE_F64], [TYPE_BOOL], Opcodes.F64_NE],
            ["<",  [TYPE_F64, TYPE_F64], [TYPE_BOOL], Opcodes.F64_LT],
            ["<=", [TYPE_F64, TYPE_F64], [TYPE_BOOL], Opcodes.F64_LE],
            [">",  [TYPE_F64, TYPE_F64], [TYPE_BOOL], Opcodes.F64_GT],
            [">=", [TYPE_F64, TYPE_F64], [TYPE_BOOL], Opcodes.F64_GE],

            ["+", [TYPE_F64, TYPE_F64], [TYPE_F64], Opcodes.F64_ADD],
            ["-", [TYPE_F64, TYPE_F64], [TYPE_F64], Opcodes.F64_SUB],
            ["*", [TYPE_F64, TYPE_F64], [TYPE_F64], Opcodes.F64_MUL],
            ["/", [TYPE_F64, TYPE_F64], [TYPE_F64], Opcodes.F64_DIV],

            ["-", [TYPE_F64], [TYPE_F64], Opcodes.F64_NEG],
        ];
        for (const [name, params, results, code] of intrins) {
            this.namespace.newfunc(
                -1, -1, name, params,
                new BuiltinFunction(name, params, results, code)
            );
        }

    }

    call(name) {

    }
}


//}}}
//{{{ Location

// A Region is a block of memory that starts with [refs: u32, chunks: u32]
// Chunks are 32 byte pieces of memory, including the refs and chunks counter.
// Remember that WASM is little endian, so you can increment the refs as a 64
// bit integer if that's convenient.

// A Handle is an i64 value on the stack or in a cell that stores a pointer to
// a Region and an Address within that region [addr: u32, base: u32].  Remember
// that WASM is little endian, so you can re-use a Handle to point to a later
// address by incrementing the i64 as a whole.  And you can get address for
// memory operations by casting from i64 to u32

function typecheck(row, col, lval, rval) {
    if (!lval.type.equal(rval.type)) error(row, col,
        `can't assign '${rval.type.name}' to '${lval.type.name}'`
    );
}

function getlane(offset, type) {
    // We want to pretend that we only work with i64 integers, and so it's
    // temping to have all of I32/U32 results include one more instruction to
    // zero or sign extend to 64 bits.  However, this means that we get into a
    // dumb situation when we're moving from the stack to memory, and right
    // after we convert to i64, we immediately convert back to I32/U32.  We can
    // avoid those two wasted instructions.
    switch (type) {
        case TYPE_I8: return [
            new Code("getlane u8", 
                Opcodes.I8X16_GETLANE +
                bytestr_byte(offset/1)
            ), TYPE_I32
        ];
        case TYPE_I16: return [
            new Code("getlane u16", 
                Opcodes.I16X8_GETLANE +
                bytestr_byte(offset/2)
            ), TYPE_I32
        ];
        case TYPE_I32: return [
            new Code("getlane u32", 
                Opcodes.B32X4_GETLANE +
                bytestr_byte(offset/4)
            ), TYPE_I32
        ];
        case TYPE_I64: return [
            new Code("getlane i64", 
                Opcodes.B64X2_GETLANE +
                bytestr_byte(offset/8)
            ), TYPE_I64
        ];
        case TYPE_U8: return [
            new Code("getlane u8", 
                Opcodes.U8X16_GETLANE +
                bytestr_byte(offset/1)
            ), TYPE_U32
        ];
        case TYPE_U16: return [
            new Code("getlane u16", 
                Opcodes.U16X8_GETLANE +
                bytestr_byte(offset/2)
            ), TYPE_U32
        ];
        case TYPE_U32: return [
            new Code("getlane u32", 
                Opcodes.B32X4_GETLANE +
                bytestr_byte(offset/4)
            ), TYPE_U32
        ];
        case TYPE_U64: return [
            // TYPE_U64 is really kind of a bastard stepchild.  We don't really
            // work with u64 outside of u64x2.  When we extract the lane, it
            // gets converted to i64.  This is ok for everything but divisions
            // and right shifts, and I guess we'll document our way out of it.
            new Code("getlane u64", 
                Opcodes.B64X2_GETLANE +
                bytestr_byte(offset/8)
            ), TYPE_I64 // I64 on purpose
        ];
        case TYPE_F32: return [
            new Code("getlane f32", 
                Opcodes.F32X4_GETLANE +
                bytestr_byte(offset/4)
            ), TYPE_F32
        ];
        case TYPE_F64: return [
            new Code("getlane f64", 
                Opcodes.F64X2_GETLANE +
                bytestr_byte(offset/8)
            ), TYPE_F64
        ];
        case TYPE_I8X16: case TYPE_I16X8: case TYPE_I32X4: case TYPE_I64X2:
        case TYPE_U8X16: case TYPE_U16X8: case TYPE_U32X4: case TYPE_U64X2:
        case TYPE_F32X4: case TYPE_F64X2: case TYPE_V128:
            assert(offset === 0);
            return [new Code("getlane nop"), type];
        default:
            panic("unexpected lane type");
    }
}

class ValueOnStack extends FrozenRecord {
    // A value on the stack can only be an rvalue
    // It is not a reference type
    constructor(type) {
        super({ type });
    }

    assign(row, col, env, rval) {
        error(row, col, "can not assign to the expression");
    }

    discard() {
        return new Code("discard drop " + this.type.name, Opcodes.DROP);
    }

    tostack(row, col, code) {
        // Nothing to do.  Return same code and this location.
        return [new Code("tostack", code), this];
    }

    // XXX: We need some sort of `growi64` to convert TYPE_I32 and TYPE_U32
    // to TYPE_I64 when needed for argument passing or similar.

    // XXX: The OTHER way to do this might be to ALWAYS have loads, stores, and
    // lane operations create TYPE_I64, and then have constant folding detect
    // the remove when an I32 or U32 is promoted and demoted in succession.

    refattr(row, col, basecode, name) {
        if (!this.type.refattr) error(row, col,
            `type ${this.type.name} does not have attributes`
        );
        const [offset, parttype] = this.type.refattr(row, col, name);
        // I'm pretty sure the only time we get here is when the thing on the
        // stack is a V128 that we're interpreting as some specialized type
        // (i8x16, ..., f64x2, ..., cf64, etc...)
        assert(this.type.wasmtype() === WASM_V128, "checking assumptions");
        // The value in the lane could be something like i8 or u16, but now
        // that we've moved it to the stack, it could end up as i32, u32, ...
        const [attrcode, attrtype] = getlane(offset, parttype);
        return [
            new Code("refattr", basecode, attrcode),
            new ValueOnStack(attrtype)
        ];
    }

    reflane(row, col, basecode, lane) {
        // Nearly the same as refattr above.  Same comments apply.
        if (!this.type.reflane) error(row, col,
            `type ${this.type.name} does not have lanes`
        );
        const [offset, parttype] = this.type.reflane(row, col, lane);
        assert(this.type.wasmtype() === WASM_V128, "checking assumptions");
        const [lanecode, lanetype] = getlane(offset, parttype);
        return [
            new Code("reflane", basecode, lanecode),
            new ValueOnStack(attrtype)
        ];
    }
}


class ValueInCell extends FrozenRecord {
    constructor(type, cell) {
        super({ type, cell });
    }

    discard() {
        return new Code(
            "discard value in cell nop"
        );
    }

    assign(row, col, rval) {
        typecheck(row, col, rval, lval);
        return new Code(
            "assign value in cell",
            rval.tostack(), this.cell.set()
        );
    }

    tostack(row, col) {
        return new Code(
            "tostack value in cell",
            this.cell.get()
        );
    }

    refattr() {
    }

    reflane() {
    }

    refitem() {
    }

}

class LaneInCell extends FrozenRecord {
    constructor(type, cell, lane) {
        super({ type, cell, lane });
    }
        /*
        typecheck(row, col, this, rval);
        const rcode   = rval.type.tostack();
        const tmp     = env.scratch.alloc(this.type);
        const store   = rval.type.store(tmp);
        const read    = this.type.tostack();
        const fetch   = rval.type.fetch(tmp);
        const setlane = this.type.setlane();
        const write   = this.type.tolocal();
        */

}

class HandleInCell extends FrozenRecord {

}

class HandleOnStack extends FrozenRecord {

}

class ValueInMemory extends FrozenRecord {

}


//}}}
//{{{ Pools and Cells 

class GlobalPool extends FrozenRecord {
    // This will start allocating GlobalCell
    // after any globals needed for builtins
    constructor(assem) {
        super({ assem });
    }

    alloc(wasmtype) {
        const index = this.assem.new_global(type);
        return new GlobalCell(index, wasmtype);
    }

    free(cell) {
        panic("not supposed to call free() on globals");
    }
}

class GlobalCell extends FrozenRecord {
    constructor(index, type) {
        super({ index, type });
    }

    get() {
        const typename = WASM_TYPES.get(this.type);
        return new Code(
            `GLOBAL_GET(${typename} ${this.index})`,
            Opcodes.GLOBAL_GET + bytestr_u32(this.index)
        );
    }

    set() {
        const typename = WASM_TYPES.get(this.type);
        return new Code(
            `GLOBAL_SET(${typename} ${this.index})`,
            Opcodes.GLOBAL_SET + bytestr_u32(this.index)
        );
    }

    tee() {
        panic("not supposed to call tee() on globals");
    }
}

class LocalPool extends FrozenRecord {
    // This will start allocating LocalCell
    // after the parameters for the function
    constructor(start=0) {
        super({ start, locals: [], freed: new Set()});
    }

    alloc(wasmtype) {
        assert(WASM_TYPES.has(wasmtype), "must be valid wasmtype");
        for (cell of this.freed) {
            if (cell.type === wasmtype) {
                this.freed.delete(cell);
                return cell;
            }
        }
        const index = this.start + this.locals.length;
        this.locals.push(wasmtype);
        return new LocalCell(index, wasmtype);
    }

    free(cell) {
        this.freed.add(cell);
    }
}

class LocalCell extends FrozenRecord {
    constructor(index, type) {
        super({ index, type });
    }

    get() {
        const typename = WASM_TYPES.get(this.type);
        return new Code(
            `LOCAL_GET(${typename} ${this.index})`,
            Opcodes.LOCAL_GET + bytestr_u32(this.index)
        );
    }

    set() {
        const typename = WASM_TYPES.get(this.type);
        return new Code(
            `LOCAL_SET(${typename} ${this.index})`,
            Opcodes.LOCAL_SET + bytestr_u32(this.index)
        );
    }

    tee() {
        const typename = WASM_TYPES.get(this.type);
        return new Code(
            `LOCAL_TEE(${typename} ${this.index})`,
            Opcodes.LOCAL_TEE + bytestr_u32(this.index)
        );
    }
}

//}}}
//{{{ Allocator

const WASM_PAGE_SIZE = 65536;
const BYTES_PER_CHUNK = 32, BITS_PER_UINT32 = 32;
const WORDMAP_LENGTH = Number(1n<<32n)/BYTES_PER_CHUNK/BITS_PER_UINT32;

// For now we're keeping this bit array outside of WASM.  If we ever implement
// alloc and free inside though, then we should move this map to use part of
// the memory itself.  It would be a wrapper around exports.mem, and I think we
// might need to re-wrap everytime the we grow the memory.  In this map, the
// bits are 1 when the chunks are available, and 0 when they are allocated.
const WORDMAP = new Uint32Array(WORDMAP_LENGTH);

class Allocator extends SealedRecord {
    constructor(exports) {
        // Ask exports.grow() for 16MB of pages and get current offset
        // Set this.heapstart so we don't search the wrong place.
        // Set this.heapsize so we know how much we've allocated.
    }

    alloc(bytes, align) {
        let header = 8;
        const extra = header%align;
        if (extra !== 0) header += align - extra; 
        const chunks = Math.ceil((header + bytes)/32);

        const offset = findrun(wordmap, chunks);
        setbits(wordmap, offset, chunks);

        const ending = (offset + chunks)*BYTES_PER_CHUNK
        if (ending > current_heap_size) {
            const pages = Math.ceil(
                (ending - current_heap_size)/WASM_PAGE_SIZE
            );
            memgrow(pages);
            current_heap_size += pages*WASM_PAGE_SIZE;
        }

        memuint32[offset + 0] = 1; // initial reference count
        memuint32[offset + 1] = chunks;

        const address = offset*BYTES_PER_CHUNK + header;

        throw "not enough memory";
    }

    setbits(array, offset, length) {
        const loword = Math.floor(offset / 32);
        const hiword = Math.floor((offset + length - 1) / 32);

        for (let ii = loword; ii <= hiword; ii++) {
            let lobit = ii === loword ? offset % 32 : 0;
            let hibit = ii === hiword ? (offset + length - 1) % 32 : 31;

            if (lobit === 0 && hibit === 31) {
                array[ii] = 0xFFFFFFFF;
            } else {
                let mask = ((1 << (hibit - lobit + 1)) - 1) << lobit;
                array[ii] |= mask;
            }
        }
    }

    clrbits(array, offset, length) {
        const loword = Math.floor(offset / 32);
        const hiword = Math.floor((offset + length - 1) / 32);

        for (let ii = loword; ii <= hiword; ii++) {
            let lobit = ii === loword ? offset % 32 : 0;
            let hibit = ii === hiword ? (offset + length - 1) % 32 : 31;

            if (lobit === 0 && hibit === 31) {
                array[ii] = 0x00000000;
            } else {
                let mask = ((1 << (hibit - lobit + 1)) - 1) << lobit;
                array[ii] &= ~mask;
            }
        }
    }

    findrun(array, length) {
        let start = -1;
        let found = 0;
        for (const ii in array) {
            const word = array[ii];
            if (word === 0xFFFFFFFF) continue;
            for (let mask = 1, jj = 0; jj<32; ++jj, mask <<= 1) {
                const test = word&mask;
                if ((word&mask) === 0) {
                    if (start === -1) {
                        start = ii*32 + jj;
                    }
                    ++found;
                    if (found === length) {
                        return start;
                    }
                } else {
                    start = -1;
                    found = 0;
                }
            }
        }
        throw "out of memory";
    }

    print(array, length) {
        let text = "";
        for (let ii = 0; ii<length; ++ii) {
            text += String(ii%10);
        }
        console.log(text);
        text = "";
        for (let ii = 0; ii<length; ++ii) {
            const word = array[Math.floor(ii/32)]
            const bit = 1<<(ii%32);
            text += (word&bit) ? "1" : "0";
        }
        console.log(text);
        console.log();
    }



}
//}}}
//{{{ Code 

//
// Code are the nodes of the intermediate representation tree.  An instruction
// can contain an ordered list of other Code or ByteStr.  Most of these are
// created using the Code class, but we also have LocalSet and LocalGet as
// special instructions that can't be flattened until later in the compilation
// process.  Additionaly Code can have an internal field like `info` for
// troubleshooting during development.
//
// Note that any place which says it returns an Code could also return a
// ByteStr.  This is simpler but makes things more difficult to understand.
//

function flatten(that) {
    if (kindof(that) === String) return that;
    assert(that.codes !== undefined);
    return that.codes.map(x => flatten(x)).join("");
}

class Code extends FrozenRecord {
    constructor(info, ...codes) {
        super({ info, codes });
    }
}

class CodeConstU32 extends FrozenRecord {
    constructor(info, value) {
        super({ info, value, codes: [
            Opcodes.U32_CONST,
            bytestr_u32(value),
        ]});
    }
}

class CodeConstI64 extends FrozenRecord {
    constructor(info, value) {
        super({ info, value, codes: [
            Opcodes.I64_CONST,
            bytestr_i64(value),
        ]});
    }
}

class CodeConstF32 extends FrozenRecord {
    constructor(info, value) {
        super({ info, value, codes: [
            Opcodes.F32_CONST,
            bytestr_f32(value),
        ]});
    }
}

class CodeConstF64 extends FrozenRecord {
    constructor(info, value) {
        super({ info, value, codes: [
            Opcodes.F64_CONST,
            bytestr_f64(value),
        ]});
    }
}

class CodeConstCF32 extends FrozenRecord {
    constructor(info, real, imag) {
        super({ info, real, imag, codes: [
            Opcodes.V128_CONST,
            bytestr_f32(real),
            bytestr_f32(imag),
            bytestr_zeros(8),
        ]});
    }
}

class CodeConstCF64 extends FrozenRecord {
    constructor(info, real, imag) {
        super({ info, real, imag, codes: [
            Opcodes.V128_CONST,
            bytestr_f64(real),
            bytestr_f64(imag),
        ]});
    }
}

class CodeAddU32 extends FrozenRecord {
    constructor(info, lhs, rhs) {
        super({ info, codes: [
            lhs, rhs, Opcodes.U32_ADD
        ]});
        // XXX: Do constant folding
    }
}

//}}}


function compile(block, env) {
    block.forEach(stmt => stmt.declare?.(env));
    //block = block.map(stmt => stmt.deftype?.(env) ?? stmt);

    const codes = block.map(addr => addr.codegen?.(env) ?? stmt);
    return new Code("compile()", ...codes);
}

async function crunch(source) {
    try {
    
        const lines  = source.split("\n");
        const tokens = profile("tokenize", () => tokenize(lines));
        const astree = profile("parse", () => parse(tokens));

        // Statements in this top level use WASM globals to implement variables
        // and locals for scratch.  Other blocks will use WASM locals for both.
        const assem = new Assembler();
        const builtins = new Builtins(assem);
        const names = new NameSpace(builtins.namespace);
        const cells = new GlobalPool();
        const temps = new LocalPool();
        const env = new Environment(assem, builtins, names, cells, temps);

        const code = profile("codegen", () => compile(astree, env));

        debug(code);
        const flat = flatten(code);
        debug(flat);

        const run = assem.new_export("run", [], []);
        run.set_code(flat);

        const binary = assem.assemble();
        writeFileSync("binary.wasm", binary);

        const exports = (await WebAssembly.instantiate(
            binary, { env: builtins.imports }
        )).instance.exports;
        profile("running", () => exports.run());
    } catch (error) {
        console.log("ERROR:", error);
    }
}

crunch(String.raw`

    #var v = 123;

    debug(2.0^10);

    #debug((1.2i).im);

    #debug(cos(0.1));
    #debug(sin(0.2));
    #debug(rand());


    #(1.2i).re;
    #(3.4i).im;


    #123 + 456;
    #jslog(123);

    #abc = 123;
    #var def = 456;
    #var ghi: i64 = 456;

    #abc;
    #123 + abc;
    #abd + 123;

    #let x = f64x2(1.0, 2.0);
    #x.0;
    #x.1 = 3.0;
    #x.i32x4.i16x8.f64x2.0;


    #123; { 456; } 789;

`);

// TODO: VarStatement
// TODO: FuncStatement matches, accepts

// TODO: Finish memalloc, memfree, and reference counting.
// TODO: memgrow(pages)  (export to JavaScript)
// TODO: builtin alloc   (import from JavaScript)
// TODO: builtin incref  (import from JavaScript)
// TODO: builtin decref  (import from JavaScript)
// TODO: free(address)   (implemented in JavaScript)


// TODO: replace bytes_leb128 calls with bytestr_u32 and bytestr_i64

// TODO: cf64 as v128
// TODO: cf64 as pair of f64???
// TODO: qf64 as pair of v128???
// TODO: Lambda Expressions
// TODO: Location::refitem


// TODO: ShadowStack and LambdaExpr weak references???
// TODO: run(stackptr)???

/*
runwasm(
    Opcodes.I64_CONST, bytestr_leb128(111),
    Opcodes.I64_CONST, bytestr_leb128(123),
    Opcodes.I64_ADD,
    Opcodes.FUNCALL, bytestr_leb128(runlib.func("logi64")),

    Opcodes.F64_CONST, bytestr_f64(4.5),
    Opcodes.FUNCALL, bytestr_leb128(runlib.func("logf64")),
);
*/


/*
crunch(String.raw`
    # A variation on a cliche interview question
    func splashboom(nn:i64):i64 {
        var count = 0;
        var ii = 0;
        while ii < nn {
            if ii%15 == 0 {
                print("pound bang");
            } elif ii%3 == 0 {
                print("thump");
            } elif ii%5 == 0 {
                print("splat");
            } else {
                count = count + 1;
            }
            ii = ii + 1;
        }
        return count;
    }

    func sort(data: matrix[i64, 32, 64], less: (i64, i64 => bool)): {}

    var meta = (x:i64): (f64 => str) => (
        (f:f64): str => "meta"
    );

`);
*/

//////////////////////////////////////////////////////////////////////////
//
// Some cases to think through
//
//////////////////////////////////////////////////////////////////////////
//
//           ExprStatement:   f(); # Calling f before x is ready
//           VarStatement:    var x = 123;
//           FuncStatement:   func f() { print(x); }
//
//////////////////////////////////////////////////////////////////////////
//
//           ExprStatement:   f(); # Calling g before x is ready
//           VarStatement:    var x = 123;
//           FuncStatement:   func f() { g();      }
//           FuncStatement:   func g() { print(x); }
//
/////////////////////////////////////////////////////////////////////////
//
//           BlockStatement   { # read/write x before it's ready
//                                print(x);  # reading
//                                x = 456;   # writing
//                            }
//           VarStatement     var x = 123;
//
/////////////////////////////////////////////////////////////////////////
//
//           VarStatement     var x = f();
//           FuncStatement    func f() {
//                                return x;
//                            }
//
/////////////////////////////////////////////////////////////////////////
//
//           FuncStatement    func f(n: i64) {
//                                if (n == 0) return 2;
//                                return f(n - 1);
//                            }
//
/////////////////////////////////////////////////////////////////////////
//
//           VarStatement     var x = even(123);
//           FuncStatement    func even(n: i64): bool {
//                                if (n == 0) return true;
//                                return odd(n - 1);
//                            }
//           FuncStatement    func odd(n: i64): bool {
//                                if (n == 0) return false;
//                                return even(n - 1);
//                            }
//
/////////////////////////////////////////////////////////////////////////
//
//           VarStatement     var L = f();
//           VarStatement     var x = 1;
//           ExprStatement    L();
//           VarStatement     var y = 2;
//
//           FuncStatement    func f(): LambdaType(params=[], results=[i64]) {
//                                if (random()%2 === 0) {
//                                     return () => x;
//                                } else {
//                                     return () => y;
//                                }
//                            }
//
/////////////////////////////////////////////////////////////////////////

//
// TODO: Walk through unwinding.  Does it work?
//


//
// TODO: Should we just use the line number for error message?
// The column number is almost always not where the problem is.
//


//
// TODO: Do we ever use the scratch cells?
//

//
// XXX: Make a doc section called "unwinding" that talks about that.
//

//
// TODO: Code coverage.  Miss parameters, extra arguments, typos.
//

//
// TODO: Test out lambda functions with runwasm.  I don't think we've called by
// pointer yet, and the sigindex has to go somewhere.
//

//
// TODO: Get str working.  It's a reference type, so we can work through
// those details before stressing about more complicated things.
//

//
// TODO: Decide if stacklets are worth it:
//     Pros:
//         They simplify some of expressions (simd.0 = 123)
//         They let us have closure functions
//     Cons:
//         We mostly quit using Locals and Globals
//         for user code, and that's probably slower
//
// Would we do weak references for lambdas?
//

//
// This is how we do browser vs console:
//
//     typeof window === 'undefined' ? console_main() : browser_main();
//
//

/* JUNK
const TYPE_CF64 = {
    name: "cf64",
    celltypes: [WASM_V128],
    heaptypes: [WASM_V128],
    isref: false,
    bytes: 16,

    equal(that) { return this === that; },

    stack_value_refattr(row, col, env, name) {
        if (name === "re") return [
            new Code("F64X2_GETLANE re",
                Opcodes.F64X2_GETLANE, bytestr_byte(0)
            ),
            new StackValue(TYPE_F64)
        ];
        if (name === "im") return [
            new Code("F64X2_GETLANE im",
                Opcodes.F64X2_GETLANE, bytestr_byte(1)
            ),
            new StackValue(TYPE_F64)
        ];
        error(row, col,
            `left side type 'cf64' does not have ${name} attribute`
        );
    },
};
*/

