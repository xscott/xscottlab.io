// Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

import {
    Pack,
    assert, panic, wrapInt64,
    Uns32, Int64, Flt64, Bytes,
    Opcodes, Assembler, Wasm,
    Instruction,
} from "./index.ts";

export class Nop implements Instruction {
    constructor() {}
    optimize(): Instruction { return this; }
    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return "";
    }
}

export class Drop implements Instruction {
    constructor() {}
    optimize(): Instruction { return this; }
    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return Opcodes.DROP;
    }
}

export class Concat implements Instruction {
    parts: Instruction[];

    constructor(... parts: Instruction[]) {
        this.parts = parts;
    }

    optimize(): Instruction {
        const list: Instruction[] = [];
        for (const item of this.parts) {
            const opt = item.optimize();
            if (opt instanceof Nop) continue;
            if (opt instanceof Concat) {
                list.push(... (opt as Concat).parts);
                continue;
            }
            list.push(opt);
        }
        if (list.length === 0) return new Nop();
        if (list.length === 1) return list[0];
        return new Concat(... list);
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return this.parts.map(
            (aa: Instruction) => aa.emitBytes(assem, flow)
        ).join('');
    }
}

export class FlowControl {
    constructor(
        public parent: null | FlowControl,
        public kind: Instruction
    ) {}
}

function getLabelDepth(flow: null | FlowControl, label: null | string) {
    let depth = 0;
    while (flow !== null) {
        if (flow.kind instanceof ForLoop) {
            if (label === null || label === flow.kind.label) {
                return depth;
            }
            depth += 3;
            flow = flow.parent;
            continue;
        }
        if (flow.kind instanceof IfThen) {
            depth += 1;
            flow = flow.parent;
            continue;
        }
        panic("unhandled kind of FlowControl");
    }

    // We check for the label's existence in Environment.emitJump()
    panic("could not find matching jump/branch loop");
}

export class Jump implements Instruction {
    constructor(
        public label: null | string,
        public extra: number,
    ) {}

    optimize(): Instruction { return this; }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        const depth = getLabelDepth(flow, this.label);
        return (
            Opcodes.JUMP +
            Pack.u32(depth + this.extra)
        );
    }
}

export class Branch implements Instruction {
    constructor(
        public cond: Instruction,
        public label: null | string,
        public extra: number,
    ) {}

    optimize(): Instruction { return this; }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        const depth = getLabelDepth(flow, this.label);
        return (
            this.cond.emitBytes(assem, flow) +
            Opcodes.BRANCH +
            Pack.u32(depth + this.extra)
        );
    }
}

export class ForLoop implements Instruction {
    constructor(
        public label: null | string,
        public init: Instruction,
        public cond: Instruction,
        public iter: Instruction,
        public body: Instruction,
    ) {}

    optimize(): Instruction {
        this.init = this.init.optimize();
        this.cond = this.cond.optimize();
        this.iter = this.iter.optimize();
        this.body = this.body.optimize();
        return this;
    }

    emitBytes(assem: Assembler, parent: null | FlowControl) { 
        const flow = new FlowControl(parent, this);
        if (this.cond instanceof ConstU32) {
            const initBytes = this.init.emitBytes(assem, flow);
            if (this.cond.value === 0) return initBytes;

            const bodyBytes = this.body.emitBytes(assem, flow);
            const iterBytes = this.iter.emitBytes(assem, flow);
            return (
                initBytes +
                Opcodes.BLOCK + "\x40" +
                    Opcodes.LOOP + "\x40" +
                        Opcodes.BLOCK + "\x40" +
                            bodyBytes +
                        Opcodes.END +
                        iterBytes +
                        Opcodes.JUMP + "\x00" +
                    Opcodes.END +
                Opcodes.END
            );
        }

        const initBytes = this.init.emitBytes(assem, flow);
        const condBytes = this.cond.emitBytes(assem, flow);
        const bodyBytes = this.body.emitBytes(assem, flow);
        const iterBytes = this.iter.emitBytes(assem, flow);
        return (
            initBytes +
            condBytes +
            Opcodes.IF + "\x40" +
                Opcodes.LOOP + "\x40" +
                    Opcodes.BLOCK + "\x40" +
                        bodyBytes +
                    Opcodes.END +
                    iterBytes +
                    condBytes +
                    Opcodes.BRANCH + "\x00" +
                Opcodes.END +
            Opcodes.END
        );
    }
}

export class IfThen implements Instruction {
    constructor(
        public type: string,
        public cond: Instruction,
        public then: Instruction,
        public other: Instruction,
    ) {}

    optimize(): Instruction {
        this.cond = this.cond.optimize();
        if (this.cond instanceof ConstU32) {
            let keep = this.cond.value !== 0 ? this.then : this.other;
            return keep.optimize();
        }
        this.then  = this.then.optimize();
        this.other = this.other.optimize();
        if (this.then instanceof Jump && this.other instanceof Nop) {
            return new Branch(this.cond, this.then.label, this.then.extra);
        }
        return this;
    }

    emitBytes(assem: Assembler, parent: null | FlowControl) {
        const flow = new FlowControl(parent, this);
        if (this.other instanceof Nop) return (
            this.cond.emitBytes(assem, flow) +
            Opcodes.IF +
            this.type +
            this.then.emitBytes(assem, flow) +
            Opcodes.END
        );
        return (
            this.cond.emitBytes(assem, flow) +
            Opcodes.IF +
            this.type +
            this.then.emitBytes(assem, flow) +
            Opcodes.ELSE +
            this.other.emitBytes(assem, flow) +
            Opcodes.END
        );
    }
}

export class ConstU32 implements Instruction {
    constructor(
        public value: Uns32,
    ) {}

    optimize(): Instruction {
        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            Opcodes.U32_CONST +
            Pack.u32(this.value)
        );
    }
}

export class ConstI64 implements Instruction {
    constructor(
        public value: Int64,
    ) {}

    optimize(): Instruction { return this; }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            Opcodes.I64_CONST +
            Pack.i64(this.value)
        );
    }
}

export class ConstF64 implements Instruction {
    constructor(
        public value: Flt64,
    ) {}

    optimize(): Instruction { return this; }

    emitBytes(assem: Assembler, flow: null | FlowControl) { 
        return (
            Opcodes.F64_CONST +
            Pack.f64(this.value)
        );
    }
}

export class ConstV128 implements Instruction {
    #data = new DataView(new ArrayBuffer(16));

    optimize(): Instruction { return this; }

    emitBytes(assem: Assembler, flow: null | FlowControl) { 
        return (
            Opcodes.V128_CONST +
            String.fromCharCode(... new Uint8Array(this.#data.buffer))
        );
    }

    getI8( lane: number): number {
        return this.#data.getInt8(1*lane);
    }

    getI16(lane: number): number {
        return this.#data.getInt16(2*lane, true);
    }

    getI32(lane: number): number {
        return this.#data.getInt32(4*lane, true);
    }

    getI64(lane: number): bigint {
        return this.#data.getBigInt64(8*lane, true);
    }

    getU8( lane: number): number {
        return this.#data.getUint8(1*lane);
    }

    getU16(lane: number): number {
        return this.#data.getUint16(2*lane, true);
    }
       
    getU32(lane: number): number {
        return this.#data.getUint32(4*lane, true);
    }

    getU64(lane: number): bigint {
        return this.#data.getBigUint64(8*lane, true);
    }

    getF32(lane: number): number { 
        return this.#data.getFloat32(4*lane, true);
    }

    getF64(lane: number): number {
        return this.#data.getFloat64(8*lane, true);
    }

    setI8( lane: number, value: number) {
        this.#data.setInt8(1*lane, value);
    }

    setI16(lane: number, value: number) {
        this.#data.setInt16(2*lane, value, true);
    }

    setI32(lane: number, value: number) {
        this.#data.setInt32(4*lane, value, true);
    }

    setI64(lane: number, value: bigint) {
        this.#data.setBigInt64(8*lane, value, true);
    }

    setU8( lane: number, value: number) {
        this.#data.setUint8(1*lane, value);
    }

    setU16(lane: number, value: number) {
        this.#data.setUint16(2*lane, value, true);
    }

    setU32(lane: number, value: number) {
        this.#data.setUint32(4*lane, value, true);
    }

    setU64(lane: number, value: bigint) {
        this.#data.setBigUint64(8*lane, value, true);
    }

    setF32(lane: number, value: number) {
        this.#data.setFloat32(4*lane, value, true);
    }

    setF64(lane: number, value: number) {
        this.#data.setFloat64(8*lane, value, true);
    }
}

export class FunCall implements Instruction {
    args: Instruction[];

    constructor(
        public funIndex: Uns32,
        ... args: Instruction[]
    ) {
        this.args = args;
    }

    optimize(): Instruction {
        this.args = this.args.map(aa => aa.optimize());
        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            this.args.map(
                (arg: Instruction) => arg.emitBytes(assem, flow)
            ).join("") +
            Opcodes.FUNCALL + Pack.u32(this.funIndex)
        );
    }
}

export class Neg_I64 implements Instruction {
    constructor(
        public val: Instruction,
    ) {}

    optimize(): Instruction {
        this.val = this.val.optimize();

        if (this.val instanceof ConstI64) {
            return new ConstI64(wrapInt64(-this.val.value));
        }

        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            Opcodes.I64_CONST + Pack.i64(0n) +
            this.val.emitBytes(assem, flow) +
            Opcodes.I64_SUB
        );
    }
}

export class Neg_F64 implements Instruction {
    constructor(
        public val: Instruction,
    ) {}

    optimize(): Instruction {
        this.val = this.val.optimize();

        if (this.val instanceof ConstF64) {
            return new ConstF64(-this.val.value);
        }

        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            this.val.emitBytes(assem, flow) +
            Opcodes.F64_NEG
        );
    }
}

export class Neg_F64x2 implements Instruction {
    constructor(
        public val: Instruction,
    ) {}

    optimize(): Instruction {
        this.val = this.val.optimize();

        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            this.val.emitBytes(assem, flow) +
            Opcodes.F64X2_NEG
        );
    }
}

export class Add_I64_I64 implements Instruction {
    constructor(
        public lhs: Instruction,
        public rhs: Instruction,
    ) {}

    optimize(): Instruction {
        this.lhs = this.lhs.optimize();
        this.rhs = this.rhs.optimize();

        if (this.lhs instanceof ConstI64 &&
            this.rhs instanceof ConstI64
        ) {
            return new ConstI64(wrapInt64(this.lhs.value + this.rhs.value));
        }

        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            this.lhs.emitBytes(assem, flow) +
            this.rhs.emitBytes(assem, flow) +
            Opcodes.I64_ADD
        );
    }
}

export class Add_F64_F64 implements Instruction {
    constructor(
        public lhs: Instruction,
        public rhs: Instruction,
    ) {}

    optimize(): Instruction {
        this.lhs = this.lhs.optimize();
        this.rhs = this.rhs.optimize();

        if (this.lhs instanceof ConstF64 &&
            this.rhs instanceof ConstF64
        ) {
            return new ConstF64(this.lhs.value + this.rhs.value);
        }

        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            this.lhs.emitBytes(assem, flow) +
            this.rhs.emitBytes(assem, flow) +
            Opcodes.F64_ADD
        );
    }
}

export class Add_F64_CF64 implements Instruction {
    constructor(
        public lhs: Instruction,
        public rhs: Instruction,
    ) {}

    optimize(): Instruction {
        this.lhs = this.lhs.optimize();
        this.rhs = this.rhs.optimize();

        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        const params = [Wasm.F64, Wasm.V128];
        const results = [Wasm.V128];
        const code = (
            Opcodes.LOCAL_GET     + Pack.u32(1) +
            Opcodes.LOCAL_GET     + Pack.u32(1) +
            Opcodes.F64X2_GETLANE + Pack.u32(0) +
            Opcodes.LOCAL_GET     + Pack.u32(0) +
            Opcodes.F64_ADD +
            Opcodes.F64X2_SETLANE + Pack.u32(0)
        );
        const funIndex = assem.cachedFunc(params, results, [], code);

        return (
            this.lhs.emitBytes(assem, flow) +
            this.rhs.emitBytes(assem, flow) +
            Opcodes.FUNCALL + Pack.u32(funIndex)
        );
    }
}

export class Add_CF64_F64 implements Instruction {
    constructor(
        public lhs: Instruction,
        public rhs: Instruction,
    ) {}

    optimize(): Instruction {
        this.lhs = this.lhs.optimize();
        this.rhs = this.rhs.optimize();

        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        const params = [Wasm.V128, Wasm.F64];
        const results = [Wasm.V128];
        const code = (
            Opcodes.LOCAL_GET     + Pack.u32(0) +
            Opcodes.LOCAL_GET     + Pack.u32(0) +
            Opcodes.F64X2_GETLANE + Pack.u32(0) +
            Opcodes.LOCAL_GET     + Pack.u32(1) +
            Opcodes.F64_ADD +
            Opcodes.F64X2_SETLANE + Pack.u32(0)
        );
        const funIndex = assem.cachedFunc(params, results, [], code);

        return (
            this.lhs.emitBytes(assem, flow) +
            this.rhs.emitBytes(assem, flow) +
            Opcodes.FUNCALL + Pack.u32(funIndex)
        );
    }
}

export class Add_F64x2_F64x2 implements Instruction {
    constructor(
        public lhs: Instruction,
        public rhs: Instruction,
    ) {}

    optimize(): Instruction {
        this.lhs = this.lhs.optimize();
        this.rhs = this.rhs.optimize();

        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            this.lhs.emitBytes(assem, flow) +
            this.rhs.emitBytes(assem, flow) +
            Opcodes.F64X2_ADD
        );
    }
}

export class Sub_I64_I64 implements Instruction {
    constructor(
        public lhs: Instruction,
        public rhs: Instruction,
    ) {}

    optimize(): Instruction {
        this.lhs = this.lhs.optimize();
        this.rhs = this.rhs.optimize();

        if (this.lhs instanceof ConstI64 &&
            this.rhs instanceof ConstI64
        ) {
            return new ConstI64(wrapInt64(this.lhs.value - this.rhs.value));
        }

        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            this.lhs.emitBytes(assem, flow) +
            this.rhs.emitBytes(assem, flow) +
            Opcodes.I64_SUB
        );
    }
}

export class Sub_F64_F64 implements Instruction {
    constructor(
        public lhs: Instruction,
        public rhs: Instruction,
    ) {}

    optimize(): Instruction {
        this.lhs = this.lhs.optimize();
        this.rhs = this.rhs.optimize();

        if (this.lhs instanceof ConstF64 &&
            this.rhs instanceof ConstF64
        ) {
            return new ConstF64(this.lhs.value - this.rhs.value);
        }

        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            this.lhs.emitBytes(assem, flow) +
            this.rhs.emitBytes(assem, flow) +
            Opcodes.F64_SUB
        );
    }
}

export class Sub_F64_CF64 implements Instruction {
    constructor(
        public lhs: Instruction,
        public rhs: Instruction,
    ) {}

    optimize(): Instruction {
        this.lhs = this.lhs.optimize();
        this.rhs = this.rhs.optimize();

        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        const params = [Wasm.F64, Wasm.V128];
        const results= [Wasm.V128];
        const code = (
            Opcodes.LOCAL_GET     + Pack.u32(1) +
            Opcodes.LOCAL_GET     + Pack.u32(1) +
            Opcodes.F64X2_GETLANE + Pack.u32(0) +
            Opcodes.LOCAL_GET     + Pack.u32(0) +
            Opcodes.F64_SUB +
            Opcodes.F64X2_SETLANE + Pack.u32(0) +
            Opcodes.F64X2_NEG
        );
        const funIndex = assem.cachedFunc(params, results, [], code);

        return (
            this.lhs.emitBytes(assem, flow) +
            this.rhs.emitBytes(assem, flow) +
            Opcodes.FUNCALL + Pack.u32(funIndex)
        );
    }
}

export class Sub_CF64_F64 implements Instruction {
    constructor(
        public lhs: Instruction,
        public rhs: Instruction,
    ) {}

    optimize(): Instruction {
        this.lhs = this.lhs.optimize();
        this.rhs = this.rhs.optimize();

        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        const params = [Wasm.V128, Wasm.F64];
        const results = [Wasm.V128];
        const code = (
            Opcodes.LOCAL_GET     + Pack.u32(0) +
            Opcodes.LOCAL_GET     + Pack.u32(0) +
            Opcodes.F64X2_GETLANE + Pack.u32(0) +
            Opcodes.LOCAL_GET     + Pack.u32(1) +
            Opcodes.F64_SUB +
            Opcodes.F64X2_SETLANE + Pack.u32(0)
        );
        const funIndex = assem.cachedFunc(params, results, [], code);

        return (
            this.lhs.emitBytes(assem, flow) +
            this.rhs.emitBytes(assem, flow) +
            Opcodes.FUNCALL + Pack.u32(funIndex)
        );
    }
}

export class Sub_F64x2_F64x2 implements Instruction {
    constructor(
        public lhs: Instruction,
        public rhs: Instruction,
    ) {}

    optimize(): Instruction {
        this.lhs = this.lhs.optimize();
        this.rhs = this.rhs.optimize();

        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            this.lhs.emitBytes(assem, flow) +
            this.rhs.emitBytes(assem, flow) +
            Opcodes.F64X2_SUB
        );
    }
}

export class Mul_I64_I64 implements Instruction {
    constructor(
        public lhs: Instruction,
        public rhs: Instruction,
    ) {}

    optimize(): Instruction {
        this.lhs = this.lhs.optimize();
        this.rhs = this.rhs.optimize();

        if (this.lhs instanceof ConstI64 &&
            this.rhs instanceof ConstI64
        ) {
            return new ConstI64(wrapInt64(this.lhs.value * this.rhs.value));
        }

        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            this.lhs.emitBytes(assem, flow) +
            this.rhs.emitBytes(assem, flow) +
            Opcodes.I64_MUL
        );
    }
}

export class Mul_F64_F64 implements Instruction {
    constructor(
        public lhs: Instruction,
        public rhs: Instruction,
    ) {}

    optimize(): Instruction {
        this.lhs = this.lhs.optimize();
        this.rhs = this.rhs.optimize();

        if (this.lhs instanceof ConstF64 &&
            this.rhs instanceof ConstF64
        ) {
            return new ConstF64(this.lhs.value * this.rhs.value);
        }

        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            this.lhs.emitBytes(assem, flow) +
            this.rhs.emitBytes(assem, flow) +
            Opcodes.F64_MUL
        );
    }
}

export class Mul_F64_CF64 implements Instruction {
    constructor(
        public lhs: Instruction,
        public rhs: Instruction,
    ) {}

    optimize(): Instruction {
        this.lhs = this.lhs.optimize();
        this.rhs = this.rhs.optimize();

        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        const params = [Wasm.F64, Wasm.V128];
        const results = [Wasm.V128];
        const code = (
            Opcodes.LOCAL_GET     + Pack.u32(0) +
            Opcodes.F64X2_SPLAT +
            Opcodes.LOCAL_GET     + Pack.u32(1) +
            Opcodes.F64X2_MUL
        );
        const funIndex = assem.cachedFunc(params, results, [], code);

        return (
            this.lhs.emitBytes(assem, flow) +
            this.rhs.emitBytes(assem, flow) +
            Opcodes.FUNCALL + Pack.u32(funIndex)
        );
    }
}

export class Mul_CF64_F64 implements Instruction {
    constructor(
        public lhs: Instruction,
        public rhs: Instruction,
    ) {}

    optimize(): Instruction {
        this.lhs = this.lhs.optimize();
        this.rhs = this.rhs.optimize();

        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        const params = [Wasm.V128, Wasm.F64];
        const results = [Wasm.V128];
        const code = (
            Opcodes.LOCAL_GET     + Pack.u32(1) +
            Opcodes.F64X2_SPLAT +
            Opcodes.LOCAL_GET     + Pack.u32(0) +
            Opcodes.F64X2_MUL
        );
        const funIndex = assem.cachedFunc(params, results, [], code);

        return (
            this.lhs.emitBytes(assem, flow) +
            this.rhs.emitBytes(assem, flow) +
            Opcodes.FUNCALL + Pack.u32(funIndex)
        );
    }
}

export class Mul_CF64_CF64 implements Instruction {
    constructor(
        public lhs: Instruction,
        public rhs: Instruction,
    ) {}

    optimize(): Instruction {
        this.lhs = this.lhs.optimize();
        this.rhs = this.rhs.optimize();

        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        const params = [Wasm.V128, Wasm.V128];
        const results = [Wasm.V128];
        const code = (
            Opcodes.LOCAL_GET     + Pack.u32(0) +
            Opcodes.F64X2_GETLANE + Pack.u32(0) +
            Opcodes.LOCAL_GET     + Pack.u32(1) +
            Opcodes.F64X2_GETLANE + Pack.u32(0) +
            Opcodes.F64_MUL +

            Opcodes.LOCAL_GET     + Pack.u32(0) +
            Opcodes.F64X2_GETLANE + Pack.u32(1) +
            Opcodes.LOCAL_GET     + Pack.u32(1) +
            Opcodes.F64X2_GETLANE + Pack.u32(1) +
            Opcodes.F64_MUL +

            Opcodes.F64_SUB +
            Opcodes.F64X2_SPLAT +

            Opcodes.LOCAL_GET     + Pack.u32(0) +
            Opcodes.F64X2_GETLANE + Pack.u32(0) +
            Opcodes.LOCAL_GET     + Pack.u32(1) +
            Opcodes.F64X2_GETLANE + Pack.u32(1) +
            Opcodes.F64_MUL +

            Opcodes.LOCAL_GET     + Pack.u32(0) +
            Opcodes.F64X2_GETLANE + Pack.u32(1) +
            Opcodes.LOCAL_GET     + Pack.u32(1) +
            Opcodes.F64X2_GETLANE + Pack.u32(0) +
            Opcodes.F64_MUL +

            Opcodes.F64_ADD +
            Opcodes.F64X2_SETLANE + Pack.u32(1)
        );
        const funIndex = assem.cachedFunc(params, results, [], code);

        return (
            this.lhs.emitBytes(assem, flow) +
            this.rhs.emitBytes(assem, flow) +
            Opcodes.FUNCALL + Pack.u32(funIndex)
        );

        // This vector approach is an alternative way to do the same thing as
        // the scalar approach above, but it has a lot of swizzle and shuffle
        // stuff instead.
        //
        // const f64x2_swap = (
        //     "\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F" +
        //     "\x00\x01\x02\x03\x04\x05\x06\x07"
        // );
        // const f64x2_firsts = (
        //     "\x00\x01\x02\x03\x04\x05\x06\x07" +
        //     "\x10\x11\x12\x13\x14\x15\x16\x17"
        // );
        // const f64x2_seconds = (
        //     "\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F" +
        //     "\x18\x19\x1A\x1B\x1C\x1D\x1E\x1F"
        // );
        // const code = (
        //     Opcodes.LOCAL_GET     + Pack.u32(0) +
        //     Opcodes.LOCAL_GET     + Pack.u32(1) +
        //     Opcodes.F64X2_MUL     +
        //     Opcodes.LOCAL_TEE     + Pack.u32(2) +
        //     Opcodes.LOCAL_GET     + Pack.u32(2) +
        //     Opcodes.F64X2_GETLANE + Pack.u32(1) +
        //     Opcodes.F64_NEG       +
        //     Opcodes.F64X2_SETLANE + Pack.u32(1) +
        //     Opcodes.LOCAL_TEE     + Pack.u32(2) +
        //  
        //     Opcodes.LOCAL_GET     + Pack.u32(0) +
        //     Opcodes.V128_CONST    + f64x2_swap +
        //     Opcodes.V128_SWIZZLE  +
        //     Opcodes.LOCAL_GET     + Pack.u32(1) +
        //     Opcodes.F64X2_MUL     +
        //     Opcodes.LOCAL_TEE     + Pack.u32(3) +
        // 
        //     Opcodes.V128_SHUFFLE  + f64x2_firsts +
        //     Opcodes.LOCAL_GET     + Pack.u32(2) +
        //     Opcodes.LOCAL_GET     + Pack.u32(3) +
        //     Opcodes.V128_SHUFFLE  + f64x2_seconds +
        //     Opcodes.F64X2_ADD
        // );
    }
}

export class Lt_I64_I64 implements Instruction {
    constructor(
        public lhs: Instruction,
        public rhs: Instruction,
    ) {}

    optimize(): Instruction {
        this.lhs = this.lhs.optimize();
        this.rhs = this.rhs.optimize();
        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            this.lhs.emitBytes(assem, flow) +
            this.rhs.emitBytes(assem, flow) +
            Opcodes.I64_LT
        );
    }
}

export class Lt_F64_F64 implements Instruction {
    constructor(
        public lhs: Instruction,
        public rhs: Instruction,
    ) {}

    optimize(): Instruction {
        this.lhs = this.lhs.optimize();
        this.rhs = this.rhs.optimize();
        // TODO: There are many possible optimizations

        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            this.lhs.emitBytes(assem, flow) +
            this.rhs.emitBytes(assem, flow) +
            Opcodes.F64_LT
        );
    }
}

export class GetLaneF64x2 implements Instruction {
    constructor(
        public base: Instruction,
        public lane: Uns32,
    ) {
        assert(lane === 0 || lane === 1);
    }

    optimize(): Instruction {
        this.base = this.base.optimize();
        if (this.base instanceof ConstV128) {
            // XXX: Uncomment these
            //if (this.lane === 0) return this.base.lane0;
            //if (this.lane === 1) return this.base.lane1;
        }
        return this;
    }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            this.base.emitBytes(assem, flow) +
            Opcodes.F64X2_GETLANE +
            Pack.u32(this.lane)
        );
    }

}

export class GlobalGet implements Instruction {
    constructor(
        public index: Uns32,
    ) {}

    optimize(): Instruction { return this; }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            Opcodes.GLOBAL_GET +
            Pack.u32(this.index)
        );
    }
}

export class GlobalSet implements Instruction {
    constructor(
        public index: Uns32,
    ) {}

    optimize(): Instruction { return this; }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            Opcodes.GLOBAL_SET +
            Pack.u32(this.index)
        );
    }
}

export class LocalGet implements Instruction {
    constructor(
        public index: Uns32,
    ) {}

    optimize(): Instruction { return this; }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            Opcodes.LOCAL_GET +
            Pack.u32(this.index)
        );
    }
}

export class LocalSet implements Instruction {
    constructor(
        public index: Uns32,
    ) {}

    optimize(): Instruction { return this; }

    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            Opcodes.LOCAL_SET +
            Pack.u32(this.index)
        );
    }
}

export class LocalTee implements Instruction {
    constructor(
        public index: Uns32,
    ) {}

    optimize(): Instruction { return this; }
    
    emitBytes(assem: Assembler, flow: null | FlowControl) {
        return (
            Opcodes.LOCAL_TEE +
            Pack.u32(this.index)
        );
    }
}

