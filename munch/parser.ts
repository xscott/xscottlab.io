// Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

import {
    failure, panic, assert,
    LiteralF64, LiteralCF64, LiteralI64,
    LiteralStr, Identifier,
    Ternary, AndExpr, OrExpr, CallExpr,
    RefItem, RefLane, RefAttr, NamedArg,
    Keyword, Punctuation,
    FuncStmt, Parameter, ReturnStmt, VarStmt, Variable,
    IfElseStmt, WhileStmt, BreakStmt, ContinueStmt,
    BlockStmt, ExprStmt, AssignStmt, LambdaFunc,
    Constructor, Lexical, Expression, Statement,
} from "./index.ts";

export function parsify(tokens: Lexical[]): BlockStmt {

    class CurlyBlock {
        constructor(
            public row: number,
            public col: number,
            public list: Statement[],
        ) {}
    }

    const result = stmtlist();
    if (tokens.length > 0) failure(
        tokens[0].row, tokens[0].col, "unexpected parse token"
    );
    return new BlockStmt(-1, 0, result);

    function stmtlist(): Statement[] {
        const list = [];
        for (;;) {
            while (match(";")); // skip semis
            const stmt = statement();
            if (stmt === null) break;
            list.push(stmt);
        }
        return list;
    }

    function curlyblock(): null | CurlyBlock {
        const curly = match("{");
        if (curly === null) return null;
        const list = stmtlist();
        if (match("}") === null) failure(
            curly.row, curly.col, "expected closing curly"
        );
        return new CurlyBlock(curly.row, curly.col, list);
    }

    function statement(): null | Statement {
        if (tokens.length === 0) return null;
        if (tokens[0] instanceof Keyword) {
            const kwd = tokens.shift() as Keyword;
            if (kwd.text === "func")     return funcstmt(kwd);
            if (kwd.text === "var")      return varstmt(kwd);
            if (kwd.text === "if")       return ifthenstmt(kwd);
            if (kwd.text === "while")    return whilestmt(kwd);
            if (kwd.text === "return")   return returnstmt(kwd);
            if (kwd.text === "break")    return breakstmt(kwd);
            if (kwd.text === "continue") return continuestmt(kwd);
            panic("unexpected keyword:" + kwd.text);
        }
        return blockstmt() ?? assignment();
    }

    function funcstmt(kwd: Keyword): null | FuncStmt {
        const name = match(
            "==", "!=", "<", "<=", ">", ">=", "+", "-",
            "*", "/", "%", "\\", "@", "^", "!", "'", "!", "~",
        ) ?? ident();
        if (name === null) failure(
            kwd.row, kwd.col, "expected name or operator"
        );
        const params = nested<Parameter>("(", parameter, ")");
        if (params === null) failure(
            name.row, name.col, "expected parameter list"
        );
        let results: null | Expression[] = null;
        if (match(":") !== null) {
            results = commas<Expression>(expression);
        }
        const block = curlyblock();
        if (block === null) failure(
            name.row, name.col, "expected function body"
        );
        return new FuncStmt(
            kwd.row, kwd.col, name.text, params, results, block.list
        );
    }

    function parameter(): null | Parameter {
        const name = ident();
        if (name === null) return null;
        const colon = match(":");
        if (colon === null) failure(
            name.row, name.col, "expected colon after param name"
        );
        const type = expression();
        if (type === null) failure(
            colon.row, colon.col, "expected type after colon"
        );
        const eq = match("=");
        if (eq !== null) {
            const expr = expression();
            if (expr === null) failure(eq.row, eq.col,
                "expected expression for optional parameter"
            );
        }
        return new Parameter(name.row, name.col, name.text, type, null);
    }

    function varstmt(kwd: Keyword): null | VarStmt {
        const lhs = commas(variable);
        const eq = match("=");
        if (!eq) failure(
            kwd.row, kwd.col, "expected assignment in variable declaration"
        );
        const rhs = commas(expression);
        const semi = match(";");
        if (semi === null) failure(eq.row, eq.col, "expected semicolon");
        lhs.forEach(vv => {
            vv.visibleRow = semi.row;
            vv.visibleCol = semi.col;
        });
        return new VarStmt(kwd.row, kwd.col, lhs, rhs);
    }

    function variable(): null | Variable {
        const name = ident();
        if (name === null) return null;
        let type = null;
        const colon = match(":");
        if (colon !== null) {
            type = expression();
            if (type === null) failure(colon.row, colon.col,
                "expected type after colon for variable"
            );
        }
        return new Variable(name.row, name.col, name.text, type);
    }

    function ifthenstmt(kwd: Keyword): IfElseStmt {
        const cond = expression();
        if (!cond) failure(kwd.row, kwd.col, "expected expression");
        const thenStmts = curlyblock();
        if (!thenStmts) failure(cond.row, cond.col, "expected curly block");
        const elseStmts = elifelse();
        return new IfElseStmt(
            kwd.row, kwd.col, cond, thenStmts.list, elseStmts
        );
    }

    function elifelse(): Statement[] {
        if (tokens.length === 0) return [];
        if (tokens[0] instanceof Keyword && tokens[0].text === "elif") {
            const elif = tokens.shift();
            assert(elif instanceof Keyword);
            const cond = expression();
            if (cond === null) failure(
                elif.row, elif.col, "expected expression"
            );
            const thenStmts = curlyblock();
            if (thenStmts === null) failure(
                cond.row, cond.col, "expected curly block"
            );
            const elseStmts = elifelse();
            return [
                new IfElseStmt(
                    elif.row, elif.col, cond, thenStmts.list, elseStmts
                )
            ];
        }

        if (tokens[0] instanceof Keyword && tokens[0].text === "else") {
            const kwd = tokens.shift();
            assert(kwd instanceof Keyword);
            const elseStmts = curlyblock();
            if (!elseStmts) failure(kwd.row, kwd.col, "expected curly block");
            return elseStmts.list;
        }

        return [];
    }

    function whilestmt(kwd: Keyword): WhileStmt {
        let name = null;
        const colon = match(":");
        if (colon !== null) {
            const id = ident();
            if (id === null) failure(kwd.row, kwd.col,
                "expected identifier after colon for while loop"
            );
            name = id.text;
        }
        const expr = expression();
        if (expr === null) failure(
            kwd.row, kwd.col, "expected expression after while"
        );
        const block = curlyblock();
        if (block === null) failure(
            kwd.row, kwd.col, "expeted curly block for while stmt"
        );
        return new WhileStmt(kwd.row, kwd.col, name, expr, block.list);
    }

    function returnstmt(kwd: Keyword): ReturnStmt {
        const list = commas(expression);
        if (match(";") === null) failure(
            kwd.row, kwd.col, "expected semicolon after return"
        );
        return new ReturnStmt(kwd.row, kwd.col, list);
    }

    function breakstmt(kwd: Keyword): BreakStmt {
        const id = ident();
        const name = id !== null ? id.text : null;
        if (match(";") === null) failure(
            kwd.row, kwd.col, "expected semicolon after break"
        );
        return new BreakStmt(kwd.row, kwd.col, name);
    }

    function continuestmt(kwd: Keyword): ContinueStmt {
        const id = ident();
        const name = id !== null ? id.text : null;
        if (match(";") === null) failure(
            kwd.row, kwd.col, "expected semicolon after continue"
        );
        return new ContinueStmt(kwd.row, kwd.col, name);
    }

    function blockstmt(): null | BlockStmt {
        const block = curlyblock();
        if (block === null) return null;
        return new BlockStmt(block.row, block.col, block.list);
    }

    function assignment(): null | Statement {
        const lhs = commas<Expression>(expression);
        if (lhs.length === 0) return null;
        const eq = match("=");
        if (eq === null) {
            const expr = lhs[0];
            // It's an expression, not assignment.
            if (lhs.length > 1) {
                const next = lhs[1];
                failure(next.row, next.col, "expected single expression");
            }
            if (match(";") === null) failure(
                expr.row, expr.col, "expected semicolon after expression"
            );
            return new ExprStmt(expr.row, expr.col, expr);
        }
        const rhs = commas(expression);
        if (rhs.length === 0) failure(
            eq.row, eq.col, "expected right hand side for assignment"
        );
        const last = rhs[rhs.length - 1];
        if (match(";") === null) failure(
            last.row, last.col, "expected semicolon after assignment"
        );
        return new AssignStmt(eq.row, eq.col, lhs, rhs);
    }

    function expression(): null | Expression {
        if (tokens.length >= 3 && (
            tokens[0] instanceof Punctuation && tokens[0].text === "(" &&
            ( // matches "() =>"
                tokens[1] instanceof Punctuation && tokens[1].text === ")" &&
                tokens[2] instanceof Punctuation && tokens[2].text === "=>"
            ) ||
            ( // matches "():"
                tokens[1] instanceof Punctuation && tokens[1].text === ")" &&
                tokens[2] instanceof Punctuation && tokens[2].text === ":"
            ) ||
            ( // matches "(param:"
                tokens[1] instanceof Identifier &&
                tokens[2] instanceof Punctuation && tokens[2].text === ":"
            )
        )) {
            return lambda(tokens[0].row, tokens[0].col);
        }

        return ternary();
    }

    function lambda(row: number, col: number): Expression {
        const params = nested<Parameter>("(", parameter, ")");
        if (params === null) failure(
            row, col, "expected parameter list"
        );
        let results: null | Expression[] = null;
        if (match(":") !== null) {
            results = commas<Expression>(expression);
        }
        const arrow = match("=>");
        if (arrow === null) failure(row, col, "expected lambda arrow");
        const block = curlyblock();
        if (block !== null) return new LambdaFunc(
            arrow.row, arrow.col, params, results, block.list
        );
        const expr = expression();
        if (expr === null) failure(
            arrow.row, arrow.col, "expected lambda block or expression"
        );
        const list = [new ReturnStmt(expr.row, expr.col, [expr])];
        return new LambdaFunc(arrow.row, arrow.col, params, results, list);
    }

    function ternary(): null | Expression {
        const cc = binor();
        if (!cc) return null;
        const qq = match("?");
        if (!qq) return cc;
        const tt = expression();
        if (!tt) failure(
            qq.row, qq.col, "expected then expression for ternary"
        );
        if (!match(":")) failure(
            qq.row, qq.col, "expected colon for ternary"
        );
        const ee = expression()
        if (!ee) failure(
            qq.row, qq.col, "expected else expression for ternary"
        );
        return new Ternary(qq.row, qq.col, cc, tt, ee);
    }

    function binor(): null | Expression {   
        return lassoc(binand, OrExpr, "|");
    }

    function binand(): null | Expression {
        return lassoc(bincmp, AndExpr, "&");
    }

    function bincmp(): null | Expression {
        return rassoc(binadd, binadd, CallExpr, // non-associative
            "===", "!==", "==", "!=", "<=", "<", ">=", ">"
        );
    }

    function binadd(): null | Expression {
        return lassoc(binmul, CallExpr, "+", "-");
    }

    function binmul(): null | Expression {
        return lassoc(prefix, CallExpr, "*", "/", "%", "\\", "@");
    }

    function prefix(): null | Expression {
        const op = match("+", "-", "!", "~");
        if (!op) return binpow();
        const rhs = prefix();
        if (!rhs) failure(
            op.row, op.col, "expected rhs for " + op.text
        );
        return new CallExpr(op.row, op.col, op.text, [rhs]);
    }

    function binpow(): null | Expression {
        return rassoc(suffix, prefix, CallExpr, "^");
    }

    function suffix(): null | Expression {
        let base = null;
        if (tokens.length >= 2 &&
            tokens[0] instanceof Identifier &&
            tokens[1] instanceof Punctuation && tokens[1].text === "("
        ) {
            // This is the special case where we know that the function
            // is named by an identifier.  If we store as a string for
            // the CallExpr, it simplifies the code for emitExpr().
            const name = tokens.shift() as Identifier;
            const args = nested("(", argument, ")");
            if (args === null) failure(name.row, name.col,
                "expected arguments for named function call"
            );
            base = new CallExpr(name.row, name.col, name.text, args);
        } else {
            base = primary();
        }

        if (base === null) return null;
        for (;;) {
            const tick = match("'");
            if (tick) {
                base = new CallExpr(tick.row, tick.col, "'", [base]);
                continue;
            }

            const dot = match(".");
            if (dot) {
                const attr = ident();
                if (attr) {
                    base = new RefAttr(dot.row, dot.col, base, attr.text);
                    continue;
                }

                const lane = integer();
                if (lane) {
                    base = new RefLane(dot.row, dot.col, base, lane.value);
                    continue;
                }

                failure(dot.row, dot.col, "expected attr or lane after dot");
            }

            const items = nested("[", subscript, "]");
            if (items) {
                base = new RefItem(base.row, base.col, base, items);
                continue;
            }

            const args = nested("(", argument, ")");
            if (args) {
                base = new CallExpr(base.row, base.col, base, args);
                continue;
            }

            return base;
        }
    }

    function subscript(): null | Expression {
        return expression();
    }

    function argument(): null | Expression | NamedArg {
        if (tokens.length >= 2 &&
            tokens[0] instanceof Identifier &&
            tokens[1] instanceof Punctuation && tokens[1].text === "="
        ) {
            const name = tokens.shift();
            assert(name instanceof Identifier);
            const eqop = tokens.shift();
            assert(eqop instanceof Punctuation);
            const expr = expression();
            if (expr === null) failure(eqop.row, eqop.col,
                "expected expression for keyword argument"
            );
            return new NamedArg(name.row, name.col, name.text, expr);
        }
        return expression();
    }

    function primary(): null | Expression {
        if (tokens.length >= 1) {
            const next = tokens[0];
            if (next instanceof LiteralI64)  { tokens.shift(); return next; }
            if (next instanceof LiteralF64)  { tokens.shift(); return next; }
            if (next instanceof LiteralCF64) { tokens.shift(); return next; }
            if (next instanceof LiteralStr)  { tokens.shift(); return next; }
            if (next instanceof Identifier)  { tokens.shift(); return next; }
        }
        const pp = match("(");
        if (pp) {
            const ee = expression();
            if (ee === null) {
                failure(pp.row, pp.col, "expected expression");
            } else if (match(")") === null) {
                failure(ee.row, ee.col, "expected closing paren");
            }
            return ee;
        }
        return null;
    }

    function nested<Type>(
        open: string,
        item: () => null | Type,
        close: string
    ): null | Type[] {
        const cc = match(open);
        if (!cc) return null;
        const list = commas<Type>(item);
        if (!match(close)) failure(
            cc.row, cc.col, `expected comma or matching ${close}`
        );
        return list;
    }

    function commas<Type>(item: () => null | Type): Type[] {
        const list = [];
        for (;;) {
            const ii = item();
            if (ii === null) break;
            list.push(ii);
            if (match(",") === null) break;
        }
        return list;
    }

    function lassoc(
        next: () => null | Expression,
        ctor: Constructor<Expression>,
        ...ops: string[]
    ): null | Expression {
        let lhs = next();
        if (!lhs) return null;
        for (;;) {
            const op = match(...ops);
            if (op === null) return lhs;
            const rhs = next();
            if (rhs === null) failure(
                op.row, op.col, "expected rhs for " + op.text
            );
            lhs = new ctor(op.row, op.col, op.text, [lhs, rhs]);
        }
    }

    function rassoc(
        left:  () => null | Expression,
        right: () => null | Expression,
        ctor:  Constructor<Expression>,
        ...ops: string[]
    ): null | Expression {
        const lhs = left();
        if (!lhs) return null;
        const op = match(...ops);
        if (!op) return lhs;
        const rhs = right();
        if (!rhs) failure(op.row, op.col, "expected rhs for " + op.text);
        return new ctor(op.row, op.col, op.text, [lhs, rhs]);
    }

    function ident(): null | Identifier{
        if (tokens.length >= 1) {
            const next = tokens[0];
            if (next instanceof Identifier) {
                tokens.shift();
                return next;
            }
        }
        return null;
    }

    function integer(): null | LiteralI64 {
        if (tokens.length >= 1) {
            const next = tokens[0];
            if (next instanceof LiteralI64) {
                tokens.shift();
                return next;
            }
        }
        return null;
    }

    function match(...patterns: string[]): null | Lexical {
        if (tokens.length >= 1) {
            const next = tokens[0];
            if (next instanceof Punctuation) {
                if (patterns.includes(next.text)) {
                    tokens.shift();
                    return next;
                }
            }
        }
        return null;
    }
}

