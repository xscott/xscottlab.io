// Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

import {
    compile,
} from "./index.ts";


const source = String.raw`

    var f = () => { return 123; };

`;

try {
    const exports = compile(source);
    console.dir(exports.mem);
    exports.run();
} catch (error) {
    console.log("ERROR:", error);
}

/*
    var u = 1.0 + 2.0j;
    var v = 3.0 + 4.0j;
    var w = u * v;
    
    log(w.real);
    log(w.imag);

*/

/*
    var u = 1.0 + 1.0j;
    var v = 2.0 + 3.0j;
    var z = 1.0 - v;

    log(z.real);
    log(z.imag);

    log(0.0 - 2.0);

*/

/*
    #log(ii); # SHOULD NOT BE VISIBLE

    var ii, jj = 0.0, 1.0;
    while ii < 1000.0 {
        log(ii);
        ii, jj = jj, ii + jj;
    }

    #var ii = 123;
    #ii = 456;
    #ii = ii + 2;
    #log(ii);


*/

/*
    log(123);

    while true {
        log(456);

        if rand() < 0.5 {
            continue;
        }

        break;
    }

    log(789);

*/

/*
    log(123);

    if false & false & false & false | true {
        log(456);
    }

    log(789);

    log(2 + 3);
    log(2.0 + 3.0);

    while:outer true {
        log(1);
        if true {
            if rand() < 0.5 {
            #if true {
                log(1.5);
                break outer;
            } else {
                log(2.5);
                break outer;
            }
        }
        log(2);
    }

    log(3);

*/

/*
    while:outer true {
        while:inner true {
            log(rand());
            if rand() < 0.05 {
                break outer;
            }
        }
    }


*/


/*
import {
    compile,
    debug, tokenize, parsify,

} from "./index.ts";

const lines = source.split('\n');
const tokens = tokenize(lines);
const parse = parsify(tokens);

debug(parse);
*/

/*
    log(0);
    while:outer rand() < 0.5 {
        log(1);
        while:inner rand() < 0.5 {
            log(2);
            #if rand() < 0.125 { log(3); break outer; }
            #if rand() < 0.25 { log(4); break inner; }
        }
        log(5);
    }
    log(6);

    #var a:i32, b = 123, 4.5;

    #func foo(a:i32, b:f64=1.0) {
    #}

    #log(123);

*/

/*
import { writeFileSync } from "node:fs"

import { Bytes } from "./bytes.mjs";
import { Wasm, Opcodes } from "./opcodes.mjs";
import { Assembler } from "./assem.mjs";


const assembler = new Assembler();

const log = assembler.newImport("log", [Wasm.I64], []);

const bar = assembler.newLambda([Wasm.I64], []);
bar.setLocals([Wasm.V128, Wasm.I64, Wasm.F64]);
bar.setCode(
    Opcodes.LOCAL_GET + Bytes.u32(0) +
    Opcodes.funcall(log.funIndex)
);
const sig = assembler.signature([Wasm.I64], []);

const hmm = assembler.newGlobal(Wasm.I64, 222n);

const mem = assembler.newMemory(Bytes.f64(0.00));
console.log("mem:", mem);
const mew = assembler.newMemory(1024*64);
console.log("mew:", mew);

const foo = assembler.newExport("foo", [Wasm.I64], [Wasm.I64]);
foo.setCode(
    Opcodes.GLOBAL_GET + Bytes.u32(hmm) +
    //Opcodes.I64_CONST + Bytes.i64(456n) +
    Opcodes.U32_CONST + Bytes.u32(bar.ptrIndex) +
    Opcodes.ptrcall(sig) +

    //Opcodes.I64_CONST + Bytes.i64(123n) +
    //Opcodes.funcall(log.funIndex) +

    Opcodes.LOCAL_GET + Bytes.u32(0) +
    Opcodes.I64_CONST + Bytes.i64(2n) +
    Opcodes.I64_MUL
);

const binary = assembler.assemble();
writeFileSync("binary.wasm", binary);

const exports = (await WebAssembly.instantiate(
    binary, { env: { log: console.log } }
)).instance.exports;

console.log(exports.foo(13n));
*/

