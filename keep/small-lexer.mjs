export { Lexer }

import { Bytes } from "./bytes.mjs";
import { error } from "./error.mjs";

class Lexer {
    constructor(text) {
        this.text = text;
        this.spot = 0;
        this.line = 1;
        this.col = 1;
    }

    close() {
        this.white();
        if (this.spot != this.text.length) {
            throw {
                line: this.line, col: this.col,
                error: "unexpected characters"
            };
        }
    }

    where() { return { line: this.line, col: this.col }; }

    white() {
        for (;;) {
            let chr = this.text[this.spot];
            if (chr === "#") {
                while (chr !== undefined && chr !== "\n") {
                    ++this.spot;
                    chr = this.text[this.spot];
                }
                continue;
            }
            if (chr === " " || chr === "\t" || chr == "\r") {
                ++this.spot;
                ++this.col;
                continue;
            }
            if (chr === "\n") {
                ++this.spot;
                ++this.line;
                this.col = 1;
                continue;
            }
            break;
        }
    }

    match(delim) {
        this.white();

        if (this.text[this.spot] == delim) {
            let where = this.where();
            this.spot += 1;
            this.col += 1;
            return where;
        }
        
        return null;
    }

    basic() {
        this.white();
        if (this.text[this.spot] === '"') return this.#string();

        // XXX: report 64 bit integer overflow issues

        const bin = regex(this, /[-+]?0b[01_]+/y);
        if (bin) {
            let val = BigInt(bin.txt.replaceAll('_', ''))
            return new LexerInteger(bin, val);
        }

        const hex = regex(this, /[-+]?0x[0-9a-fA-F_]+/y);
        if (hex) {
            let val = BigInt(hex.txt.replaceAll('_', ''))
            return new LexerInteger(hex, val);
        }

        const flt = regex(this,
            /[-+]?[0-9][0-9_]*\.[0-9_]+(?:[eE][-+]?[0-9_]+)?j?/y
        );
        if (flt) {
            let val = parseFloat(flt.txt.replaceAll('_', ''));
            if (flt.txt[flt.txt.length - 1] === 'j') {
                return new LexerImaginary(flt, val);
            } else {
                return new LexerFloating(flt, val);
            }
        }

        const dec = regex(this, /[-+]?[0-9][0-9_]*/y);
        if (dec) {
            let val = BigInt(dec.txt.replaceAll('_', ''));
            return new LexerInteger(dec, val);
        }

        const sym = regex(this,
            /[-+*/%\\@^?!a-zA-Z_][-+*/%\\@^?!a-zA-Z0-9_]*/y
        );
        if (sym) return new LexerSymbol(sym);

        return null;

        function regex(that, pat) {
            console.assert(pat.sticky, "regex pattern must have /y");
            pat.lastIndex = that.spot;
            const match = pat.exec(that.text);
            if (!match) return null;
            const txt = match[0];
            const result = { line:that.line, col:that.col, txt };
            that.col += pat.lastIndex - that.spot;
            that.spot = pat.lastIndex;
            const after = that.text[that.spot];
            if ("a" <= after && after <= "z" ||
                "A" <= after && after <= "Z"
            ) error(result, "unexpected characters");
            return result;
        }
    }

    #string() {
        const BACK_SLASH = 0x5C, DOUBLE_QUOTE = 0x22, NEWLINE = 0x0A;
        let buf = new Bytes();
        let ii = 1;
        for (;;) {
            const chr = this.text.codePointAt(this.spot + ii);
            if (chr == NEWLINE) error(
                { line: this.line, col: this.col + ii + 1 },
                "unexpected newline in string literal"
            );
            if (chr == DOUBLE_QUOTE) break;
            if (chr === BACK_SLASH) {
                const esc = this.text[this.spot + ii + 1];
                if (esc === 'n') { buf.push_string("\n"); ii += 2; continue; }
                if (esc === 't') { buf.push_string("\t"); ii += 2; continue; }
                if (esc === '0') { buf.push_string("\0"); ii += 2; continue; }
                if (esc === '\\') { buf.push_string("\\"); ii += 2; continue; }
                if (esc === 'x') {
                    // XXX: Need to implement hexnum somewhere
                    const val = hexnum(this.text, this.spot + ii + 2, 2);
                    buf.push_byte(val);
                    ii += 4;
                    continue;
                }
                if (esc === 'u') {
                    const val = hexnum(this.text, this.spot + ii + 2, 4);
                    buf.push_utf8(val);
                    ii += 4;
                    continue;
                }
                if (esc === 'U') {
                    const val = hexnum(this.text, this.spot + ii + 2, 6);
                    buf.push_utf8(val);
                    ii += 8;
                    continue;
                }
                error(
                    { line: this.line, col: this.col + ii + 1 },
                    "unexpected character as escape sequence"
                );
            }
            buf.push_utf8(chr);
            ii += chr > 0xFFFF ? 2 : 1;
        }
        const result = {
            line: this.line, col: this.col, buf: buf.grab_bytes(),
            str: this.text.slice(this.spot + 1, this.spot + ii),
        };
        this.spot += ii + 1;
        this.col += ii + 1;
        return new LexerString(result);
    }
}

class LexerSymbol {
    constructor(match) {
        Object.assign(this, match);
    }
}

class LexerInteger {
    constructor(match, val) {
        Object.assign(this, match);
        this.val = val;
    }
}

class LexerFloating {
    constructor(match, val) {
        Object.assign(this, match);
        this.val = val;
    }
}

class LexerImaginary {
    constructor(match, val) {
        Object.assign(this, match);
        this.val = val;
    }
}

class LexerString {
    constructor(match) {
        Object.assign(this, match);
    }
}

