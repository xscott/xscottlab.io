

%token LPAREN RPAREN EQUAL COLON NAME OPER INT FLT STR

%%

group
    : // zero or more
    | group equal
    ;

equal
    : colon EQUAL equal
    | colon
    ;

colon
    : basic COLON colon
    | basic
    ;

basic
    : LPAREN OPER group RPAREN
    | LPAREN group RPAREN
    | NAME
    | INT
    | FLT
    | STR
    ;

%%

