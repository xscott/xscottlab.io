
export { parse };

import { Lexer } from "./lexer.mjs";
import { error } from "./error.mjs";

function parse(text) {
    let lexer = new Lexer(text);
    const result = tuple();
    lexer.close();
    return result;

    function tuple() {
        const where = lexer.where();
        let list = [];
        for (;;) {
            const ee = equal();
            if (ee === null) break;
            list.push(ee);
        }
        return new ParseTuple(where, list);
    }

    function equal() { return rassoc("=", ParseEqual, colon, equal); }
    function colon() { return rassoc(":", ParseColon, suffix, colon); }

    function rassoc(tok, type, left, right) {
        const lhs = left();
        if (lhs === null) return null;
        const where = lexer.match(tok);
        if (where) {
            const rhs = right();
            if (rhs === null) error(
                where, "expected expression after " + tok
            );
            return new type(where, lhs, rhs);
        }
        return lhs;
    }

    function suffix() {
        let base = basic();
        if (base === null) return null;
        for (;;) {
            const brace = lexer.match("[");
            if (brace !== null) {
                let list = items();
                base = new ParseItem(brace, base, list);
                if (lexer.match("]") === null) {
                    error(brace, "unmatched brace");
                }
                continue;
            }

            const dot = lexer.match(".");
            if (dot !== null) {
                const field = lexer.basic();
                if (field) {
                    if (field.constructor.name === 'LexerSymbol') {
                        base = new ParseAttr(dot, base, field);
                        continue;
                    }
                    if (field.constructor.name === 'LexerInteger') {
                        base = new ParseLane(dot, base, field);
                        continue;
                    }
                }
                error(dot, "expected attr or lane after dot");
            }
            break;
        }
        return base;
    }

    function items() {
        let list = [];
        for (;;) {
            const bb = basic();
            if (bb) list.push(bb);
            const cc = lexer.match(",");
            if (cc === null) break;
        }
        return list;
    }

    function basic() {
        const where = lexer.match("(");
        if (where !== null) {
            const tt = tuple();
            if (lexer.match(")") === null) {
                error(where, "unmatched paren");
            }
            return tt;
        }
        return lexer.basic();
    }
}

class ParseTuple {
    constructor(where, list) {
        Object.assign(this, where);
        Object.assign(this, list);
        this.length = list.length;
    }
}

class ParseEqual {
    constructor(where, lhs, rhs) {
        Object.assign(this, where);
        Object.assign(this, { lhs, rhs });
    }
}

class ParseColon {
    constructor(where, lhs, rhs) {
        Object.assign(this, where);
        Object.assign(this, { lhs, rhs });
    }
}

class ParseItem {
    constructor(where, base, list) {
        Object.assign(this, where);
        this.base = base;
        Object.assign(this, list);
        this.length = list.length;
    }
}

class ParseAttr {
    constructor(where, base, lane) {
        Object.assign(this, where);
        Object.assign(this, { base, attr });
    }
}

class ParseLane {
    constructor(where, base, lane) {
        Object.assign(this, where);
        Object.assign(this, { base, lane });
    }
}


