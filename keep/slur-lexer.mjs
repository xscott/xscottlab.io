// Copyright 2022-2024 Scott Eric Gilbert.  All Rights Reserved.

import { Bytes } from "./bytes.mjs";
import { error } from "./basics.mjs";

export function tokenize(lines) {
    const spaces = /[ \t\r]*/y;
    const floating = new RegExp(
        "[-+]?"                    + // optional sign
        "[0-9]"                    + // required leading digit
        "[0-9_]*"                  + // digits or underscores
        "[.]"                      + // required decimal point
        "[0-9]"                    + // required decimal digit
        "[0-9_]*"                  + // digits or underscores
        "(?:[eE][-+]?[0-9_]+)?"    + // optional exponent
        "(?![a-zA-Z.])"            , // no alpha or dot
        "y"
    );
    const integer = new RegExp(
        "[-+]?"                    + // optional sign
        "(?:"                      + // pick one of {
            "(0b[01_]+)|"          + //     binary
            "(0x[0-9a-fA-F_]+)|"   + //     hexadecimal
            "([0-9][0-9_]*)"       + //     decimal
        ")"                        + // }
        "(?![a-zA-Z.])"            , // no alpha or dot
        "y"
    );
    const operator = new RegExp(
        "[-+*/%@|<>^=!]+"          + // one or more ops
        "(?![a-zA-Z.0-9])"         , // no alpha, dot, or digit
        "y"
    );
    const identifier = /[a-zA-Z_][a-zA-Z_0-9]*/y;
    const delimiter = /[[\]().:,]/y;

    const table = [
        [floating,   LiteralF64],
        [integer,    LiteralI64],
        [operator,   Identifier],
        [identifier, Identifier],
        [delimiter,  Delimiter],
    ];

    let row = 0, col = 0;
    const tokens = [];

    loop: for (;;) {
        const line = lines[row];
        if (line === undefined) break;

        spaces.lastIndex = col;
        spaces.exec(line);
        col = spaces.lastIndex;
        const chr = line[col];
        if (chr === undefined || chr === "#") {
            ++row;
            col = 0;
            continue;
        }
        if (chr === '"') {
            const [str, end] = string(line, row, col);
            col = end;
            tokens.push(str);
            continue;
        }

        for (let [regex, ctor] of table) {
            regex.lastIndex = col;
            const match = regex.exec(line);
            if (match) {
                tokens.push(new ctor(row, col, match[0]));
                col = regex.lastIndex;
                continue loop;
            }
        }

        error(row, col, "lexical error");
    }

    return tokens;

    function string(line, row, col) {
        // Any web pages I care about will be encoded in UTF-8, but when we ask
        // for the source to the script we're compiling it will be given to us
        // as a JavaScript UTF-16 string.  The tokenizer for most program
        // tokens will only accept ASCII for delimiters, identifiers, and
        // operators, but I'm going to allow full unicode in comments and
        // strings.
        //
        // However, Crunch strings are not strictly UTF-8.  Much like the Go
        // programming language, Crunch allows any 8-bit binary value in them.
        // So \xHH escape sequences will insert any byte, \uHHHH and \UHHHHHH
        // will insert the corresponding UTF-8 bytes, and UTF-16 Code Points
        // (possibly as a surrogate pair) from the source code will be
        // converted to UTF-8 sequences of bytes.
        //
        // So looking at the whole picture, we've got a UTF-8 web page
        // converted to a UTF-16 string, scanned/parsed into a sequence of
        // bytes, and stored in a UTF-16 string that only uses values/codes in
        // the range of 0 to 255.  See the Bytes class for the motivation
        // on using JavaScript String to store 8 bit bytes.

        const BACK_SLASH = 0x5C, DOUBLE_QUOTE = 0x22, NEWLINE = 0x0A;
        let bytes = "";
        let ptr = col + 1;
        for (;;) {
            // Note: strings and comments are allowed to have unicode,
            // but the rest of the source code is expected to be ascii.
            const ord = line.codePointAt(ptr);
            if (ord == DOUBLE_QUOTE) { break; }
            if (ord == NEWLINE) error(row, ptr,
                "unexpected newline in string literal"
            );
            if (ord === BACK_SLASH) {
                switch (line[ptr + 1]) {
                    case  'n': { bytes += "\n"; ptr += 2; continue; }
                    case  't': { bytes += "\t"; ptr += 2; continue; }
                    case  '0': { bytes += "\0"; ptr += 2; continue; }
                    case  '"': { bytes += "\""; ptr += 2; continue; }
                    case '\\': { bytes += "\\"; ptr += 2; continue; }
                    case  'x': {
                        const value = hexnum(line, row, ptr + 2, 2);
                        bytes += String.fromCodePoint(value);
                        ptr += 4;
                        continue;
                    }
                    case  'u': {
                        const value = hexnum(line, row, ptr + 2, 4);
                        bytes += Bytes.utf8(value);
                        ptr += 6;
                        continue;
                    }
                    case  'U': {
                        const value = hexnum(line, row, ptr + 2, 6);
                        bytes += Bytes.utf8(value);
                        ptr += 8;
                        continue;
                    }
                    default: error(
                        row, ptr, "unexpected character as escape sequence"
                    );
                }
            }
            // convert the ordinal to UTF-8 bytes, and increment
            // by 2 if the source code had a surrogates pair
            bytes += Bytes.utf8(ord);
            ptr += ord > 0xFFFF ? 2 : 1;
        }
        let text = line.slice(col + 1, ptr);
        return [new LiteralStr(row, col, text, bytes), ptr + 1];
    }

    function hexnum(line, row, col, len) {
        let hex = line.slice(col, col + len);
        for (let ii = 0; ii<len; ++ii) {
            if (!/[0-9a-fA-F]/.test(hex[ii])) {
                error(row, col + ii, "expected hex digit");
            }
        }
        return parseInt(hex, 16);
    }
}

export class LiteralF64 {
    constructor(row, col, text) {
        this.row = row;
        this.col = col;
        this.text = text;
        this.value = parseFloat(text.replaceAll('_', ''));
    }
}

export class LiteralI64 {
    constructor(row, col, text) {
        // XXX: range checks, convert to I64
        let value = BigInt(text.replaceAll('_', ''));
        this.row = row;
        this.col = col;
        this.text = text;
        this.value = value;
    }
}

export class LiteralStr {
    constructor(row, col, text, bytes) {
        this.row = row;
        this.col = col;
        this.text = text;
        this.bytes = bytes;
    }
}

export class Identifier {
    constructor(row, col, text) {
        this.row = row;
        this.col = col;
        this.text = text;
    }
}

export class Delimiter {
    constructor(row, col, text) {
        this.row = row;
        this.col = col;
        this.text = text;
    }
}

