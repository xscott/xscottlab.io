"use strict";

class Complex {
    constructor(real=0.0, imag=0.0) {
        this.real = Number(real);
        this.imag = Number(imag);
    }

    static cis(angle) {
        return new Complex(Math.cos(angle), Math.sin(angle));
    }

    add(that) {
        if (that instanceof Complex) {
            return new Complex(
                this.real + that.real,
                this.imag + that.imag
            );
        }
        if (typeof(that) === "number") {
            return new Complex(this.real + that, this.imag);
        }
        throw "Can't add Complex to: " + that;
    }

    sub(that) {
        if (that instanceof Complex) {
            return new Complex(
                this.real - that.real,
                this.imag - that.imag
            );
        }
        if (typeof(that) === "number") {
            return new Complex(this.real - that, this.imag);
        }
        throw "Can't sub Complex to: " + that;
    }

    mul(that) {
        if (that instanceof Complex) {
            return new Complex(
                this.real*that.real - this.imag*that.imag,
                this.imag*that.real + this.real*that.imag
            );
        }
        if (typeof(that) === "number") {
            return new Complex(this.real*that, this.imag*that);
        }
        if (that instanceof Vector) return that.mul(this);
        throw "Can't mul Complex to: " + that;
    }

    copy()  { return new Complex(this.real, this.imag); }
    conj()  { return new Complex(this.real, -this.imag); }
    mag2()  { return this.real*this.real + this.imag*this.imag; }
    mag()   { return Math.hypot(this.real, this.imag); }
    angle() { return Math.atan2(this.imag, this.real); }
}

class Vector {
    constructor(real, imag) { this.real = real; this.imag = imag; }
    
    static complex(len, init=null) {
        let real = new Float64Array(len);
        let imag = new Float64Array(len);
        if (init !== null) {
            for (let ii = 0; ii<len; ++ii) {
                let zz = init(ii);
                if (zz instanceof Complex) {
                    real[ii] = zz.real;
                    imag[ii] = zz.imag;
                } else {
                    real[ii] = zz;
                }
            }
        }
        return new Vector(real, imag);
    }

    static real(len, init=null) {
        let real = new Float64Array(len);
        if (init !== null) {
            for (let ii = 0; ii<len; ++ii) {
                real[ii] = init(ii);
            }
        }
        return new Vector(real, null);
    }

    static concat(list) {
        let len = list.reduce((sum, vec) => {
            if (!(vec instanceof Vector) || vec.imag === null) {
                throw "must be Complex Vector";
            }
            return sum + vec.real.length;
        }, 0);
        let offset = 0;
        let result = Vector.complex(len);
        for (let vec of list) {
            result.real.set(vec.real, offset);
            result.imag.set(vec.imag, offset);
            offset += vec.real.length;
        }
        return result;
    }

    len() { return this.real.length; }

    get(ii) {
        if (this.imag === null) return this.real[ii];
        return new Complex(this.real[ii], this.imag[ii]);
    }

    sub(that) {
        const len = this.len();
        if (!(that instanceof Vector)) throw "must be vector";
        if (that.imag === null) throw "must be complex";
        let result = Vector.complex(len);
        for (let ii = 0; ii<len; ++ii) {
            result.real[ii] = this.real[ii] - that.real[ii];
            result.imag[ii] = this.imag[ii] - that.imag[ii];
        }
        return result;
    }

    mul(that) {
        const len = this.len();
        if (that instanceof Vector) {
            if (that.len() != len) throw "length must match";

            if (this.imag !== null && that.imag !== null) {
                let result = Vector.complex(len);
                for (let ii = 0; ii<len; ++ii) {
                    result.real[ii] = (
                        this.real[ii]*that.real[ii] -
                        this.imag[ii]*that.imag[ii]
                    );
                    result.imag[ii] = (
                        this.imag[ii]*that.real[ii] +
                        this.real[ii]*that.imag[ii]
                    );
                }
                return result;
            }

            if (this.imag !== null && that.imag === null) {
                let result = Vector.complex(len);
                for (let ii = 0; ii<len; ++ii) {
                    result.real[ii] = this.real[ii] * that.real[ii];
                    result.imag[ii] = this.imag[ii] * that.real[ii];
                }
                return result;
            }

            if (this.imag === null && that.imag !== null) {
                let result = Vector.complex(len);
                for (let ii = 0; ii<len; ++ii) {
                    result.real[ii] = this.real[ii] * that.real[ii];
                    result.imag[ii] = this.real[ii] * that.imag[ii];
                }
                return result;
            }

            if (this.imag === null && that.imag === null) {
                let result = Vector.real(len);
                for (let ii = 0; ii<len; ++ii) {
                    result.real[ii] = this.real[ii] * that.real[ii];
                }
                return result;
            }
        }

        if (typeof(that) === "number") {
            if (this.imag !== null) {
                let result = Vector.complex(len);
                for (let ii = 0; ii<len; ++ii) {
                    result.real[ii] = this.real[ii]*that;
                    result.imag[ii] = this.imag[ii]*that;
                }
                return result;
            }

            if (this.imag === null) {
                let result = Vector.real(len);
                for (let ii = 0; ii<len; ++ii) {
                    result.real[ii] = this.real[ii]*that;
                }
                return result;
            }
        }

        if (that instanceof Complex) {
            const { real, imag } = that;
            if (this.imag !== null) {
                let result = Vector.complex(len);
                for (let ii = 0; ii<len; ++ii) {
                    result.real[ii] = (
                        this.real[ii]*real -
                        this.imag[ii]*imag
                    );
                    result.imag[ii] = (
                        this.imag[ii]*real +
                        this.real[ii]*imag
                    );
                }
                return result;
            }

            if (this.imag === null) {
                let result = Vector.complex(len);
                for (let ii = 0; ii<len; ++ii) {
                    result.real[ii] = this.real[ii]*real;
                    result.imag[ii] = this.real[ii]*imag;
                }
                return result;
            }
        }

        throw "Vector can't multiply:" + that;
    }

    db() {
        if (self.imag === null) throw "expected Complex Vector";
        const { log10 } = Math;
        const len = this.len();
        let result = Vector.real(len);
        for (let ii = 0; ii<len; ++ii) {
            let re = this.real[ii];
            let im = this.imag[ii];
            result.real[ii] = 10*log10(re*re + im*im + 1e-100);
        }
        return result;
    }

    conj() {
        const len = this.len();
        if (this.imag !== null) {
            let result = Vector.complex(len);
            for (let ii = 0; ii<len; ++ii) {
                result.real[ii] = +this.real[ii];
                result.real[ii] = -this.imag[ii];
            }
            return result;
        } else {
            let result = Vector.real(len);
            for (let ii = 0; ii<len; ++ii) {
                result[ii].real = this.real[ii];
            }
            return result;
        }
    }

    mag() {
        const { hypot, abs } = Math;
        const len = this.len();
        let result = Vector.real(len);
        if (this.imag !== null) {
            for (let ii = 0; ii<len; ++ii) {
                result.real[ii] = hypot(this.real[ii], this.imag[ii]);
            }
        } else {
            for (let ii = 0; ii<len; ++ii) {
                result.real[ii] = abs(this.real[ii]);
            }
        }
        return result;
    }

    decimate(step) {
        let len = Math.floor(this.len()/step);
        if (this.imag !== null) {
            let result = Vector.complex(len);
            for (let ii = 0; ii<len; ++ii) {
                result.real[ii] = this.real[ii*step];
                result.imag[ii] = this.imag[ii*step];
            }
            return result;
        } else {
            let result = Vector.real(len);
            for (let ii = 0; ii<len; ++ii) {
                result.real[ii] = this.real[ii*step];
            }
            return result;
        }
    }

    dft() {
        const { cos, sin, PI } = Math;
        const len = this.len();
        let result = Vector.complex(len);
        for (let ii = 0; ii<len; ++ii) {
            let real = 0.0;
            let imag = 0.0;
            for (let jj = 0; jj<len; ++jj) {
                let aa = -2*PI*ii*jj/len;
                let cc = cos(aa);
                let ss = sin(aa);
                real += this.real[jj]*cc - this.imag[jj]*ss;
                imag += this.real[jj]*ss + this.imag[jj]*cc;
            }
            result.real[ii] = real;
            result.imag[ii] = imag;
        }
        return result;
    }

    fft() {
        const len = this.len();
        let result = Vector.complex(len);
        this._fft(result.real, result.imag, this.real, this.imag, len);
        return result;
    }

    ifft() {
        const len = this.len();
        let result = Vector.complex(this.len());
        // swapping reals and imags for ins and outs does inverse fft
        this._fft(result.imag, result.real, this.imag, this.real, len);
        return result;
    }

    _fft(dst_real, dst_imag, src_real, src_imag, len) {
        const fht = hartley(len);
        fht(dst_real, src_real);
        fht(dst_imag, src_imag);
        const half = len/2;
        for (let ii = 1; ii<half; ++ii) {
            const real_lo = dst_real[ii]*.5, real_hi = dst_real[len - ii]*.5;
            const imag_lo = dst_imag[ii]*.5, imag_hi = dst_imag[len - ii]*.5;
            const real_re = real_lo + real_hi, real_im = real_hi - real_lo;
            const imag_re = imag_lo + imag_hi, imag_im = imag_hi - imag_lo;
            dst_real[ii] = real_re - imag_im;
            dst_imag[ii] = imag_re + real_im;
            dst_real[len - ii] = real_re + imag_im;
            dst_imag[len - ii] = imag_re - real_im;
        }
    }
}

class Random {
    constructor() {
        [this.s0, this.s1, this.s2, this.s3] = [1n, 2n, 3n, 4n];
        this.jump();
    }

    normal() { // return zero-mean unit-variance Complex
        const { PI, sqrt, log, cos, sin } = Math;
        // the max value for mm is about 4.783
        const mm = sqrt(-log(this.uniform()));
        const aa = 2*PI*this.uniform();
        return new Complex(mm*cos(aa), mm*sin(aa));
    }

    uniform() { // return uniform Number in open interval (0, 1)
        const scale = 2.3283064365386963e-10; // 2**-32
        return (this.next() + .5)*scale;
    }

    next() { // return next 32 bit random BigInt
        const MASK = 0xFFFFFFFFn;

        // This is xoshiro128 from David Blackman and Sebastiano
        // Vigna who have placed their code in the public domain:
        //
        //     https://prng.di.unimi.it/xoshiro128plusplus.c
        //
        let result = rotl(this.s0 + this.s3, 7n) + this.s0;
        let tt = (this.s1 << 9n)&MASK;
        this.s2 ^= this.s0;
        this.s3 ^= this.s1;
        this.s1 ^= this.s2;
        this.s0 ^= this.s3;
        this.s2 ^= tt;
        this.s3 = rotl(this.s3, 11n);

        return Number(result&MASK);

        function rotl(xx, kk) {
            return ((xx << kk) | (xx >> 32n - kk))&MASK;
        }
    }

    jump() { // jump as though next() was called 2**64 times
        const JUMP = [
            0x8764000Bn, 0xF542D2D3n, 0x6FA035C3n, 0x77F2DB5Bn
        ];

        let [t0, t1, t2, t3] = [0n, 0n, 0n, 0n];
        for (let ii = 0; ii<4; ++ii) {
            for (let bb = 0n; bb<32n; bb++) {
                if (0) console.log(
                    t0.toString(16), t1.toString(16),
                    t2.toString(16), t3.toString(16)
                );
                if (JUMP[ii] & (1n << bb)) {
                    t0 ^= this.s0;
                    t1 ^= this.s1;
                    t2 ^= this.s2;
                    t3 ^= this.s3;
                }
                this.next();
            }
        }

        this.s0 = t0;
        this.s1 = t1;
        this.s2 = t2;
        this.s3 = t3;
    }
}

class Stream {
    constructor(maxlen=32768) {
        this.buffer = new Float64Array(maxlen);
        // Start with zeros "preloaded" so we can do non-causal filters
        const preload = maxlen/4|0;
        this.offset = -preload; // running stream offset of buffer[0]
        this.length = +preload; // number of valid samples in buffer
    }

    push(slice) {
        if (this.length + slice.length > this.buffer.length) {
            throw "Need a bigger maxlen";
        }
        this.buffer.set(slice, this.length);
        this.length += slice.length;
    }

    pull(offset, length) {
        const start = offset - this.offset;
        if (start < 0) throw "can't go back in time";
        if (start + length > this.length) {
            if (start < this.length) {
                this.buffer.copyWithin(0, start, this.length);
                this.offset += start;
                this.length -= start;
            } else {
                this.offset = offset;
                this.length = 0;
            }
            return null;
        }
        return this.buffer.subarray(start, start + length);
    }
}

function specwin(len) {
    let result = Vector.real(len);
    for (let ii = 0; ii<len; ++ii) {
        // XXX: This should probably be Blackman alpha = .16
        result.real[ii] = firwin(3, ii - 0.5*len, len);
    }
    return result;
}

function listen(framelen, samplerate, callback) {
    const device_to_resample = new Stream();
    const resample_to_baseband = new Stream();

    let sample_offset = 0;
    let sample_time = 0.0;
    let resample_step = 0;
    let resample_firs;
    let resample_taps = 0;
    let resample_whole = 0;
    let resample_fract = 0.0;
    const resample_length = 1024;
    const resample_buffer = new Float64Array(resample_length);

    let baseband_offset = 0;
    const baseband_fir = hilbert57();

    navigator.mediaDevices.getUserMedia({ audio: {
        autoGainControl: false,
        echoCancellation: false,
        noiseSuppression: false,
    }}).then(stream => {
        const audio = new AudioContext();
        const devrate = audio.sampleRate;
        console.log("device rate:", devrate);
        resample_step = devrate / (2*samplerate);
        console.log("decimate amount:", resample_step);
        const transbw = devrate/2 - samplerate;
        if (resample_step < 1 || transbw <= 0) {
            throw "need higher sound card rate";
        }
        resample_taps = Math.ceil(firlen(4, devrate, transbw));
        if (resample_taps % 2 != 0) resample_taps += 1;
        if (resample_taps < 4) resample_taps = 4;
        console.log("resample taps:", resample_taps);
        resample_firs = polyphase(4, resample_taps);
        const source = audio.createMediaStreamSource(stream);
        const filter = audio.createConvolver();
        const block = 16384;
        const script = audio.createScriptProcessor(block, 1, 1);
        filter.normalize = false;
        filter.buffer = lowpass(2*samplerate, audio);
        let sampnum = 0;
        script.onaudioprocess = evt => {
            if (sampnum % (100*block) == 0) {
                sample_offset = sampnum*samplerate/devrate;
                sample_time = Date.now()*.001;
            }
            sampnum += block;
            const data = evt.inputBuffer.getChannelData(0);
            device_to_resample.push(data);
            resample();
            baseband();
        };
        source.connect(filter);
        filter.connect(script);
        script.connect(audio.destination);
    });

    function resample() {
        const { floor, ceil, round } = Math;
        const taps = resample_taps;
        for (;;) {
            let pull_length = ceil(
                resample_fract + resample_taps +
                resample_length*resample_step
            );
            let slice = device_to_resample.pull(resample_whole, pull_length);
            if (slice === null) break;
            for (let ii = 0; ii<resample_length; ii++) {
                let offset = resample_fract + ii*resample_step;
                let whole = floor(offset);
                let fract = offset - whole;
                let index = round(1024*fract);
                let sum = 0.0;
                for (let jj = 0; jj<taps; jj++) {
                    sum += resample_firs[index*taps + jj]*slice[whole + jj];
                }
                resample_buffer[ii] = sum;
            }
            resample_to_baseband.push(resample_buffer);
            let next_offset = (
                resample_whole + resample_fract + 
                resample_length*resample_step
            );
            resample_whole = floor(next_offset);
            resample_fract = next_offset - resample_whole;
        }
    }

    function baseband() {
        let input;
        while (input = resample_to_baseband.pull(
            baseband_offset, 2*framelen + 2*113
        )) {
            let result = Vector.complex(framelen);
            for (let ii = 0; ii<framelen; ++ii) {
                let jj = 2*ii + 113;
                let sum = 0.0;
                for (let kk = 0; kk<57; kk++) {
                    let ll = 113 - 2*kk;
                    sum += baseband_fir[kk]*(input[jj - ll] - input[jj + ll]);
                }
                result.real[ii] = input[jj];
                result.imag[ii] = sum;
            }
            let time = sample_time + (
                0.5*baseband_offset - sample_offset
            )/samplerate;
            baseband_offset += 2*framelen;
            callback(result, time);
        }
    }

    function lowpass(rate, audio) {
        const trans = 200;
        let taps = Math.ceil(firlen(4, audio.sampleRate, trans));
        if (taps%2 == 0) taps += 1;
        console.log("lowpass taps:", taps);
        const width = rate / audio.sampleRate;
        let buffer = audio.createBuffer(1, taps, audio.sampleRate);
        let impulse = buffer.getChannelData(0);
        const mid = Math.floor(taps/2);
        let sum = 0.0;
        for (let ii = 0; ii<taps; ++ii) {
            const xx = ii - mid;
            sum += impulse[ii] = sinc(xx*width)*firwin(4, xx, taps); 
        }
        let scale = 1.0/sum;
        for (let ii = 0; ii<taps; ++ii) impulse[ii] *= scale;
        return buffer;
    }

    function hilbert57() {
        const { PI } = Math;
        // Builds a 230 tap Hilbert FIR using 40 multiplies.
        // -80dB sidelobes, 18dB / octave rolloff,
        // 100Hz transition at 8192Hz input rate (4096Hz out)
        let result = new Float64Array(57);
        for (let ii = 0; ii<57; ii++) {
            let kk = 113 - 2*ii;
            result[ii] = firwin(3, kk, 230)/(.5*PI*kk);
        }
        return result;
    }

    function polyphase(winfun, taps) {
        let result = new Float64Array(1025*taps);
        for (let ii = 0; ii<=1024; ii++) {
            let center = taps/2 - 1 + ii/1024;
            let sum = 0.0;
            for (let jj = 0; jj<taps; jj++) {
                let xx = jj - center;
                let tap = sinc(xx)*firwin(winfun, xx, taps);
                sum += result[ii*taps + jj] = tap;
            }
            let scale = 1.0/sum;
            for (let jj = 0; jj<taps; jj++) {
                result[ii*taps + jj] *= scale;
            }
        }
        return result;
    }
}

function play(samples, rate) {
    if (play.audio === undefined) {
        play.audio = new window.AudioContext();
    }
    if (!(samples instanceof Float32Array)) {
        samples = new Float32Array(samples);
    }
    const buffer = play.audio.createBuffer(
        1, samples.length, rate //audio.sampleRate
    );
    buffer.copyToChannel(samples, 0);
    const source = play.audio.createBufferSource();
    source.buffer = buffer;
    source.connect(play.audio.destination);
    source.start();
}

function linchirp(bw, len, fs) {
    const { PI, cos, sin } = Math;
    const ss = len/fs;
    const aa = bw/(2*ss);
    const bb = -bw/2;
    const result = new CxVec(len);
    for (let ii = 0; ii<len; ++ii) {
        const tt = ii/fs;
        const pp = 2*PI*(aa*tt + bb)*tt;
        result.real[ii] = cos(pp);
        result.imag[ii] = sin(pp);
    }
    return result;
}

function quadpeak(left, middle, right) {
    let two_a = (left - 2*middle + right);
    let neg_b = (right - left);
    return neg_b/two_a;
}

function pickpeaks(frame) {
    let len = frame.len();
    let range = 8, size = 2*range + 1;
    let slice = new Float64Array(size);
    let peaks = [];
    for (let ii = 0; ii<len; ++ii) {
        let left = frame.data[(ii + len - 1)%len];
        let middle = frame.data[ii];
        let right = frame.data[(ii + len + 1)%len]
        if (left < middle && middle > right) {
            for (let jj = 0; jj<size; ++jj) {
                slice[jj] = frame.data[(ii + len - range + jj)%len];
            }
            slice.sort();
            if (frame.data[ii] > 14 + slice[range]) {
                peaks.push(ii + quadpeak(left, middle, right));
            }
        }
    }
    return peaks;
}

function sinc(xx) {
    const { PI, sin } = Math;
    return xx === 0.0 ? 1.0 : sin(PI*xx)/(PI*xx);
}

function firwin(winfun, offset, length) {
    const { PI, cos } = Math;
    if (offset > +0.5*length) return 0;
    if (offset < -0.5*length) return 0;
    const xx = PI*offset/length;
    switch (winfun) {
        case 0: // sidelobe:  -20.9,  ripple: 1.2e-0 dB,  rolloff:  6 dB
            return 1.0;
        case 1: // sidelobe:  -33.7,  ripple: 2.5e-1 dB,  rolloff: 12 dB 
            return cos(xx);
        case 2: // sidelobe:  -43.9,  ripple: 7.2e-2 dB,  rolloff: 18 dB
            return 0.5 + 0.5*cos(2*xx);
        case 3: // sidelobe:  -80.4,  ripple: 1.5e-3 dB,  rolloff: 18 dB
            return (
                8.999994258544674608e-02*cos(4*xx) +
                5.000000000000000000e-01*cos(2*xx) +
                4.100000574145532539e-01
            );
        case 4: // sidelobe: -100.3,  ripple: 1.4e-4 dB,  rolloff: 30 dB
            return (
                1.817828495156346286e-02*cos(6*xx) +
                1.613565699031269118e-01*cos(4*xx) +
                4.818217150484365163e-01*cos(2*xx) +
                3.386434300968730882e-01
            );
        case 5: // sidelobe: -118.5,  ripple: 1.5e-5 dB,  rolloff: 42 dB
            return (
                3.780611465758034698e-03*cos(8*xx) +
                4.637244586303213706e-02*cos(6*xx) +
                2.026224458630321301e-01*cos(4*xx) +
                4.536275541369678699e-01*cos(2*xx) +
                2.935969426712098373e-01
            );
        case 6: // sidelobe: -135.7,  ripple: 2.0e-6 dB,  rolloff: 54 dB
            return (
                8.060440222729769837e-04*cos(10*xx) +
                1.264876413363786103e-02*cos(8*xx) +
                7.297857228954869602e-02*cos(6*xx) +
                2.251983521781838193e-01*cos(4*xx) +
                4.262153836881783509e-01*cos(2*xx) +
                2.621528836881783509e-01
            );
    }
    throw "invalid firwin";
}

function firlen(winfun, inrate, transbw) {
    // transbw is the bandwidth between cutoff point (-6dB power)
    // for the filter and the first null before the sidelobes.
    switch (winfun) {
        case 0: return 0.618 * inrate/transbw;
        case 1: return 1.153 * inrate/transbw;
        case 2: return 1.674 * inrate/transbw;
        case 3: return 2.8109613 * inrate/transbw;
        case 4: return 3.8354245 * inrate/transbw;
        case 5: return 4.8523531 * inrate/transbw;
        case 6: return 5.8650296 * inrate/transbw;
    }
    throw "invalid firwin";
}

function colormap(xx) {
    const rr = 255*xx|0;
    const gg = 255*((.75*xx*xx + .25)*xx)|0;
    const bb = 255*(((8*xx - 12)*xx + 5)*xx)|0;
    return [rr, gg, bb];
}

function hartley(size) {
    if (hartley.cache === undefined) hartley.cache = {};
    let result = hartley.cache[size];
    if (result !== undefined) return result;
    
    if (size < 8) throw "must be larger than size 8";
    const log2 = Math.round(Math.log2(size));
    if (2**log2 != size) throw "must be power of 2 size";

    const perm = new Uint32Array(size);
    for (let ii = 0; ii<size; ++ii) {
        let fwd = ii>>3, rev = ii%8;
        for (let jj = 0; jj<log2-3; ++jj) {
            rev = rev*2 + fwd%2;
            fwd = fwd/2|0;
        }
        perm[ii] = rev;
    }

    const cos = new Float64Array(size/4);
    const sin = new Float64Array(size/4);
    for (let ii = 0, nn = size/4|0; ii<nn; ++ii) {
        const angle = 2*Math.PI*ii/size;
        cos[ii] = Math.cos(angle);
        sin[ii] = Math.sin(angle);
    }

    return hartley.cache[size] = (dst, src) => {
        for (let ii = 0; ii<size; ++ii) {
            dst[ii] = src[perm[ii]];
        }

        first8(dst, size);
        let reps = size/8, cols = 8;
        for (let ii = 0; ii<log2-3; ++ii) {
            reps /= 2;
            radix2(dst, cols, reps);
            cols *= 2;
        }
    };

    function first8(data, size) {
        const rt2 = 1.4142135623730951455;
        for (let ii = 0; ii < size; ii += 8) {
            const aa = data[ii + 0], bb = data[ii + 1];
            const cc = data[ii + 2], dd = data[ii + 3];
            const ee = data[ii + 4], ff = data[ii + 5];
            const gg = data[ii + 6], hh = data[ii + 7];
            const apb = aa + bb,  amb = aa - bb;
            const apc = aa + cc,  amc = aa - cc;
            const cpd = cc + dd,  cmd = cc - dd;
            const epf = ee + ff,  emf = ee - ff;
            const gph = gg + hh,  gmh = gg - hh;
            const epg = ee + gg,  emg = ee - gg;
            const bmf = bb - ff,  dmh = dd - hh;
            data[ii + 0] = (apb + cpd) + (epf + gph);
            data[ii + 1] = (apc - epg) + rt2*bmf;
            data[ii + 2] = (apb - cpd) + (epf - gph);
            data[ii + 3] = (amc - emg) + rt2*dmh;
            data[ii + 4] = (amb + cmd) + (emf + gmh);
            data[ii + 5] = (apc - epg) - rt2*bmf;
            data[ii + 6] = (amb - cmd) + (emf - gmh);
            data[ii + 7] = (amc - emg) - rt2*dmh;
        }
    }

    function radix2(data, cols, reps) {
        const half = cols>>1;
        for (let rr = 0; rr<reps; ++rr) {
            const row0 = (2*rr + 0)*cols;
            const row1 = (2*rr + 1)*cols;
            const row2 = (2*rr + 2)*cols;

            for (let jj = 1; jj < half; ++jj) {
                const ll = data[row1 + jj], cc = cos[jj*reps];
                const rr = data[row2 - jj], ss = sin[jj*reps];
                data[row1 + jj] = cc*ll + ss*rr;
                data[row2 - jj] = ss*ll - cc*rr;
            }

            for (let jj = 0; jj < cols; ++jj) {
                const ll = data[row0 + jj];
                const rr = data[row1 + jj];
                data[row0 + jj] = ll + rr;
                data[row1 + jj] = ll - rr;
            }
        }
    }
}

function plot_frame(context, screen, chart, options={}) {
    const opts = {
        frame_style: "#FFF", grid_style:  "#333",
        label_style: "#FC9", tick_style:  "#999",
        label_font: '20px monospace',
        xunits: "U", yunits: "U",
        xticks: 10, yticks: 6,
        ... options
    };

    context.save();
    context.font = opts.label_font;

    const metric = context.measureText("0.1");
    const height = (
        metric.actualBoundingBoxAscent +
        metric.actualBoundingBoxDescent
    );
    const pad = height*0.5;
    const yticks = ticks(chart.y, chart.y + chart.h, opts.yticks);
    const yunits = units(opts.yunits, yticks.exponent);
    context.fillStyle = opts.label_style;
    context.textAlign = "right";
    context.textBaseline = "top";
    draw_text(opts.label_style, yunits, screen.x - 1.5*pad, screen.y);
    const ymetric = context.measureText(yunits);
    const ystart = screen.y + 2*height;
    const ystop = screen.y + screen.h - height;
    context.textAlign = "right";
    context.textBaseline = "middle";
    const yscale = 10**yticks.exponent;
    for (let ytext of yticks.values) {
        const ynum = ytext*yscale;
        const yy = screen.y + screen.h - (ynum - chart.y)*screen.h/chart.h;
        draw_line(opts.tick_style, screen.x, yy, screen.x - pad, yy);
        draw_line(opts.grid_style, screen.x, yy, screen.x + screen.w, yy);
        if (ystart < yy && yy < ystop) {
            draw_text(opts.label_style, ytext, screen.x - 1.5*pad, yy);
        }
    }

    outer:
    for (let count = opts.xticks; count > 2; --count) {
        const xticks = ticks(chart.x, chart.x + chart.w, count);
        const xscale = 10**xticks.exponent;
        let lastx = 0;
        for (let xtext of xticks.values) {
            const xnum = xtext*xscale;
            const xx = screen.x + (xnum - chart.x)*screen.w/chart.w;
            const width = context.measureText(xtext + "  ").width;
            if (xx - 0.5*width < lastx) continue outer;
            lastx = xx + 0.5*width;
        }
        const xunits = units(opts.xunits, xticks.exponent);
        context.textAlign = "right";
        context.textBaseline = "top";
        draw_text(
            opts.label_style, xunits,
            screen.x + screen.w,
            screen.y + screen.h + 1.5*pad
        );
        const xkeep = (
            screen.x + screen.w - context.measureText(xunits).width
        );
        context.textAlign = "center";
        context.textBaseline = "top";
        for (let xtext of xticks.values) {
            const xnum = xtext*xscale;
            const xx = screen.x + (xnum - chart.x)*screen.w/chart.w;
            const yy = screen.y + screen.h;
            draw_line(opts.tick_style, xx, yy, xx, yy + pad);
            draw_line(opts.grid_style, xx, yy, xx, yy - screen.h);
            if (xx + context.measureText(xtext).width/2 < xkeep) {
                draw_text(opts.label_style, xtext, xx, yy + 1.5*pad);
            }
        }
        break;
    }

    draw_line(
        opts.frame_style, screen.x, screen.y,
        screen.x, screen.y + screen.h
    );
    draw_line(
        opts.frame_style, screen.x, screen.y + screen.h,
        screen.x + screen.w, screen.y + screen.h
    );

    context.restore();

    function units(base, exponent) {
        const prefix = {
            "-24": "y",  "-21": "z",  "-18": "a",  "-15": "f",
            "-12": "p",   "-9": "n",   "-6": "u",   "-3": "m",
              "3": "k",    "6": "M",    "9": "G",   "12": "T",
             "15": "P",   "18": "E",   "21": "Z",   "24": "Y",
        };
        if (exponent == 0) return base;
        const pre = prefix[exponent];
        if (pre !== undefined) return pre + base;
        return "10**" + exponent + " " + base;
    }

    function draw_line(style, x0, y0, x1, y1) {
        context.strokeStyle = style;
        context.beginPath();
        context.moveTo(x0 - .5, y0 + .5);
        context.lineTo(x1 - .5, y1 + .5);
        context.stroke();
    }

    function draw_text(style, text, xx, yy) {
        context.fillStyle = style;
        context.fillText(text, xx - .5, yy + .5);
    }
    
    function ticks(minvalue, maxvalue, maxticks) {
        for (let count = maxticks; count > 0; --count) {
            const delta = friendly((maxvalue - minvalue)/count);
            const start = Math.ceil(minvalue/delta)*delta;
            const stop = Math.floor(maxvalue/delta)*delta;

            let exponent = Math.floor(Math.log10(delta + 1e-300));
            let fixed = -exponent;
            if (exponent%3) exponent -= (exponent + 3333)%3;
            if (Math.abs(start) > 1000 || Math.abs(stop) > 1000) {
                exponent += 3;
            }
            fixed += exponent;
            if (fixed < 0) fixed = 0;

            let values = [];
            const scale = 10**-exponent;
            for (let ii = 0;; ++ii) {
                const value = start + ii*delta;
                if (value < minvalue) continue;
                if (value > maxvalue) break;
                values.push((scale*value).toFixed(fixed));
            }
            if (values.length <= maxticks) return { exponent, values };
        }
        return { exponent: 0, values: [] };
        
        function friendly(value) {
            const exponent = Math.floor(Math.log10(value + 1e-300));
            const fraction = value/10**exponent;
            if (fraction <= 1.5) return 1.0*10**exponent;
            if (fraction <= 3.0) return 2.0*10**exponent;
            if (fraction <= 7.5) return 5.0*10**exponent;
            return 10*10**exponent;
        }
    }
}

function fwht(data) { // Fast Walsh-Hadamard Transform
    const len = data.length;
    if (len & (len - 1)) throw "fwht must be power of 2";
    for (let reps = 1, step = len>>1; step; reps <<= 1, step >>= 1) {
        for (let rr = 0; rr<reps; ++rr) {
            const lo = 2*rr*step;
            const hi = lo + step;
            for (let ii = 0; ii<step; ++ii) {
                let [aa, bb] = [data[lo + ii], data[hi + ii]];
                [data[lo + ii], data[hi + ii]] = [aa + bb, aa - bb];
            }
        }
    }
}

function fourier2(size) {
    if (size & (size - 1)) throw "fft must be power of 2";
    const perm = new Uint32Array(size);
    const cos = new Float64Array(size);
    const sin = new Float64Array(size);
    const root = -2*Math.PI/size;
    for (let ii = 0; ii<size; ++ii) {
        let bitrev = 0;
        for (let lo = 1, hi = size>>1; hi; hi >>= 1, lo <<= 1) {
            if (ii&hi) bitrev |= lo;
        }
        perm[ii] = bitrev;
        cos[ii] = Math.cos(ii*root);
        sin[ii] = Math.sin(ii*root);
    }

    return (dst_real, dst_imag, src_real, src_imag) => {
        for (let ii = 0; ii<size; ++ii) {
            let pp = perm[ii];
            dst_real[ii] = src_real[pp];
            dst_imag[ii] = src_imag[pp];
        }

        for (let rr = size>>1, cc=1; rr; rr >>= 1, cc <<= 1) {
            radix2(dst_real, dst_imag, rr, cc);
        }
    };

    function radix2(real, imag, reps, cols) {
        let off0 = 0, off1 = cols;
        for (let kk = 0; kk<reps; ++kk) {
            let index = reps;
            for (let jj = 1; jj<cols; ++jj) {
                let loc = off1 + jj;
                let cc = cos[index];
                let ss = sin[index];
                let re = real[loc];
                let im = imag[loc];
                real[loc] = re*cc - im*ss;
                imag[loc] = re*ss + im*cc;
                index += reps;
            }
            for (let jj = 0; jj<cols; ++jj) {
                let r0 = real[off0];
                let i0 = imag[off0];
                let r1 = real[off1];
                let i1 = imag[off1];
                real[off0] = r0 + r1;
                imag[off0] = i0 + i1;
                real[off1] = r0 - r1;
                imag[off1] = i0 - i1;
                ++off0;
                ++off1;
            }
            off0 += cols;
            off1 += cols;
        }
    }
}

