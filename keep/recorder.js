
/*
function listen(millis, rate, callback) {
    navigator.mediaDevices.getUserMedia({ audio: true }).then(stream => {
        const recorder = new MediaRecorder(
            //stream, { audioBitsPerSecond: 1e6, mimeType: "audio/wav" }
            stream, { audioBitsPerSecond: 1e6 }
        );
        const context = new AudioContext({ sampleRate: rate });

        recorder.addEventListener("dataavailable", event => {
            event.data.arrayBuffer().then(encoded => {
                context.decodeAudioData(encoded).then(callback);
            });
        });

        recorder.start();
        setInterval(() => {
            recorder.stop();
            recorder.start();
        }, millis);
    });
}
listen(1000, 16000, data => {
    console.log(data, data.getChannelData(0));
});
*/


/*
navigator.mediaDevices.getUserMedia({ audio: true }).then(stream => {
    const mediaRecorder = new MediaRecorder(stream);

    let audioChunks = [];

    let audioCtx = new AudioContext();

    mediaRecorder.addEventListener("dataavailable", event => {
        console.log("data available");
        audioChunks.push(event.data);
        event.data.arrayBuffer().then(buf => {
            console.log("got buf:", buf);
            audioCtx.decodeAudioData(buf).then(decode => {
                console.log("decode:", decode);
                let chan = decode.getChannelData(0);
                for (let ii = 0; ii<10; ii++) {
                    console.log(chan[ii]);
                }
            });
        });
    });

    mediaRecorder.addEventListener("stop", () => {
        const audioBlob = new Blob(audioChunks);
        audioChunks = [];
        const audioUrl = URL.createObjectURL(audioBlob);
        const audio = new Audio(audioUrl);
        audio.play();
    });

    mediaRecorder.start();
    setInterval(() => {
        mediaRecorder.stop();
        mediaRecorder.start();
    }, 1000);
});
*/

