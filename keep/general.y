
%token
    LBRACE RBRACE LPAREN RPAREN LCURLY RCURLY KEYWORD MIDWORD PREWORD COMMA
    EQUAL ARROW COLON QUEST SEMI OR_EQ AND_EQ ADD_EQ SUB_EQ MUL_EQ DIV_EQ
    MOD_EQ MAT_EQ VID_EQ POW_EQ IS NI OR AND NOT EQ NE LT LE GT GE ADD SUB MUL
    DIV MOD MAT VID POW TICK DOT IDENT STRING SYMBOL INTEGER FLOATING

%%

block
    : // zero or more
    | block stmt
    ;


stmt
    : KEYWORD more
    | equal SEMI
    | curly
    | SEMI
    ;

more
    :       SEMI
    | equal SEMI
    |       curly
    | equal curly
    | equal       MIDWORD more
    |       curly MIDWORD more
    | equal curly MIDWORD more
    ;

paren: LPAREN comma RPAREN;
brace: LBRACE comma RBRACE;
curly: LCURLY block RCURLY;

//
// It took a while to figure out the precedence of these first few,
// so I'm documenting it here with some examples:
//
//     precedence: comma < equal < arrow < colon < quest
//
//     f(123, a = (x:i64):f64 => x < 0 ? g(x) : h(x));
//     let (a, b) = g(x, y);
//     (c, d) = (d, c);
//

comma
    : // zero or more
    | equal
    | equal COMMA comma
    ;

equal // rassoc
    : arrow EQUAL equal
    | arrow OR_EQ equal
    | arrow AND_EQ equal
    | arrow ADD_EQ equal
    | arrow SUB_EQ equal
    | arrow MUL_EQ equal
    | arrow DIV_EQ equal
    | arrow MOD_EQ equal
    | arrow MAT_EQ equal
    | arrow VID_EQ equal
    | arrow POW_EQ equal
    | arrow
    ;
    
arrow // rassoc
    : colon ARROW arrow
    | colon ARROW curly
    | colon
    ;

colon // rassoc
    : quest COLON colon
    | quest
    ;

quest
    : binor QUEST quest COLON quest
    | binor
    ;

binor // lassoc
    : binor OR binand
    | binand
    ;

binand // lassoc
    : binand AND bincmp
    | bincmp
    ;

bincmp // nassoc
    : binadd IS binadd
    | binadd NI binadd
    | binadd EQ binadd
    | binadd NE binadd
    | binadd LT binadd
    | binadd LE binadd
    | binadd GT binadd
    | binadd GE binadd
    | binadd
    ;

binadd // lassoc
    : binadd ADD binmul
    | binadd SUB binmul
    | binmul
    ;

binmul // lassoc
    : binmul MUL prefix
    | binmul DIV prefix
    | binmul MOD prefix
    | binmul MAT prefix
    | binmul VID prefix
    | prefix
    ;

prefix
    : ADD prefix
    | SUB prefix
    | NOT prefix
    | AND prefix
    | PREWORD suffix
    /*
    | MATRIX suffix
    | COLVEC suffix
    | ROWVEC suffix
    | BOXED suffix
    */
    | binpow
    ;

binpow // rassoc
    : suffix POW prefix
    | suffix
    ;

suffix
    : suffix TICK
    | suffix DOT IDENT
    | suffix DOT INTEGER
    | suffix paren
    | suffix brace
    | basic
    ;

basic
    : paren
    | IDENT
    | STRING
    | INTEGER
    | FLOATING
    ;

%%

