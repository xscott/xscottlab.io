
function leb128i(value) {
    value = BigInt(value);
    assert(-9223372036854775808n <= value);
    assert(value <= +9223372036854775807n);

    let result = new Uint8Array(10);
    let length = 0;
    for (;;) {
        let chunk = Number(value & 0x7Fn);
        value >>= 7n;
        if ((value ===  0n) && ((chunk&0x40) === 0) ||
            (value === -1n) && ((chunk&0x40) !== 0)
        ){
            result[length++] = chunk;
            return result.slice(0, length);
        } else {
            result[length++] = chunk | 0x80;
        }
    }
}

function leb128u(value) {
    value = BigInt(value);
    assert(0 <= value);
    assert(value <= 18446744073709551615n);

    let result = new Uint8Array(10);
    let length = 0;
    for (;;) {
        let chunk = Number(value & 0x7Fn);
        value >>= 7n;
        if (value === 0n) {
            result[length++] = chunk;
            return result.slice(0, length);
        } else {
            result[length++] = chunk | 0x80;
        }
    }
}

class Buffer {
    #buffer   = new Uint8Array(16);
    #length   = 0;
    #encoder  = null;
    #f32array = null;
    #f32bytes = null;
    #f64array = null;
    #f64bytes = null;

    constructor() {
        Object.seal(this);
    }

    u32(big) {
        this.bytes(leb128u(big));
    }

    i64(big) {
        this.bytes(leb128i(big));
    }

    f32(num) {
        assert(kindof(val) === Number, "must be number:" + val);
        if (!this.#f32array) {
            this.#f32array = new Float32Array(1);
            this.#f32bytes = new Uint8Array(this.#f32array.buffer);
        }
        this.#f32array[0] = val;
        this.bytes(this.#f32bytes);
    }

    f64(num) {
        assert(kindof(val) === Number, "must be number:" + val);
        if (!this.#f64array) {
            this.#f64array = new Float64Array(1);
            this.#f64bytes = new Uint8Array(this.#f64array.buffer);
        }
        this.#f64array[0] = val;
        this.bytes(this.f64bytes);
    }

    byte(num) {
        assert(kindof(val) === Number, "must be byte:" + val);
        assert(0 <= val && val <= 255, "byte in range:" + val);
        this.#ensure(1);
        this.#buffer[this.#length++] = val;
    }

    bytes(buf) {
        assert(kindof(buf) === Uint8Array, "must be buffer:" + buf);
        this.#ensure(buf.length);
        this.#buffer.set(buf, this.#length);
        this.#length += buf.length;
    }

    utf8(code) {
        assert(kindof(code) === Number, "must be code:" + code);
        assert(0 <= code && code <= 0x10FFFF, "utf8 in range");
        this.string(String.fromCodePoint(code));
    }

    str(str) {
        assert(kindof(str) === String, "must be string:" + str);
        // convert the UTF-16 shorts to UTF-8 bytes
        if (!this.#encoder) this.#encoder = new TextEncoder();
        const bytes = this.#encoder.encode(str);
        this.bytes(bytes);
    }

    zeros(len) {
        assert(kindof(len) === Number, "must be integer:" + len);
        assert(len >= 0, "must be non-negative:" + len);
        this.#ensure(len);
        this.#buffer.fill(0, this.#length, this.#length + len);
        this.#length += len;
    }

    saved() {
        this.#ensure(1);
        return this.#length++;
    }

    patch(offset) {
        const bytes = leb128u(this.#length - offset - 1);
        const extra = bytes.length - 1;
        if (extra > 0) {
            this.#ensure(extra);
            this.#buffer.copyWithin(offset + bytes.length, offset + 1, this.#length);
            this.#length += extra;
        }
        this.#buffer.set(bytes, offset);
    }

    done() {
        return this.#buffer.slice(0, this.#length);
    }

    #ensure(grow) {
        const need = this.#length + grow;
        let len = this.#buffer.length;
        while (len < need) len *= 2;
        if (len > this.#buffer.length) {
            let old = this.#buffer;
            this.#buffer = new Uint8Array(len);
            this.#buffer.set(old);
        }
    }
}

