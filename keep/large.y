
%token
    LET VAR TYPE FUNC IF ELIF ELSE WHILE FOR
    BREAK CONTINUE RETURN NEW STRUCT UNION ASSERT
    LPAREN RPAREN LBRACE RBRACE LCURLY RCURLY
    COMMA EQUAL ARROW QUEST COLON SEMI
    OR AND NOT EQ NE LT LE GT GE
    ADD SUB MUL DIV MOD ATS VID POW DOT TILDE TICK
    LIT_I64 LIT_F64 LIT_BOOL LIT_STR LIT_SYM IDENT

    // Stuff that is too hard for now:
    //   Augmented Assignments
    //   ENUM MATCH
    //   TRY CATCH THROW

%%

codeblock
    : // zero or more
    | codeblock statement
    ;

statement
    : curlyblock
    | LET letorvarstmt
    | VAR letorvarstmt
    | TYPE typestmt
    | FUNC funcstmt
    | IF ifthenstmt
    | RETURN returnstmt
    | WHILE whilestmt
    | FOR forstmt
    | BREAK breakstmt
    | CONTINUE continuestmt
    | ASSERT assertstmt
    | exprstmt
    | assignstmt
    | SEMI
    ;

curlyblock: LCURLY codeblock RCURLY;

letorvarstmt: letorvars EQUAL exprlist SEMI;

letorvars
    : letorvar COMMA letorvars
    | letorvar
    | // zero or more
    ;

letorvar
    : IDENT
    | IDENT COLON typespec
    ;

//
// Someday (not soon), we'll add this:
//
// type List[T] = enum {
//      Node(List, T);
///     Null;
// }
//
// let l = new List.Null;
//
// match l {
//     Node(next): { ... }
//     Null: { ... }
// }
//

typestmt
    : IDENT EQUAL unistruct 
    | IDENT EQUAL typespec 
    ;

unistruct
    : STRUCT LPAREN fieldlist RPAREN
    | UNION  LPAREN fieldlist RPAREN
    ;

fieldlist
    : IDENT COLON typespec COMMA
    | IDENT COLON unistruct COMMA
    | unistruct
    ;

funcstmt
    : funcname LPAREN paramlist RPAREN COLON resultlist curlyblock
    | funcname LPAREN paramlist RPAREN /* no results */ curlyblock
    ;

funcname
    : IDENT | OR | AND | EQ | NE | LT | LE | GT | GE
    | ADD | SUB | MUL | DIV | MOD | VID | ATS | POW
    | NOT | TICK | TILDE
    ;

paramlist
    : parameter COMMA paramlist
    | parameter
    | // zero or more
    ;

parameter // OptionalParam, RequiredParam
    : IDENT COLON typespec EQUAL expression
    | IDENT COLON typespec
    ;

resultlist
    : typespec COMMA resultlist
    | typespec
    | // zero or more
    ;

returnstmt
    : RETURN exprlist SEMI
    ;

ifthenstmt
    : expression curlyblock elifelse;
    ;

elifelse
    : ELIF expression curlyblock elifelse
    | ELSE curlyblock
    | // empty
    ;

whilestmt
    : COLON IDENT expression curlyblock
    |             expression curlyblock
    ;

forstmt
    : COLON IDENT IDENT EQUAL curlyblock
    |             IDENT EQUAL curlyblock
    ;

breakstmt
    : IDENT SEMI
    |       SEMI
    ;

continuestmt
    : IDENT SEMI
    |       SEMI
    ;

assertstmt
    : expression SEMI
    | expression COMMA expression SEMI

exprstmt
    : expression SEMI
    ;

assignstmt
    : exprlist EQUAL exprlist SEMI
    ;

typespec
    : IDENT
    | lambdatype
    // This is for both fixed sized arrays
    // and specifiers for generic types:
    //
    // let vec: f64[3] = foo();
    // let mat: matrix[f64] = bar();
    //
    | typespec LBRACE speclist RBRACE
    ;

speclist
    : specifier COMMA speclist
    | specifier
    | // zero or more
    ;

specifier
    : typespec
    | LIT_I64
    ;

lambdatype
    : LPAREN typelist ARROW typelist RPAREN
    // Examples:
    //
    //   func sort(data: list[i64], less: (i64, i64 => bool))
    //
    //   var less = (x:i64, y:i64): bool => { return x<y; }
    //
    //   # A lambda function that returns a lambda function
    //   var meta = (x:i64): (f64 => str) => (
    //       (f:f64): str => "meta"
    //   );
    //
    ;

typelist
    : typespec COMMA typelist
    | typespec
    | // zero more
    ;

exprlist
    : expression COMMA exprlist
    | expression
    | // zero or more
    ;

expression: lambdafunc;

lambdafunc
    : LPAREN paramlist RPAREN                  ARROW lambdafunc
    // This paramlist must not have optional parameters
    | LPAREN paramlist RPAREN COLON resultlist ARROW curlyblock
    | ternary
    ;

ternary
    : binor QUEST expression COLON expression
    | binor
    ;

binor // lassoc
    : binor OR binand
    | binand
    ;

binand // lassoc
    : binand AND bincmp
    | bincmp
    ;

cmpops: EQ | NE | LT | LE | GT | GE;

bincmp // nassoc
    : binadd cmpops binadd
    | binadd
    ;

binadd // lassoc
    : binadd ADD binmul
    | binadd SUB binmul
    | binmul
    ;

mulops: MUL DIV MOD ATS VID;

binmul // lassoc
    : binmul mulops prefix
    | prefix
    ;

prefix
    : ADD prefix
    | SUB prefix
    | NOT prefix
    | TILDE prefix
    | binpow
    ;

binpow // rassoc
    : suffix POW prefix
    | suffix
    ;

suffix
    : suffix TICK
    | suffix DOT IDENT    // AttrReference
    | suffix DOT LIT_I64  // LaneReference
    | suffix LPAREN arguments  RPAREN // FunctionCall
    | suffix LBRACE subscripts RBRACE // ItemReference
    //
    //     var x = new matrix[f64] {
    //         { 1, 2, 3 },
    //         { 4, 5, 6 },
    //     };
    //
    | NEW typespec LCURLY initlist  RCURLY // TypeInitializer
    | NEW typespec LPAREN arguments RPAREN // TypeConstructor
    | primary
    ;

arguments
    : argument COMMA arguments
    | argument
    | // zero or more
    ;

argument // Expression, NamedArgument
    : expression
    | IDENT EQUAL expression
    ;

subscripts
    : subscript COMMA subscripts
    | subscript
    | // zero or more
    ;

subscript
    : expression
    ;

initlist
    : initializer COMMA initlist
    | initializer
    | // zero or more
    ;

initializer
    : LCURLY initlist RCURLY
    | expression
    ;

primary
    : LPAREN expression RPAREN
    | IDENT
    | LIT_I64
    | LIT_F64
    | LIT_BOOL
    | LIT_STR
    | LIT_SYM
    ;

