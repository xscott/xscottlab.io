//
// Copyright 2022 Scott Eric Gilbert.  All Rights Reserved.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// This Source Code Form is "Incompatible With Secondary Licenses", as
// defined by the Mozilla Public License, v. 2.0.
//

/**
 * Iteratively approximate the zero (root) of the one-dimensional real-valued
 * function in the bracketed range between xlo and xhi within the tolerance
 * given by tol using the algorithm specified by alg.  The algorithm is one of
 * "bisect", "illinois", or "ridders":
 *
 *     bisect:     https://en.wikipedia.org/wiki/Bisection_method
 *     illinois:   https://en.wikipedia.org/wiki/Regula_falsi
 *     ridders:    https://en.wikipedia.org/wiki/Ridders%27_method
 *
**/
export function findzero(func, xlo, xhi, opts={}) {
    let {tol=0.0, alg="ridders"} = opts;

    if (xhi < xlo) [xlo, xhi] = [xhi, xlo];
    let flo = func(xlo);
    if (flo === 0.0) return xlo;
    let fhi = func(xhi);
    if (fhi === 0.0) return xhi;
    if (sign(flo) === sign(fhi)) {
        throw "roots must be bracketed";
    }
    if (alg === 'bisect')   return bisect(func, xlo, xhi, flo, fhi, tol);
    if (alg === 'illinois') return illinois(func, xlo, xhi, flo, fhi, tol);
    if (alg === 'ridders')  return ridders(func, xlo, xhi, flo, fhi, tol);
    throw "unsupported algorithm";
}

const [sign, abs, sqrt, hypot] = [
    Math.sign, Math.abs, Math.sqrt, Math.hypot
];

function bisect(func, xlo, xhi, flo, fhi, tol) {
    while (xhi - xlo > tol) {
        let xmid = 0.5*(xlo + xhi);
        if (xmid === xlo || xmid === xhi) break;
        let fmid = func(xmid);
        if (fmid === 0.0) return xmid;
        if (sign(fmid) === sign(flo)) {
            [xlo, flo] = [xmid, fmid];
        } else {
            [xhi, fhi] = [xmid, fmid];
        }
    }
    return 0.5*(xlo + xhi);
}

function illinois(func, xlo, xhi, flo, fhi, tol) {
    let [slo, shi, side] = [1, 1, 0];
    while (xhi - xlo > tol) {
        let xpos = (xhi*slo*flo - xlo*shi*fhi)/(slo*flo - shi*fhi);
        if (xpos === xlo || xpos === xhi) xpos = 0.5*(xlo + xhi);
        if (xpos === xlo || xpos === xhi) break;
        let fpos = func(xpos);
        if (fpos === 0.0) return xpos;
        if (sign(fpos) === sign(flo)) {
            [xlo, flo] = [xpos, fpos];
            if (side === -1) {
                slo *= 8.0;
            } else {
                side = -1;
                slo = shi = 1.0;
            }
        } else {
            [xhi, fhi] = [xpos, fpos];
            if (side === +1) {
                shi *= 8.0;
            } else {
                side = +1;
                slo = shi = 1.0;
            }
        }
    }
    return 0.5*(xlo + xhi);
}

function ridders(func, xlo, xhi, flo, fhi, tol) {
    while (xhi - xlo > tol) {
        let xmid = .5*(xhi + xlo);
        if (xmid === xlo || xmid === xhi) break;
        let fmid = func(xmid);
        if (fmid === 0.0) return xmid;
        let rtlo = sqrt(abs(flo));
        let rthi = sqrt(abs(fhi));
        let denom = hypot(fmid, rtlo*rthi);
        let half = .5*(xhi - xlo);
        let xexp = xmid + half*sign(flo - fhi)*fmid/denom;
        let fexp = func(xexp);
        if (fexp === 0.0) { return xexp }
        let xs, ys;
        if (xmid < xexp) {
            xs = [xlo, xmid, xexp, xhi];
            ys = [flo, fmid, fexp, fhi];
        } else {
            xs = [xlo, xexp, xmid, xhi];
            ys = [flo, fexp, fmid, fhi];
        }
        for (let ii = 0; ii<3; ii++) {
            if (sign(ys[ii]) != sign(ys[ii + 1])) {
                if (xs[ii + 1] - xs[ii] < xhi - xlo) {
                    [xlo, xhi] = [xs[ii], xs[ii + 1]];
                    [flo, fhi] = [ys[ii], ys[ii + 1]];
                }
            }
        }
    }
    return 0.5*(xlo + xhi);
}

