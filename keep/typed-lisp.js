
//
// Copyright (C) 2022-2023, Scott Eric Gilbert.
//
// I've licensed this compiler under the Mozilla Public License version 2.0
// with the "Incompatible With Secondary Licenses" notice.  I want people to be
// able to use this software freely for almost any purpose, including using it
// commercially.  However, I do not want people to make additions or changes
// which use a more restrictive license such as the GPL.  In other words, I
// chose the MPL with the no secondary licenses clause because I think it's a
// good way to enforce a "Share Alike" constraint.  However, I wish there was a
// common software license which stated this intent and mechanism more clearly.
//

//
// LICENSE
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
// This Source Code Form is “Incompatible With Secondary Licenses”, as
// defined by the Mozilla Public License, v. 2.0.
//

(function() { "use strict";

const source = document.currentScript.textContent;

window.addEventListener("load", function() {
    try {
        const before = Date.now();
        let program = parse(source);
        // program = expand(program);
        // program = register(program);
        // program = analyze(program);
        // program = optimize(program);
        // program = assemble(program);
        const after = Date.now();
        console.log("compilation took:", after - before, "milliseconds");
        debug(program);

    } catch (err) {
        console.log(err);
        const pre = document.createElement("pre");
        pre.textContent = JSON.stringify(
            err, (kk, vv) => typeof vv == "bigint" ? String(vv) + "n" : vv, 4
        );
        pre.style.cssText = "padding:8px;border:2px solid red;";
        document.body.appendChild(pre);
    }
});

function debug(data) {
    let output = "";
    recur(data, 0);
    const pre = document.createElement("pre");
    pre.textContent = output;
    pre.style.cssText = "padding:8px;border:2px solid blue;";
    document.body.appendChild(pre);

    function recur(data, indent) {
        if (typeof(data) === 'object') {
            if (data instanceof Uint8Array) {
                output += "Uint8Array( ";
                for (let ii = 0; ii<data.length; ++ii) {
                    output += data[ii].toString(16).padStart(2, '0') + " ";
                }
                output += ")";
                return;
            }
            output += data.constructor.name + "( ";
            let nested = false;
            for (let key in data) {
                const val = data[key];
                if (Number(key) == key || typeof(val) === 'object') {
                    nested = true;
                    continue;
                }
                output += key + ":";
                recur(val, indent+1);
                output += " ";
            }
            if (nested) {
                for (let key in data) {
                    const val = data[key];
                    if (Number(key) == key || typeof(val) === 'object') {
                        output += "\n" + "    ".repeat(indent+1) + key + ": ";
                        recur(val, indent+1);
                    }
                }
                output += "\n" + "    ".repeat(indent) + ")";
            } else {
                output += ")";
            }
            return;
        }
        if (typeof(data) === 'string') {
            output += '"' + data + '"';
            return;
        }
        if (typeof(data) === 'bigint') {
            output += String(data) + 'n';
            return;
        }
        output += String(data);
    }
}

function isalnum(chr) {
    if (isalpha(chr)) return true;
    if (isdigit(chr)) return true;
    return false;
}

function isalpha(chr) {
    if ("a" <= chr && chr <= "z") return true;
    if ("A" <= chr && chr <= "Z") return true;
    if (chr === "_") return true;
    return false;
}

function isdigit(chr) {
    if ("0" <= chr && chr <= "9") return true;
    return false;
}

class Buffer { // a growable array of bytes
    constructor() {
        this.buffer = new Uint8Array(16);
        this.length = 0;
        this.encoder = null;
        this.f32array = this.f32bytes = null;
        this.f64array = this.f64bytes = null;
    }

    push_byte(val) {
        console.assert(typeof val == 'number', "must be byte");
        console.assert(0 <= val && val <= 255, "byte in range");
        this.ensure(1);
        this.buffer[this.length] = val;
        this.length += 1;
    }

    push_bytes(val) {
        console.assert(val instanceof Uint8Array, "must be buffer");
        this.ensure(val.length);
        this.buffer.set(val, this.length);
        this.length += val.length;
    }

    push_utf8(val) {
        console.assert(typeof val == 'number', "must be byte");
        console.assert(0 <= val && val <= 0x10FFFF, "utf8 in range");
        this.push_string(String.fromCodePoint(val));
    }

    push_string(val) {
        console.assert(typeof val == 'string', "must be string");
        if (!this.encoder) this.encoder = new TextEncoder();
        const bytes = this.encoder.encode(val);
        this.push_bytes(bytes);
    }

    push_float(val) {
        console.assert(typeof val == 'number', "must be number");
        if (!this.f32array) {
            this.f32array = new Float32Array(1);
            this.f32bytes = new Uint8Array(this.f32array.buffer);
        }
        this.f32array[0] = val;
        this.push_bytes(this.f32bytes);
    }

    push_double(val) {
        console.assert(typeof val == 'number', "must be number");
        if (!this.f64array) {
            this.f64array = new Float64Array(1);
            this.f64bytes = new Uint8Array(this.f64array.buffer);
        }
        this.f64array[0] = val;
        this.push_bytes(this.f64bytes);
    }

    push_leb128(val) {
        let big = BigInt(val);
        for (;;) {
            let chunk = Number(big & 0x7Fn);
            big >>= 7n;
            if (big == 0 || big == -1) {
                this.push_byte(chunk);
                break;
            } else {
                this.push_byte(chunk | 0x80);
            }
        }
    }

    grab_bytes() { return this.buffer.slice(0, this.length); }

    ensure(needed) {
        needed += this.length;
        let len = this.buffer.length;
        while (len < needed) len *= 2;
        if (len > this.buffer.length) {
            let old = this.buffer;
            this.buffer = new Uint8Array(len);
            this.buffer.set(old);
        }
    }
}

class Scanner {
    constructor(text) {
        this.text = text;
        this.spot = 0;
        this.line = 1;
        this.col = 1;
    }

    finish() {
        this.white();
        if (this.spot != this.text.length) {
            throw {
                line: this.line, col: this.col,
                error: "unexpected characters"
            };
        }
    }

    where() { return { line: this.line, col: this.col }; }

    white() {
        for (;;) {
            let chr = this.text[this.spot];
            if (chr === "#") {
                while (chr !== undefined && chr !== "\n") {
                    ++this.spot;
                    chr = this.text[this.spot];
                }
                continue;
            }
            if (chr === " " || chr === "\t" || chr == "\r") {
                ++this.spot;
                ++this.col;
                continue;
            }
            if (chr === "\n") {
                ++this.spot;
                ++this.line;
                this.col = 1;
                continue;
            }
            break;
        }
    }

    match(pattern) {
        this.white();

        for (let ii = 0;; ++ii) {
            if (pattern[ii] === undefined) {
                const next = this.text[this.spot + ii]
                // special handling for keywords
                if (isalpha(pattern[0]) && isalnum(next)) return null;
                // special handling for assignments
                if (next === "=") return null;
                // special handling for ellipsis
                if (pattern === "." && next === ".") return null;
                // pattern matched, return where
                let where = { line: this.line, col: this.col };
                this.spot += ii;
                this.col += ii;
                return where;
            }
            if (this.text[this.spot + ii] !== pattern[ii]) {
                return null;
            }
        }
    }

    basic() {
        const chr = this.text[this.spot];
        if (isalpha(chr)) return this.#ident();
        if (isdigit(chr)) return this.#number();
        if (chr === '"') return this.#string();
        return null;
    }


    #ident() {
        let pat = /[a-zA-Z_][a-zA-Z0-9_]*/y;
        pat.lastIndex = this.spot;
        const match = pat.exec(this.text);
        if (!match) return null;
        const name = match[0];
        const result = new Ident(this.line, this.col, name);
        this.col += pat.lastIndex - this.spot;
        this.spot = pat.lastIndex;
        return result;
    }

    #number() {
        // XXX: report 64 bit overflow issues

        const bin = try_regex(this, /0b[01_]+/y);
        if (bin) return new IntLit(bin, BigInt(bin.txt.replaceAll('_', '')));

        const hex = try_regex(this, /0x[0-9a-fA-F_]+/y);
        if (hex) return new IntLit(hex, BigInt(hex.txt.replaceAll('_', '')));

        const flt = try_regex(this,
            /[0-9][0-9_]*\.[0-9_]+(?:[eE][-+]?[0-9_]+)?j?/y
        );
        if (flt) {
            let val = parseFloat(flt.txt.replaceAll('_', ''));
            if (flt.txt[flt.txt.length - 1] === 'j') {
                return new ImagLit(flt, val);
            } else {
                return new FltLit(flt, val);
            }
        }

        const dec = try_regex(this, /[0-9][0-9_]*/y);
        if (dec) return new IntLit(dec, BigInt(dec.txt.replaceAll('_', '')));

        throw "internal error, we shouldn't get here";

        function try_regex(that, pat) {
            console.assert(pat.sticky, "regex pattern must have /y");
            pat.lastIndex = that.spot;
            const match = pat.exec(that.text);
            if (!match) return null;
            const txt = match[0];
            const result = { line:that.line, col:that.col, txt };
            that.col += pat.lastIndex - that.spot;
            that.spot = pat.lastIndex;
            const after = that.text[that.spot];
            if (isalnum(after)) {
                throw {
                    line: that.line, col: that.col,
                    error: "unexpected '" + after + "' after '" + txt + "'"
                };
            }
            return result;
        }

    }

    #string() {
        const BACK_SLASH = 0x5C, DOUBLE_QUOTE = 0x22, NEWLINE = 0x0A;
        let buf = new Buffer();
        let ii = 1;
        for (;;) {
            const chr = this.text.codePointAt(this.spot + ii);
            if (chr == NEWLINE) error(
                { line: this.line, col: this.col + ii + 1 },
                "unexpected newline in string literal"
            );
            if (chr == DOUBLE_QUOTE) break;
            if (chr === BACK_SLASH) {
                const esc = this.text[this.spot + ii + 1];
                if (esc === 'n') { buf.push_string("\n"); ii += 2; continue; }
                if (esc === 't') { buf.push_string("\t"); ii += 2; continue; }
                if (esc === '0') { buf.push_string("\0"); ii += 2; continue; }
                if (esc === '\\') { buf.push_string("\\"); ii += 2; continue; }
                if (esc === 'x') {
                    const val = hexnum(this.text, this.spot + ii + 2, 2);
                    buf.push_byte(val);
                    ii += 4;
                    continue;
                }
                if (esc === 'u') {
                    const val = hexnum(this.text, this.spot + ii + 2, 4);
                    buf.push_byte(val);
                    ii += 4;
                    continue;
                }
                if (esc === 'U') {
                    const val = hexnum(this.text, this.spot + ii + 2, 6);
                    buf.push_byte(val);
                    ii += 8;
                    continue;
                }
                error(
                    { line: this.line, col: this.col + ii + 1 },
                    "unexpected character as escape sequence"
                );
            }
            buf.push_utf8(chr);
            ii += chr > 0xFFFF ? 2 : 1;
        }
        const result = {
            line: this.line, col: this.col, buf: buf.grab_bytes(),
            str: this.text.slice(this.spot + 1, this.spot + ii),
        };
        this.spot += ii + 1;
        this.col += ii + 1;
        return new StrLit(result);
    }
}

function parse(text) {
    const scan = new Scanner(text);
    const result = tuple();
    scan.finish();
    return result;

    function tuple() {
        const start = scan.where();
        const list = [];
        for (;;) {
            const expr = equal();
            if (!expr) break;
            list.push(expr);
        }
        return new Tuple(start, list);
    }
    
    function equal() { return rassoc("=", colon, Equal); }
    function colon() { return rassoc(":", basic, Colon); }

    function rassoc(op, next, ctor) {
        const lhs = next();
        if (lhs === null) return null;
        const where = scan.match(op);
        if (where) {
            const rhs = rassoc(op, next, ctor);
            if (rhs === null) error(
                where, "expected expression after " + op
            );
            return new ctor(where, lhs, rhs);
        }
        return lhs;
    }

    function basic() {
        const open = scan.match("(");
        if (open) {
            const result = tuple();
            if (!scan.match(")")) error(open, "need closing paren");
            return result;
        }
        return scan.basic();
    }
}

class Tuple {
    constructor(where, list) {
        Object.assign(this, where);
        Object.assign(this, list);
    }
}

class Equal {
    constructor(where, lhs, rhs) {
        Object.assign(this, where);
        Object.assign(this, { lhs, rhs });
    }
}

class Colon {
    constructor(where, lhs, rhs) {
        Object.assign(this, where);
        Object.assign(this, { lhs, rhs });
    }
}

class Ident {
    constructor(line, col, name) {
        Object.assign(this, { line, col, name });
    }
}

class IntLit {
    constructor(match, val) {
        Object.assign(this, match);
        this.val = val;
    }
}

class FltLit {
    constructor(match, val) {
        Object.assign(this, match);
        this.val = val;
    }
}

class ImagLit {
    constructor(match, val) {
        Object.assign(this, match);
        this.val = val;
    }
}

class StrLit {
    constructor(match) {
        Object.assign(this, match);
    }
}

function error(where, msg) {
    throw { line: where.line, col: where.col, error: msg };
}

})();

