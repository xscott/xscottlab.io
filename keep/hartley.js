
function hartley(size) {
    if (hartley.cache === undefined) hartley.cache = {};
    let result = hartley.cache[size];
    if (result !== undefined) return result;
    
    if (size < 8) throw "must be larger than size 8";
    const log2 = Math.round(Math.log2(size));
    if (2**log2 != size) throw "must be power of 2 size";

    const perm = new Uint32Array(size);
    for (let ii = 0; ii<size; ++ii) {
        let fwd = ii>>3, rev = ii%8;
        for (let jj = 0; jj<log2-3; ++jj) {
            rev = rev*2 + fwd%2;
            fwd = fwd/2|0;
        }
        perm[ii] = rev;
    }

    const cos = new Float64Array(size/4);
    const sin = new Float64Array(size/4);
    for (let ii = 0, nn = size/4|0; ii<nn; ++ii) {
        const angle = 2*Math.PI*ii/size;
        cos[ii] = Math.cos(angle);
        sin[ii] = Math.sin(angle);
    }

    return hartley.cache[size] = (dst, src) => {
        for (let ii = 0; ii<size; ++ii) {
            dst[ii] = src[perm[ii]];
        }

        first8(dst, size);
        let reps = size/8, cols = 8;
        for (let ii = 0; ii<log2-3; ++ii) {
            reps /= 2;
            radix2(dst, cols, reps);
            cols *= 2;
        }
    };

    function first8(data, size) {
        const rt2 = 1.4142135623730951455;
        for (let ii = 0; ii < size; ii += 8) {
            const aa = data[ii + 0], bb = data[ii + 1];
            const cc = data[ii + 2], dd = data[ii + 3];
            const ee = data[ii + 4], ff = data[ii + 5];
            const gg = data[ii + 6], hh = data[ii + 7];
            const apb = aa + bb,  amb = aa - bb;
            const apc = aa + cc,  amc = aa - cc;
            const cpd = cc + dd,  cmd = cc - dd;
            const epf = ee + ff,  emf = ee - ff;
            const gph = gg + hh,  gmh = gg - hh;
            const epg = ee + gg,  emg = ee - gg;
            const bmf = bb - ff,  dmh = dd - hh;
            data[ii + 0] = (apb + cpd) + (epf + gph);
            data[ii + 1] = (apc - epg) + rt2*bmf;
            data[ii + 2] = (apb - cpd) + (epf - gph);
            data[ii + 3] = (amc - emg) + rt2*dmh;
            data[ii + 4] = (amb + cmd) + (emf + gmh);
            data[ii + 5] = (apc - epg) - rt2*bmf;
            data[ii + 6] = (amb - cmd) + (emf - gmh);
            data[ii + 7] = (amc - emg) - rt2*dmh;
        }
    }

    function radix2(data, cols, reps) {
        const half = cols>>1;
        for (let rr = 0; rr<reps; ++rr) {
            const row0 = (2*rr + 0)*cols;
            const row1 = (2*rr + 1)*cols;
            const row2 = (2*rr + 2)*cols;

            for (let jj = 1; jj < half; ++jj) {
                const ll = data[row1 + jj], cc = cos[jj*reps];
                const rr = data[row2 - jj], ss = sin[jj*reps];
                data[row1 + jj] = cc*ll + ss*rr;
                data[row2 - jj] = ss*ll - cc*rr;
            }

            for (let jj = 0; jj < cols; ++jj) {
                const ll = data[row0 + jj];
                const rr = data[row1 + jj];
                data[row0 + jj] = ll + rr;
                data[row1 + jj] = ll - rr;
            }
        }
    }
}
