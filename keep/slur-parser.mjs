
import { kindof, error } from "./basics.mjs";
import { debug } from "./debug.mjs";
import {
    LiteralF64, LiteralI64, LiteralStr, Identifier, Delimiter
} from "./lexer.mjs";

export function parsify(tokens) {

    const result = [];

    for (;;) {
        const item = equal();
        if (item === null) break;
        result.push(item);
    }

    if (tokens.length > 0) {
        error(
            tokens[0].row, tokens[0].col, "unexpected token"
        );
    }

    return result;

    function equal() {
        const lhs = colon();
        if (lhs === null) return null;
        const ee = match("=");
        if (ee === null) return lhs;
        const rhs = colon();
        if (rhs === null) error(
            ee.row, ee.col, "expected rhs for equal"
        );
        return new EqualPair(ee.row, ee.col, lhs, rhs);
    }

    function colon() {
        const lhs = parts();
        if (lhs === null) return null;
        const cc = match(":");
        if (cc === null) return lhs;
        const rhs = parts();
        if (!rhs) error(
            cc.row, cc.col, "expected rhs for colon"
        );
        return new ColonPair(cc.row, cc.col, lhs, rhs);
    }

    function parts() {
        let base = basic();
        for (;;) {
            const bb = match("[");
            if (bb !== null) {
                const list = comma();
                base = new RefItem(bb.row, bb.col, base, list);
                if (match("]") === null) error(
                    bb.row, bb.col, "expected closing brace"
                );
                continue;
            }
            const dd = match(".");
            if (dd !== null) {
                const kind = kindof(tokens[0]);
                if (kind === LiteralI64) {
                    base = new RefLane(dd.row, dd.col, base, tokens.shift());
                    continue;
                }
                if (kind === Identifier) {
                    base = new RefAttr(dd.row, dd.col, base, tokens.shift());
                    continue;
                }
                error(dd.row, dd.col, "expected lane or attr");
            }
            break;
        }
        return base;
    }

    function comma() {
        const list = [];
        for (;;) {
            const item = basic();
            if (item === null) break;
            list.push(item);
            if (match(",") === null) break;
        }
        return list;
    }

    function basic() {
        const kind = kindof(tokens[0]);
        if (kind === LiteralF64) return tokens.shift();
        if (kind === LiteralI64) return tokens.shift();
        if (kind === LiteralStr) return tokens.shift();
        if (kind === Identifier) return tokens.shift();
        return tuple();
    }

    function tuple() {
        const pp = match("(");
        if (pp === null) return null;
        const list = [];
        for (;;) {
            const item = equal();
            if (item === null) break;
            list.push(item);
        }
        if (match(")") === null) error(
            pp.row, pp.col, "expected closing paren"
        );
        return new TupleList(pp.row, pp.col, list);
    }

    function match(...patterns) {
        if (kindof(tokens[0]) === Delimiter) {
            if (patterns.includes(tokens[0].text)) {
                return tokens.shift();
            }
        }
        return null;
    }
}

export class TupleList {
    constructor(row, col, list) {
        this.row = row;
        this.col = col;
        this.list = list;
    }
}

export class EqualPair {
    constructor(row, col, lhs, rhs) {
        this.row = row;
        this.col = col;
        this.lhs = lhs;
        this.rhs = rhs;
    }
}

export class ColonPair {
    constructor(row, col, lhs, rhs) {
        this.row = row;
        this.col = col;
        this.lhs = lhs;
        this.rhs = rhs;
    }
}

export class RefItem {
    constructor(row, col, base, list) {
        this.row = row;
        this.col = col;
        this.base = base;
        this.list = list;
    }
}

export class RefLane {
    constructor(row, col, base, lane) {
        this.row = row;
        this.col = col;
        this.base = base;
        this.lane = lane;
    }
}

export class RefAttr {
    constructor(row, col, base, attr) {
        this.row = row;
        this.col = col;
        this.base = base;
        this.attr = attr;
    }
}

