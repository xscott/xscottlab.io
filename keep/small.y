
%token
    LPAREN RPAREN LBRACE RBRACE COLON EQUAL
    DOT COMMA IDENT STRING INTEGER FLOATING

%%

start: /* zero or more */ | start equal;

equal: colon | colon EQUAL colon;

colon: parts | parts COLON parts;

parts
    : parts LBRACE comma RBRACE
    | parts DOT IDENT
    | parts DOT INTEGER
    | basic
    ;

basic: FLOATING | INTEGER | STRING | IDENT | tuple;

tuple: LPAREN elems RPAREN;

elems: /* zero or more */ | elems equal;

comma: /* zero or more */ | basic | basic COMMA comma;


%%

