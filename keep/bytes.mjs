export { Bytes };

class Bytes { // a growable array of bytes
    constructor() {
        this.buffer = new Uint8Array(16);
        this.length = 0;
        this.encoder = null;
        this.f32array = this.f32bytes = null;
        this.f64array = this.f64bytes = null;
    }

    push_byte(val) {
        console.assert(typeof val === 'number', "must be byte:" + val);
        console.assert(0 <= val && val <= 255, "byte in range:" + val);
        this.ensure(1);
        this.buffer[this.length] = val;
        this.length += 1;
    }

    push_bytes(val) {
        console.assert(val instanceof Uint8Array, "must be buffer:" + val);
        this.ensure(val.length);
        this.buffer.set(val, this.length);
        this.length += val.length;
    }

    push_codes(val) {
        console.assert(typeof val === 'string', "must be string:" + val);
        this.ensure(val.length);
        for (let ii = 0; ii<val.length; ++ii) {
            // reading the two byte UTF-16 code, but it must be [0, 255]
            const num = val.charCodeAt(ii);
            this.push_byte(num);
        }
    }

    push_zeros(count) {
        console.assert(typeof val === 'number', "must be integer:" + count);
        this.ensure(count);
        this.buffer.fill(0, this.length, this.length + count);
        this.length += count;
    }

    push_utf8(val) {
        console.assert(typeof val === 'number', "must be byte:" + val);
        console.assert(0 <= val && val <= 0x10FFFF, "utf8 in range");
        this.push_string(String.fromCodePoint(val));
    }

    push_string(val) {
        console.assert(typeof val === 'string', "must be string:" + val);
        // convert the UTF-16 shorts to UTF-8 bytes
        if (!this.encoder) this.encoder = new TextEncoder();
        const bytes = this.encoder.encode(val);
        this.push_bytes(bytes);
    }

    push_float(val) {
        console.assert(typeof val === 'number', "must be number:" + val);
        if (!this.f32array) {
            this.f32array = new Float32Array(1);
            this.f32bytes = new Uint8Array(this.f32array.buffer);
        }
        this.f32array[0] = val;
        this.push_bytes(this.f32bytes);
    }

    push_double(val) {
        console.assert(typeof val === 'number', "must be number:" + val);
        if (!this.f64array) {
            this.f64array = new Float64Array(1);
            this.f64bytes = new Uint8Array(this.f64array.buffer);
        }
        this.f64array[0] = val;
        this.push_bytes(this.f64bytes);
    }

    push_leb128(val) {
        let big = BigInt(val);
        for (;;) {
            let chunk = Number(big & 0x7Fn);
            big >>= 7n;
            if ((big ===  0n) && ((chunk&0x40) === 0) ||
                (big === -1n) && ((chunk&0x40) !== 0)
            ){
                this.push_byte(chunk);
                break;
            } else {
                this.push_byte(chunk | 0x80);
            }
        }
    }

    grab_bytes() { return this.buffer.slice(0, this.length); }

    ensure(needed) {
        needed += this.length;
        let len = this.buffer.length;
        while (len < needed) len *= 2;
        if (len > this.buffer.length) {
            let old = this.buffer;
            this.buffer = new Uint8Array(len);
            this.buffer.set(old);
        }
    }
}

