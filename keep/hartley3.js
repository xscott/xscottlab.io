
function hartley(size) {
    const log3 = Math.round(Math.log(size)/Math.log(3));
    if (3**log3 != size) throw "must be power of 3";
    const third = size/3;

    const twid = new Float64Array(size);
    const perm = new Int32Array(size);
    for (let ii = 0; ii<size; ++ii) {
        const aa = 2*Math.PI*ii/size;
        twid[ii] = 0.5*Math.cos(aa) + 0.5*Math.sin(aa);
        let fwd = ii, rev = 0;
        for (let jj = 0; jj<log3; ++jj) {
            rev *= 3;
            rev += fwd%3;
            fwd /= 3;
            fwd |= 0;
        }
        perm[ii] = rev;
    }

    return (dst, src) => {
        for (let ii = 0; ii<size; ++ii) {
            dst[ii] = src[perm[ii]];
        }
        let reps = third, cols = 1;
        for (let ii = 0; ii<log3; ++ii) {
            radix3(dst, cols, reps);
            reps /= 3;
            cols *= 3;
        }
    };

    function radix3(data, cols, reps) {
        for (let rr = 0; rr<reps; ++rr) {
            const row0 = (3*rr + 0)*cols;
            const row1 = (3*rr + 1)*cols;
            const row2 = (3*rr + 2)*cols;
            const row3 = (3*rr + 3)*cols;

            for (let jj = 1; jj <= cols>>1; ++jj) {
                const t0p1 = twid[0*third + 1*reps*jj];
                const t0p2 = twid[0*third + 2*reps*jj];
                const t1m1 = twid[1*third - 1*reps*jj];
                const t1p2 = twid[1*third + 2*reps*jj];
                const t2p1 = twid[2*third + 1*reps*jj];
                const t2m2 = twid[2*third - 2*reps*jj];
                const t3m1 = twid[3*third - 1*reps*jj];
                const t3m2 = twid[3*third - 2*reps*jj];
                const aa = data[row1 + jj], bb = data[row2 - jj];
                const cc = data[row2 + jj], dd = data[row3 - jj];
                data[row1 + jj] =  aa*t3m1 + bb*t0p1 + cc*t0p2 - dd*t3m2;
                data[row2 - jj] =  aa*t1m1 + bb*t2p1 - cc*t1p2 + dd*t2m2;
                data[row2 + jj] =  aa*t0p1 - bb*t3m1 + cc*t3m2 + dd*t0p2;
                data[row3 - jj] = -aa*t2p1 + bb*t1m1 + cc*t2m2 + dd*t1p2;
            }

            for (let jj = 0; jj<cols; ++jj) {
                const aa = data[row0 + jj];
                const bb = data[row1 + jj];
                const cc = data[row2 + jj];
                data[row0 + jj] = aa + bb + cc;
                const mm = 0.366025403784438929655209449;
                data[row1 + jj] = aa + mm*(bb - cc) - cc;
                data[row2 + jj] = aa + mm*(cc - bb) - bb;
            }
        }
    }
}

