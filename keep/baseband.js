    function baseband() {
        let input;
        while (input = resample_to_baseband.pull(
            baseband_offset, 2*framelen + 2*79
        )) {
            let jj;
            for (let ii = 0; ii<framelen; ++ii) {
                let jj = 2*ii + 79;

		baseband_real[ii] = input[jj];
                baseband_imag[ii] = (
                    +2.475514712670565385e-9*(input[jj - 79] - input[jj + 79])
                    +7.549062534827286971e-8*(input[jj - 77] - input[jj + 77])
                    +4.748834393406405555e-7*(input[jj - 75] - input[jj + 75])
                    +1.698370080874456069e-6*(input[jj - 73] - input[jj + 73])
                    +4.532955692990659503e-6*(input[jj - 71] - input[jj + 71])
                    +1.011387509193243612e-5*(input[jj - 69] - input[jj + 69])
                    +1.999463420678874835e-5*(input[jj - 67] - input[jj + 67])
                    +3.622702631446763093e-5*(input[jj - 65] - input[jj + 65])
                    +6.144940392142080683e-5*(input[jj - 63] - input[jj + 63])
                    +9.898102262497561650e-5*(input[jj - 61] - input[jj + 61])
                    +1.529199929598185125e-4*(input[jj - 59] - input[jj + 59])
                    +2.282423362290247394e-4*(input[jj - 57] - input[jj + 57])
                    +3.308998978405287622e-4*(input[jj - 55] - input[jj + 55])
                    +4.679154870248692266e-4*(input[jj - 53] - input[jj + 53])
                    +6.474746527057936981e-4*(input[jj - 51] - input[jj + 51])
                    +8.790150545675241367e-4*(input[jj - 49] - input[jj + 49])
                    +1.173316556930919770e-3*(input[jj - 47] - input[jj + 47])
                    +1.542598121062358982e-3*(input[jj - 45] - input[jj + 45])
                    +2.000631542668325751e-3*(input[jj - 43] - input[jj + 43])
                    +2.562887458828798978e-3*(input[jj - 41] - input[jj + 41])
                    +3.246736448319686563e-3*(input[jj - 39] - input[jj + 39])
                    +4.071738477045227199e-3*(input[jj - 37] - input[jj + 37])
                    +5.060069065219673784e-3*(input[jj - 35] - input[jj + 35])
                    +6.237153193895236293e-3*(input[jj - 33] - input[jj + 33])
                    +7.632612986772789772e-3*(input[jj - 31] - input[jj + 31])
                    +9.281691193244068216e-3*(input[jj - 29] - input[jj + 29])
                    +1.122740510328064031e-2*(input[jj - 27] - input[jj + 27])
                    +1.352384419715173779e-2*(input[jj - 25] - input[jj + 25])
                    +1.624130723811793159e-2*(input[jj - 23] - input[jj + 23])
                    +1.947449876191611334e-2*(input[jj - 21] - input[jj + 21])
                    +2.335602588683596825e-2*(input[jj - 19] - input[jj + 19])
                    +2.807953879986748585e-2*(input[jj - 17] - input[jj + 17])
                    +3.394148450331848504e-2*(input[jj - 15] - input[jj + 15])
                    +4.142147660071571202e-2*(input[jj - 13] - input[jj + 13])
                    +5.135037348939792273e-2*(input[jj - 11] - input[jj + 11])
                    +6.530242900036109521e-2*(input[jj -  9] - input[jj +  9])
                    +8.665956810664812715e-2*(input[jj -  7] - input[jj +  7])
                    +1.242296135783214905e-1*(input[jj -  5] - input[jj +  5])
                    +2.103367116559932137e-1*(input[jj -  3] - input[jj +  3])
                    +6.359942525516816270e-1*(input[jj -  1] - input[jj +  1])
                );
            }
            callback(baseband_real, baseband_imag);
            baseband_offset += 2*framelen;
        }
    }

