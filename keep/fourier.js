

//example();

function example() {
    const len = 100*1000;
    const src_real = new Float64Array(len);
    const src_imag = new Float64Array(len);
    const dst_real = new Float64Array(len);
    const dst_imag = new Float64Array(len);

    for (let ii = 0; ii<len; ++ii) {
        src_real[ii] = normal()/len;
        src_imag[ii] = normal()/len;
    }

    const fft = fourier(len);
    fft(dst_real, dst_imag, src_real, src_imag);
    
    for (let ii = 0; ii<len; ++ii) {
        console.log(
            dst_real[ii].toFixed(18),
            dst_imag[ii].toFixed(18), 
        );
    }

    function normal() {
        let sum = 0.0;
        for (let ii = 0; ii<12; ++ii) {
            sum += Math.random();
        }
        return sum - 6.0;
    }
}

function fourier(size) {
    if (size < 1) throw "size must be non-negative";
    if ((size|0) !== size) throw "size must be integer";
    if (size & (size - 1) === 0) {
        // It's a power of 2, do it the faster way
        return fourstep(size);
    }
    // Bluestein's algorithm works with any size
    return bluestein(size);
}

function bluestein(size) {
    if (bluestein.cache === undefined) bluestein.cache = {};
    const result = bluestein.cache[size];
    if (result !== undefined) return result;

    let over = 4;
    while (over < 2*size - 2) over *= 2;
    const fft = fourstep(over);

    const chirp_real = new Float64Array(size);
    const chirp_imag = new Float64Array(size);
    const cdata_real = new Float64Array(over);
    const cdata_imag = new Float64Array(over);
    const tdata_real = new Float64Array(over);
    const tdata_imag = new Float64Array(over);
    const fdata_real = new Float64Array(over);
    const fdata_imag = new Float64Array(over);

    const halfu = -Math.PI/size;
    for (let ii = 0; ii<size; ++ii) {
        chirp_real[ii] = Math.cos(halfu*(ii*ii%(2*size)));
        chirp_imag[ii] = Math.sin(halfu*(ii*ii%(2*size)));
    }
    tdata_real[0] = 1.0;
    tdata_imag[0] = 0.0;
    for (let ii = 1; ii<size; ++ii) {
        tdata_real[ii] = tdata_real[over - ii] = +chirp_real[ii];
        tdata_imag[ii] = tdata_imag[over - ii] = -chirp_imag[ii];
    }
    fft(cdata_real, cdata_imag, tdata_real, tdata_imag);
    const scale = 1.0/Math.sqrt(over);
    for (let ii = 0; ii<size; ++ii) {
        chirp_real[ii] *= scale;
        chirp_imag[ii] *= scale;
    }

    return bluestein.cache[size] = (dstre, dstim, srcre, srcim) => {
        for (let ii = 0; ii<size; ++ii) {
            const sreal = srcre?.[ii] ?? 0.0;
            const simag = srcim?.[ii] ?? 0.0;
            const creal = chirp_real[ii];
            const cimag = chirp_imag[ii];
            tdata_real[ii] = sreal*creal - simag*cimag;
            tdata_imag[ii] = sreal*cimag + simag*creal;
        }
        for (let ii = size; ii<over; ++ii) {
            tdata_real[ii] = 0.0;
            tdata_imag[ii] = 0.0;
        }
        fft(fdata_real, fdata_imag, tdata_real, tdata_imag);
        for (let ii = 0; ii<over; ++ii) {
            const freal = fdata_real[ii];
            const fimag = fdata_imag[ii];
            const creal = cdata_real[ii];
            const cimag = cdata_imag[ii];
            fdata_real[ii] = freal*creal - fimag*cimag;
            fdata_imag[ii] = freal*cimag + fimag*creal;
        }
        fft(tdata_imag, tdata_real, fdata_imag, fdata_real);
        for (let ii = 0; ii<size; ++ii) {
            const treal = tdata_real[ii];
            const timag = tdata_imag[ii];
            const creal = chirp_real[ii];
            const cimag = chirp_imag[ii];
            dstre[ii] = treal*creal - timag*cimag;
            dstim[ii] = treal*cimag + timag*creal;
        }
    }
}

function fourstep(size) {
    if (fourstep.cache === undefined) fourstep.cache = {};
    const result = fourstep.cache[size];
    if (result !== undefined) return result;

    const perm = new Uint32Array(size);
    const cos = new Float64Array(size);
    const sin = new Float64Array(size);
    const root = -2*Math.PI/size;
    for (let ii = 0; ii<size; ++ii) {
        perm[ii] = ii;
        cos[ii] = Math.cos(ii*root);
        sin[ii] = Math.sin(ii*root);
    }

    const dfts = { 2: radix2, };
    let count = 1;
    let cols = size;
    const factors = factor(size);
    const ops = [];
    for (let radix of factors) {
        cols /= radix;
        transpose(perm, count, cols, radix);
        let dft = dfts[radix];
        let cnt=count;
        let cls=cols;
        ops.push((real, imag, off=0) => {
            dft(real, imag, off, cnt, cls, cos, sin)
        });
        count *= radix;
    }
    ops.reverse();

    return fourstep.cache[size] = (dstre, dstim, srcre, srcim) => {
        for (let ii = 0; ii<size; ++ii) {
            let pp = perm[ii];
            dstre[ii] = srcre?.[pp] ?? 0;
            dstim[ii] = srcim?.[pp] ?? 0;
        }
        for (let op of ops) {
            op(dstre, dstim);
        }
    }

    function factor(size) {
        let result = [];
        for (let radix of [2]) {
            while (size%radix == 0) {
                result.push(radix);
                size /= radix;
            }
        }
        if (size > 1) throw "need Bluestein";
        result.sort((x,y)=>x-y);
        return result;
    }

    function transpose(perm, count, rows, cols) {
        const size = rows*cols;
        const temp = new Uint32Array(size);
        let offset = 0;
        for (let cc = 0; cc<count; ++cc) {
            for (let ii = 0; ii<rows; ++ii) {
                for (let jj = 0; jj<cols; ++jj) {
                    temp[jj*rows + ii] = perm[offset + ii*cols + jj];
                }
            }
            for (let ij = 0; ij<size; ++ij) {
                perm[offset + ij] = temp[ij];
            }
            offset += size;
        }
    }

    function radix2(real, imag, offset, count, cols, cos, sin) {
        let off0 = offset + 0*cols;
        let off1 = offset + 1*cols;
        let step = 2*cols - cols;
        for (let kk = 0; kk<count; ++kk) {
            let index = count;
            let stride = count;
            for (let jj = 1; jj<cols; ++jj) {
                let loc = off1 + jj;
                let cc = cos[index];
                let ss = sin[index];
                let re = real[loc];
                let im = imag[loc];
                real[loc] = re*cc - im*ss;
                imag[loc] = re*ss + im*cc;
                index += stride;
            }
            for (let jj = 0; jj<cols; ++jj) {
                let r0 = real[off0];
                let i0 = imag[off0];
                let r1 = real[off1];
                let i1 = imag[off1];
                real[off0] = r0 + r1;
                imag[off0] = i0 + i1;
                real[off1] = r0 - r1;
                imag[off1] = i0 - i1;
                ++off0;
                ++off1;
            }
            off0 += step;
            off1 += step;
        }
    }
}


