(module

    (import "env" "put" (func $put (param i32) (result)))

    (func $nested (param) (result)
        (br 0)
        (br 1)

        (loop $outer
            (br $outer)

            (if
                (i32.const 1)
                (then
                    (block (block (block (block 
                        (br $outer)
                    ))))
                )
            )
        )

        (;(loop $outer
            (br $outer)
            (loop $inner0
                (br $outer)
                (block 
                    (br $outer)

                    (if
                        (i32.const 1)
                        (then
                            (br $outer)
                        )
                    )
                )
            )
        );)

    )
    
)
