Stuff I'm willing to share with others...

<h1>Pet Projects</h1>

<ul>
<li>crunch.js compiler
<li>Setup Amazon server (ssh, git, https)
<li>Recursive neural net
    <ul>
    <li>Image in image out
    <li>Prime factors
    <li>Standard OCR
    <li>Dogs and Cats?
    <li>CPU as a net
    </ul>
<li>SVG for math shapes (sinc, gaussian, etc...)
<li>Lorentz Transform Visualization
<li>Ray tracer for tris, splats, and lines
<li>Work through SICP
<li>Work through HnR
<li>Work through Griffith
<li>cosine windows for filters
<li>cosine windows for spectra
<li>Play with WebGL Shader Language
<li>Play with WebGPU Shader Language
<li>Canvas visualization of Riemann Zeta
<li>Implement an eigen decomposition
    <ul>
    <li>Try it with Lorentz matrices
    </ul>
<li>Belka DX
    <ul>
    <li>AM/FM audio
    <li>FT8
    </ul>
<li>cnsk (cyclic noise shift keying)
    <ul>
    <li>Simple random number generator (sum 12 minus 6)
    <li>Use jump shifts to provide independent sub channels
    <li>Separate blocks noise for each sample
    <li>Cyclic rotation to indicate symbol value
    <li>50% overlap with a Sine window
    </ul>
<li>Revisit antialiasing and gamma correction in monomath.h
<li>Publish Rust graphics?
    <ul>
    <li>Glyphs? (done for C++)
    <li>Lines?
    <li>Plots? Traces? Rasters?
    <li>Immediate mode Widgets?
    </ul>
<li>Chrome/USB interface to RTL-SDR
</ul>

